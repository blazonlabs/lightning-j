/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onion;

import ec.Utils;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class OnionParsedPacket {
    
    private byte[] payload;
    private OnionPacket nextPacket;
    private byte[] sharedSecret;
    
    public OnionParsedPacket(byte[] payload, OnionPacket nextPacket, byte[] sharedSecret){
        this.payload = payload;
        this.nextPacket = nextPacket;
        this.sharedSecret = sharedSecret;
    }
    
    public static OnionParsedPacket parsePacket(byte[] privateKey, byte[] associatedData, byte[] rawPacket){
        if(rawPacket.length != OnionConstant.packetLength){
            System.out.println("onion packet length is "+rawPacket.length+", it should be "+OnionConstant.packetLength);
            return null;
        }
        OnionPacket packet = new OnionPacket(rawPacket);
        byte[] sharedSecret = OnionUtils.computeSharedSecret(packet.getPublicKey(), privateKey);
        byte[] mu = OnionUtils.generateKey("mu", sharedSecret);
        byte[] check = OnionUtils.mac(mu, Utils.concatAll(packet.getRoutingInfo(), associatedData));
        if(!Arrays.equals(check, packet.getHmac())){
            System.out.println("invalid header mac");
            return null;
        }
        byte[] rho = OnionUtils.generateKey("rho", sharedSecret);
        byte[] bin = OnionUtils.xor(Utils.concatAll(packet.getRoutingInfo(), OnionUtils.zeroes(OnionConstant.payloadLength+OnionConstant.macLength)), OnionUtils.generateStream(rho, OnionConstant.payloadLength + OnionConstant.macLength + OnionConstant.maxHops * (OnionConstant.payloadLength + OnionConstant.macLength)));
        byte[] payload = Arrays.copyOfRange(bin, 0, OnionConstant.payloadLength);
        byte[] hmac = Arrays.copyOfRange(bin, OnionConstant.payloadLength, OnionConstant.payloadLength+OnionConstant.macLength);
        byte[] nextRouteInfo = Arrays.copyOfRange(bin, OnionConstant.payloadLength+OnionConstant.macLength, bin.length);

        byte[] nextPubKey = OnionUtils.blind(packet.getPublicKey(), OnionUtils.computeblindingFactor(packet.getPublicKey(), sharedSecret));
        return new OnionParsedPacket(payload, new OnionPacket(OnionConstant.version, nextPubKey, hmac, nextRouteInfo), sharedSecret);
    }

    /**
     * @return the nextPacket
     */
    public OnionPacket getNextPacket() {
        return nextPacket;
    }

    /**
     * @param nextPacket the nextPacket to set
     */
    public void setNextPacket(OnionPacket nextPacket) {
        this.nextPacket = nextPacket;
    }

    /**
     * @return the payload
     */
    public byte[] getPayload() {
        return payload;
    }
    
    
}
