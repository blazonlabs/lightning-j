/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onion;

import ec.ChaCha20Poly1305;
import static ec.ECDH_BC.*;
import ec.Hash;
import ec.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;

/**
 *
 * @author Abhimanyu
 */
public class OnionUtils {
    //public static final byte[] version = new byte[1];
    /*public static final int macLength = 32;
    public static final int payloadLength = 33;
    public static final int maxHops = 20;
    public static final int packetLength = 1 + 33 + macLength + maxHops * (payloadLength + macLength);*/
    
    public static List<byte[]> publicKeys = new ArrayList<byte[]>();
    public static List<byte[]> ephemeralPublicKeys = new ArrayList<byte[]>();
    public static List<byte[]> blindingFactors = new ArrayList<byte[]>();
    public static List<byte[]> sharedSecrets = new ArrayList<byte[]>();
    
    public static OnionPacket lastPacket(){
        return new OnionPacket(0, zeroes(33), zeroes(OnionConstant.macLength), zeroes(OnionConstant.maxHops * (OnionConstant.payloadLength + OnionConstant.macLength)));
    }
    
    public static byte[] hmac256(byte[] key, byte[] message){
        HMac mac = new HMac(new SHA256Digest());
        mac.init(new KeyParameter(key));
        mac.update(message, 0, message.length);
        byte[] output = new byte[32];
        mac.doFinal(output, 0);
        return output;
    }
    
    public static byte[] mac(byte[] key, byte[] message){
        byte[] hm = hmac256(key, message);
        return Arrays.copyOfRange(hm, 0, OnionConstant.macLength);
    }
    
    public static byte[] xor(byte[] a, byte[] b){
        byte[] output = new byte[a.length];
        int i = 0;
        for (byte by : a){
            output[i] = (byte)((by ^  b[i++]) & 0xff);
        }
        return output;
    }
    
    public static byte[] generateKey(byte[] keyType, byte[] secret){
        if(secret.length != 32)
            return "secret must be 32 bytes".getBytes();
        return hmac256(keyType, secret);
    }
    
    public static byte[] generateKey(String keyType, byte[] secret){
        if(secret.length != 32)
            return "secret must be 32 bytes".getBytes();
        return hmac256(keyType.getBytes(), secret);
    }
    
    public static byte[] zeroes(int length){
        return new byte[length];
    }
    
    public static byte[] generateStream(byte[] key, int length){
        return new ChaCha20Poly1305().chaCha20LegacyEncrypt(zeroes(length), key, zeroes(8), 0);
    }
    
    public static byte[] computeSharedSecret(byte[] pub, byte[] prv){
        try {
            return doECDH(prv, pub);
        } catch (Exception ex) {
            return new byte[0];
        }
    }
    
    public static byte[] computeblindingFactor(byte[] pub, byte[] prv){
        return Hash.sha256(Utils.concatAll(pub, prv));
    }
    
    public static byte[] blind(byte[] pub, byte[] blindingFactor){
        try {
            return getECPoint(blindingFactor, pub);
        } catch (Exception ex) {
            return null;
        }
    }
    
    public static byte[] blind(byte[] pub, List<byte[]> blindingFactor){
        try {
            return getECPointN(blindingFactor, pub);
        } catch (Exception ex) {
            return null;
        }
    }
    
    public static byte[] head(List<byte[]> list) {
        return list.subList(0, 1).get(0);
    }
    
    public static byte[] last(List<byte[]> list) {
        return list.subList(list.size()-1, list.size()).get(0);
    }
    
    public static List<byte[]> tail(List<byte[]> list) {
        return list.subList(1, list.size());
    }
    
    public static void computeEphemeralPublicKeysAndSharedSecrets(byte[] sessionKey){
        try {
            ephemeralPublicKeys.add(blind(getCurveG().getEncoded(true), sessionKey));
            sharedSecrets.add(computeSharedSecret(publicKeys.get(0), sessionKey));
            blindingFactors.add(computeblindingFactor(ephemeralPublicKeys.get(0), sharedSecrets.get(0)));
            computeEphemeralPublicKeysAndSharedSecrets(sessionKey, tail(publicKeys));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void computeEphemeralPublicKeysAndSharedSecrets(byte[] sessionKey, List<byte[]> publicKeysSub){
        try {
            //System.out.println(publicKeysSub.size());
            if(!publicKeysSub.isEmpty()){
                //System.out.println(ephemeralPublicKeys.size());
                byte[] ephemeralPublicKey = blind(last(ephemeralPublicKeys), last(blindingFactors));
                ephemeralPublicKeys.add(ephemeralPublicKey);
                byte[] sharedSecret = computeSharedSecret(blind(head(publicKeysSub), blindingFactors), sessionKey);
                sharedSecrets.add(sharedSecret);
                blindingFactors.add(computeblindingFactor(ephemeralPublicKey, sharedSecret));
                computeEphemeralPublicKeysAndSharedSecrets(sessionKey, tail(publicKeysSub));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static byte[] generateFiller(String keyType, List<byte[]> sharedSecrets, int hopSize, int maxNumberOfHops){
        //if(maxNumberOfHops <= maxHops)
        byte[] padding = new byte[0];
        for(int i=0;i<sharedSecrets.size();i++){
            byte[] key = generateKey(keyType, sharedSecrets.get(i));
            byte[] padding1 = Utils.concatAll(padding, zeroes(hopSize));
            byte[] temp = generateStream(key, hopSize * (maxNumberOfHops + 1));
            byte[] stream = Arrays.copyOfRange(temp, temp.length - padding1.length, temp.length);
            padding = xor(padding1, stream);
        }
        return padding;
    }
    
}
