/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onion;

import ec.Utils;
import java.util.ArrayList;
import java.util.List;
import static onion.OnionUtils.computeEphemeralPublicKeysAndSharedSecrets;
import static onion.OnionUtils.ephemeralPublicKeys;
import static onion.OnionUtils.generateFiller;
import static onion.OnionUtils.publicKeys;
import static onion.OnionUtils.sharedSecrets;

/**
 *
 * @author Abhimanyu
 */
public class OnionTest {
    
    public static void main(String[] args){
        System.out.println(Utils.byteArrayToHexString("wellcome".getBytes()));
        
        List<byte[]> privKeys = new ArrayList<>();
        privKeys.add(Utils.hexStringToByteArray("4141414141414141414141414141414141414141414141414141414141414141"));
        privKeys.add(Utils.hexStringToByteArray("4242424242424242424242424242424242424242424242424242424242424242"));
        privKeys.add(Utils.hexStringToByteArray("4343434343434343434343434343434343434343434343434343434343434343"));
        privKeys.add(Utils.hexStringToByteArray("4444444444444444444444444444444444444444444444444444444444444444"));
        privKeys.add(Utils.hexStringToByteArray("4545454545454545454545454545454545454545454545454545454545454545"));
        
        publicKeys.add(Utils.hexStringToByteArray("02eec7245d6b7d2ccb30380bfbe2a3648cd7a942653f5aa340edcea1f283686619"));
        publicKeys.add(Utils.hexStringToByteArray("0324653eac434488002cc06bbfb7f10fe18991e35f9fe4302dbea6d2353dc0ab1c"));
        publicKeys.add(Utils.hexStringToByteArray("027f31ebc5462c1fdce1b737ecff52d37d75dea43ce11c74d25aa297165faa2007"));
        publicKeys.add(Utils.hexStringToByteArray("032c0b7cf95324a07d05398b240174dc0c2be444d96b159aa6c7f7b1e668680991"));
        publicKeys.add(Utils.hexStringToByteArray("02edabbd16b41c8371b92ef2f04c1185b4f03b6dcd52ba9b78d9d7c89c8f221145"));
        
        byte[] sessionKey = Utils.hexStringToByteArray("4141414141414141414141414141414141414141414141414141414141414141");
        
        List<byte[]> payloads = new ArrayList<>();
        payloads.add(Utils.hexStringToByteArray("000000000000000000000000000000000000000000000000000000000000000000"));
        payloads.add(Utils.hexStringToByteArray("000101010101010101000000010000000100000000000000000000000000000000"));
        payloads.add(Utils.hexStringToByteArray("000202020202020202000000020000000200000000000000000000000000000000"));
        payloads.add(Utils.hexStringToByteArray("000303030303030303000000030000000300000000000000000000000000000000"));
        payloads.add(Utils.hexStringToByteArray("000404040404040404000000040000000400000000000000000000000000000000"));
        
        byte[] associatedData = Utils.hexStringToByteArray("4242424242424242424242424242424242424242424242424242424242424242");
        
        /*//test => generate ephemeral keys and secrets
        computeEphemeralPublicKeysAndSharedSecrets(sessionKey);
        for(int i=0;i<ephemeralPublicKeys.size();i++){
            System.out.println("ephemeralPublicKeys["+i+"] = "+Utils.byteArrayToHexString(ephemeralPublicKeys.get(i)));
            System.out.println("sharedSecrets["+i+"] = "+Utils.byteArrayToHexString(sharedSecrets.get(i)));
        }
        
        
        //test => generate filler
        byte[] filler = generateFiller("rho", sharedSecrets.subList(0, sharedSecrets.size()-1), OnionConstant.payloadLength + OnionConstant.macLength, 20);
        System.out.println("filler = "+Utils.byteArrayToHexString(filler));
        */
        
        //test => create packet (reference test vector)
        OnionPacketAndSecrets opas = new OnionPacketAndSecrets().makePacket(sessionKey, payloads, associatedData);
        System.out.println("onion.serialize  = "+Utils.byteArrayToHexString(opas.getPacket().write()));
        
        OnionParsedPacket pp0 = OnionParsedPacket.parsePacket(privKeys.get(0), associatedData, opas.getPacket().serialize());
        OnionParsedPacket pp1 = OnionParsedPacket.parsePacket(privKeys.get(1), associatedData, pp0.getNextPacket().serialize());
        OnionParsedPacket pp2 = OnionParsedPacket.parsePacket(privKeys.get(2), associatedData, pp1.getNextPacket().serialize());
        OnionParsedPacket pp3 = OnionParsedPacket.parsePacket(privKeys.get(3), associatedData, pp2.getNextPacket().serialize());
        OnionParsedPacket pp4 = OnionParsedPacket.parsePacket(privKeys.get(4), associatedData, pp3.getNextPacket().serialize());
        
        System.out.println("pp0 payload = "+Utils.byteArrayToHexString(pp0.getPayload()));
        System.out.println("pp1 payload = "+Utils.byteArrayToHexString(pp1.getPayload()));
        System.out.println("pp2 payload = "+Utils.byteArrayToHexString(pp2.getPayload()));
        System.out.println("pp3 payload = "+Utils.byteArrayToHexString(pp3.getPayload()));
        System.out.println("pp4 payload = "+Utils.byteArrayToHexString(pp4.getPayload()));
        
        System.out.println("pp0 hmac = "+Utils.byteArrayToHexString(pp0.getNextPacket().getHmac()));
        System.out.println("pp1 hmac = "+Utils.byteArrayToHexString(pp1.getNextPacket().getHmac()));
        System.out.println("pp2 hmac = "+Utils.byteArrayToHexString(pp2.getNextPacket().getHmac()));
        System.out.println("pp3 hmac = "+Utils.byteArrayToHexString(pp3.getNextPacket().getHmac()));
        System.out.println("pp4 hmac = "+Utils.byteArrayToHexString(pp4.getNextPacket().getHmac()));
        
    }
}
