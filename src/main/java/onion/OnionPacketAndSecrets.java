/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onion;

import ec.Utils;
import java.util.List;
import org.bouncycastle.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class OnionPacketAndSecrets {
    
    private OnionPacket packet;
    
    public OnionPacketAndSecrets(){
        
    }
    public OnionPacketAndSecrets(OnionPacket packet){
        this.packet = packet;
    }
    
    public OnionPacket makeNextPacket(byte[] payload, byte[] associatedData, byte[] ephemeralPublicKey, byte[] sharedSecret, OnionPacket packet, byte[] routingInfoFiller){
        if(payload.length != OnionConstant.payloadLength){
            System.out.println("payload length is "+payload.length+", it should be "+OnionConstant.payloadLength);
            return null;
        }
        byte[] routingInfo1 = Utils.concatAll(payload, Utils.concatAll(packet.getHmac(), Arrays.copyOfRange(packet.getRoutingInfo(), 0, packet.getRoutingInfo().length-(OnionConstant.payloadLength + OnionConstant.macLength))));
        byte[] routingInfo2 = OnionUtils.xor(routingInfo1, OnionUtils.generateStream(OnionUtils.generateKey("rho", sharedSecret), OnionConstant.maxHops * (OnionConstant.payloadLength + OnionConstant.macLength)));
        byte[] nextRoutingInfo = Utils.concatAll(Arrays.copyOfRange(routingInfo2, 0, routingInfo2.length-routingInfoFiller.length), routingInfoFiller);
        
        byte[] nextMac = OnionUtils.mac(OnionUtils.generateKey("mu", sharedSecret), Utils.concatAll(nextRoutingInfo, associatedData));
        return new OnionPacket(0, ephemeralPublicKey, nextMac, nextRoutingInfo);
    }
    
    public OnionPacket loop(List<byte[]> hoppayloads, List<byte[]> ephkeys, List<byte[]> sharedSecrets, OnionPacket packet, byte[] associatedData){
        if(hoppayloads.isEmpty())
            return packet;
        else{
            OnionPacket nextPacket = makeNextPacket(OnionUtils.last(hoppayloads), associatedData, OnionUtils.last(ephkeys), OnionUtils.last(sharedSecrets), packet, new byte[0]);
            //System.out.println("loop nextPacket = "+Utils.byteArrayToHexString(nextPacket.serialize()));
            return loop(hoppayloads.subList(0, hoppayloads.size()-1), ephkeys.subList(0, ephkeys.size()-1), sharedSecrets.subList(0, sharedSecrets.size()-1), nextPacket, associatedData);
        }
    }
    
    public OnionPacketAndSecrets makePacket(byte[] sessionKey, List<byte[]> payloads, byte[] associatedData){
        OnionUtils.computeEphemeralPublicKeysAndSharedSecrets(sessionKey);
        for(int i=0;i<OnionUtils.ephemeralPublicKeys.size();i++){
            System.out.println("makePacket ephemeralPublicKeys["+i+"] = "+Utils.byteArrayToHexString(OnionUtils.ephemeralPublicKeys.get(i)));
            System.out.println("makePacket sharedSecrets["+i+"] = "+Utils.byteArrayToHexString(OnionUtils.sharedSecrets.get(i)));
        }
        byte[] filler = OnionUtils.generateFiller("rho", OnionUtils.sharedSecrets.subList(0, OnionUtils.sharedSecrets.size()-1), OnionConstant.payloadLength + OnionConstant.macLength, OnionConstant.maxHops);
        System.out.println("makePacket filler = "+Utils.byteArrayToHexString(filler));
        OnionPacket lastPacket = makeNextPacket(OnionUtils.last(payloads), associatedData, OnionUtils.last(OnionUtils.ephemeralPublicKeys), OnionUtils.last(OnionUtils.sharedSecrets), OnionUtils.lastPacket(), filler);
        //System.out.println("makePacket lastPacket = "+Utils.byteArrayToHexString(lastPacket.serialize()));
        OnionPacket packet = loop(payloads.subList(0, payloads.size()-1), OnionUtils.ephemeralPublicKeys.subList(0, OnionUtils.ephemeralPublicKeys.size()-1), OnionUtils.sharedSecrets.subList(0, OnionUtils.sharedSecrets.size()-1), lastPacket, associatedData);
        //System.out.println("makePacket packet = "+Utils.byteArrayToHexString(packet.serialize()));
        return new OnionPacketAndSecrets(packet);
    }

    /**
     * @return the packet
     */
    public OnionPacket getPacket() {
        return packet;
    }

    /**
     * @param packet the packet to set
     */
    public void setPacket(OnionPacket packet) {
        this.packet = packet;
    }
    
    
}
