/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onion;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class OnionPacket {
    
    private int version;
    private byte[] publicKey;
    private byte[] hmac;
    private byte[] routingInfo;
    
    public OnionPacket(byte[] in){
        try{
            InputStream bain= new ByteArrayInputStream(in);
            this.version = bain.read();
            this.publicKey = new byte[33];
            bain.read(this.publicKey);
            this.routingInfo = new byte[OnionConstant.maxHops * (OnionConstant.payloadLength + OnionConstant.macLength)];
            bain.read(this.routingInfo);
            this.hmac = new byte[OnionConstant.macLength];
            bain.read(this.hmac);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public OnionPacket(int version, byte[] publicKey, byte[] hmac, byte[] routingInfo){
        this.version = version;
        this.publicKey = publicKey;
        this.hmac = hmac;
        this.routingInfo = routingInfo;
    }

    public boolean isLastPacket(){
        return Arrays.equals(hmac, OnionUtils.zeroes(OnionConstant.macLength));
    }
    
    public byte[] serialize(){
        return write();
    }
    
    public byte[] write(){
        try{
            ByteArrayOutputStream out = new ByteArrayOutputStream(OnionConstant.packetLength);
            out.write(this.version);
            out.write(this.publicKey);
            out.write(this.routingInfo);
            out.write(this.hmac);
            return out.toByteArray();
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public void read(byte[] in){
        try{
            InputStream bain= new ByteArrayInputStream(in);
            this.version = bain.read();
            this.publicKey = new byte[33];
            bain.read(this.publicKey);
            this.routingInfo = new byte[OnionConstant.maxHops * (OnionConstant.payloadLength + OnionConstant.macLength)];
            bain.read(this.routingInfo);
            this.hmac = new byte[OnionConstant.macLength];
            bain.read(this.hmac);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    /**
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * @return the publicKey
     */
    public byte[] getPublicKey() {
        return publicKey;
    }

    /**
     * @param publicKey the publicKey to set
     */
    public void setPublicKey(byte[] publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * @return the hmac
     */
    public byte[] getHmac() {
        return hmac;
    }

    /**
     * @param hmac the hmac to set
     */
    public void setHmac(byte[] hmac) {
        this.hmac = hmac;
    }

    /**
     * @return the routingInfo
     */
    public byte[] getRoutingInfo() {
        return routingInfo;
    }

    /**
     * @param routingInfo the routingInfo to set
     */
    public void setRoutingInfo(byte[] routingInfo) {
        this.routingInfo = routingInfo;
    }
}
