/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onion;

/**
 *
 * @author Abhimanyu
 */
public class OnionConstant {
    public static final int version = 0;
    public static final int macLength = 32;
    public static final int payloadLength = 33;
    public static final int maxHops = 20;
    public static final int packetLength = 1 + 33 + macLength + maxHops * (payloadLength + macLength);
}
