/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author neerajnagi
 */

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.io.Tcp;
import broker.MqttServer;
import ec.KeyUtils;
import ec.Utils;
import io.tcp.ActorPool;
import io.tcp.InitiatorTemp;
import io.tcp.ResponderTemp;
import java.util.logging.Level;
import java.util.logging.Logger;
import misc.ActorSystemContainer;
import zmq.ZmqManager;

public class Main {
    
    public static void main(String args[]){
        
        ActorSystem system = ActorSystemContainer.getInstance().getSystem();
        System.out.println("1");
        //ActorRef listener = system.actorOf(Props.create(Listener.class ), "listener");
        System.out.println("2");
        final ActorRef tcpManager = Tcp.get(system).manager();
        System.out.println("3");
        
        //final ActorRef mqttServerActor = system.actorOf(Props.create(MqttServer.class));
        final ActorRef mqttServerActor = ActorPool.get(ActorPool.actor.MqttServer, "broker");
        final ActorRef zmqManager = ActorPool.get(ActorPool.actor.ZmqManager, "zmq");
        
        KeyUtils ls = new KeyUtils();
        try {
            System.out.println("JBitNode ls.prv : " + Utils.byteArrayToHexString(ls.prv()));
            System.out.println("JBitNode ls.pub : " + Utils.byteArrayToHexString(ls.pub()));
            
            //ActorRef zmqManager = system.actorOf(Props.create(ZmqManager.class), "ZMQManager");
            
            ActorRef r = system.actorOf(Props.create(ResponderTemp.class, tcpManager, ls), ActorPool.actor.ResponderTemp.toString());
            ActorPool.set(ActorPool.actor.ResponderTemp, r);
            ActorRef i = system.actorOf(Props.create(InitiatorTemp.class, tcpManager, ls), ActorPool.actor.InitiatorTemp.toString());
            ActorPool.set(ActorPool.actor.InitiatorTemp, i);
            
            mqttServerActor.tell(i, ActorRef.noSender());
            mqttServerActor.tell("START_MQTT_BROKER", ActorRef.noSender());
            
            //i.tell(new CmdToInitiator("127.0.0.1", 9735, Utils.byteArrayToHexString(ls.pub())), ActorRef.noSender());
            //i.tell(new CmdToInitiator("192.168.1.101", 9735, "02513c17d45d1204093ba50a58dca592a5fca61b36a3a11c105d0c8d95336dbc7e"), ActorRef.noSender());
            //i.tell(new CmdToInitiator("192.168.1.101", 10015, "03f1995b5fdb47a40d02686e72fac42b0459d0c0ef1f5675a740b38ac2d7b2a862"), ActorRef.noSender());
            
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
