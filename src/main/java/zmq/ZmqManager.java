/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zmq;

import akka.actor.AbstractActor;
import akka.actor.ActorSystem;
import akka.actor.Props;
import bitcoin.BitcoinClient;
import bitcoin.BitcoinUtils;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import misc.ActorSystemContainer;
import msg.FundingTxIdStatus;
import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZMsg;
import scala.concurrent.duration.Duration;
import ec.Utils;
import java.util.ArrayList;
import msg.Transaction;
/**
 *
 * @author Abhimanyu
 */
public class ZmqManager extends AbstractActor {
    final ActorSystem system = ActorSystemContainer.getInstance().getSystem();
    BitcoinUtils bu;
    BitcoinClient bc;
    ZMQ.Context context  = ZMQ.context(1);
    Socket socket = context.socket(ZMQ.SUB);
    HashMap<String, FundingTxIdStatus> ftxids = new HashMap<String, FundingTxIdStatus>();
    HashMap<String, String> frawtxs = new HashMap<String, String>();
    HashMap<String, Integer> ftxblocks = new HashMap<String, Integer>();
    
    public static Props props() {
        return Props.create(ZmqManager.class);
    }

    public ZmqManager() {
        System.out.println("ZmqManager Constructor Start");
        this.bu = new BitcoinUtils();
        this.bc = new BitcoinClient("http://blazon:blazon123@192.168.1.101:7332/");
        System.out.println("ZmqManager Constructor End");
    }

    public int getBlockHeight(){
        return  bc.getBitcoin().getBlockCount();
    }
    
    @Override
    public void preStart() throws Exception {
        System.out.println("ZmqManager PreStart Start");
        socket.connect("tcp://192.168.1.101:8332");
        socket.subscribe("rawblock");
        socket.subscribe("rawtx");
        system.scheduler().schedule(Duration.Zero(), Duration.create(1, TimeUnit.SECONDS  ), getSelf(), "READ", system.dispatcher(), null);
        system.scheduler().schedule(Duration.Zero(), Duration.create(10, TimeUnit.SECONDS  ), getSelf(), "BLOCK_HEIGHT", system.dispatcher(), null);
        System.out.println("ZmqManager PreStart End");
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(FundingTxIdStatus.class, message -> {
                    System.out.println("ZmqManager FundingTxIdStatus.class => " + message.getFtxid());
                    ftxids.put(message.getFtxid(), message);
                })
                .matchEquals("BLOCK_HEIGHT", message -> {
                    System.out.println("ZmqManager BLOCK_HEIGHT : " + message);
                    int blockCount = getBlockHeight();
                    System.out.println("ZmqManager blockCount : " + blockCount);
                    ArrayList<String> temp = new ArrayList<String>();
                    for(String ftxblock : ftxblocks.keySet()){
                        if(ftxblocks.get(ftxblock) <= blockCount){
                            temp.add(ftxblock);
                            ftxids.get(ftxblock).setTxIdError(false);
                            ftxids.get(ftxblock).setTxIdErrorMessage("");
                            ftxids.get(ftxblock).setTxIdVerifed(true);
                            ftxids.get(ftxblock).getPeer().tell(ftxids.get(ftxblock), getSelf());
                        }
                    }
                    for(String txid : temp){
                        ftxblocks.remove(txid);
                    }
                    temp.clear();
                })
                .matchEquals("READ", message -> {
                    //System.out.println(getSelf().path().toString());
                    //System.out.println("ZmqManager READ : " + message);
                    ZMsg zMsg = new ZMsg();
                    while(zMsg != null){
                        zMsg = ZMsg.recvMsg(socket, ZMQ.NOBLOCK);
                        if(zMsg != null){
                            System.out.println("ZmqManager : New message received!");
                            int messageNumber = 0;
                            String messageType = "";
                            for (ZFrame f: zMsg) {
                                byte[] bytes = f.getData();
                                System.out.println("ZmqManager Message Byte array length : " + bytes.length);
                                if (messageNumber == 0){
                                    messageType = new String(bytes);
                                    System.out.println("ZmqManager Topic : " + messageType);
                                } else if (messageNumber == 1) {
                                    String msg = ec.Utils.byteArrayToHexString(bytes);
                                    
                                    if(messageType.equals("rawtx")){
                                        Transaction trs = new Transaction(bytes);
                                        String ftxid = Utils.byteArrayToHexString(trs.getTxid());
                                        if(ftxids.containsKey(ftxid)){
                                            ftxids.get(ftxid).setTxInSystem(true);
                                            frawtxs.put(ftxid, msg);
                                        }
                                    }else if(messageType.equals("rawblock")){
                                        int blockCount = bc.getBitcoin().getBlockCount();
                                        ArrayList<String> temp = new ArrayList<String>();
                                        for(String frawtx : frawtxs.keySet()){
                                            String raw = frawtxs.get(frawtx);
                                            if(msg.contains(raw)){
                                                ftxblocks.put(frawtx, blockCount+(ftxids.get(frawtx).getMinimumDepth()));
                                                temp.add(frawtx);
                                            }
                                        }
                                        for(String txid : temp){
                                            frawtxs.remove(txid);
                                        }
                                        temp.clear();
                                    }else{
                                        System.out.println("ZmqManager Message Type is different ");
                                    }
                                    
                                    System.out.println("ZmqManager Message : " + msg);
                                }
                                messageNumber++;
                            }
                        }
                    }
                })
                .matchAny(message -> {
                    System.out.println("ZmqManager Any Msg => " + message);
                })
                .build();
    }
}
