/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zmq;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import ec.Utils;
import org.zeromq.ZFrame;
import org.zeromq.ZMsg;
/**
 *
 * @author Abhimanyu
 */
public class test {
    
    public static void main(String[] args){
        ZMQ.Context context  = ZMQ.context(1);
        Socket socket = context.socket(ZMQ.SUB);
        socket.connect("tcp://192.168.1.101:8332");
        socket.subscribe("rawblock");
        socket.subscribe("rawtx");
        
        System.out.println("Subscribed.. Waiting for messages.");
        //while (true) {

            ZMsg zMsg = ZMsg.recvMsg(socket, ZMQ.NOBLOCK);
            if(zMsg != null){
                System.out.println("New message received!");
                int messageNumber = 0;
                for (ZFrame f: zMsg) {
                    byte[] bytes = f.getData();
                    System.out.println("Message number: " + messageNumber + " | Byte array length: " + bytes.length);
                    if (messageNumber == 0){
                        String messageType = new String(bytes);
                        System.out.println("Message type: " + messageType);
                    } else if (messageNumber == 1) {
                        String message = Utils.byteArrayToHexString(bytes);
                        System.out.println("Message: " + message);
                    }
                    messageNumber++;
                }
            }

       // }
        
//        while (!Thread.currentThread ().isInterrupted ()) {
//            // Read envelope with address
//            String address = socket.recvStr();
//            // Read message contents
//            byte[] contents = socket.recv();
//            System.out.println(address + " : " + Utils.byteArrayToHexString(contents));
//        }
//        socket.close();
//        context.term();
    }
}
