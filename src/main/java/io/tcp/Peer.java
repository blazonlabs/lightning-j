/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.tcp;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.io.Tcp;
import akka.io.TcpMessage;
import akka.util.ByteString;
import bitcoin.BitTrans;
import bitcoin.BitcoinUtils;
import bolt3.RPC;
import ec.ByteUtils;
import ec.ChaCha20Poly1305;
import ec.ECDH_BC;
import static ec.ECDH_BC.doECDH;
import ec.HKDF;
import ec.Hash;
import ec.KeyUtils;
import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import static ec.Utils.hexStringToByteArray;
import io.moquette.interception.messages.InterceptPublishMessage;
import io.netty.buffer.ByteBufUtil;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import misc.ActorSystemContainer;
import msg.*;

/**
 *
 * @author Abhimanyu
 */
public class Peer extends AbstractActor {
    
    final ActorSystem system = ActorSystemContainer.getInstance().getSystem();
    InetSocketAddress remote = null;
    final ActorRef manager;
    final ActorRef zmqManager;
    KeyUtils e = null;
    KeyUtils ls;
    String peerType;//ResponderPeer OR Initiator Peer
    byte[] h;
    byte[] ck;
    int sn = 0;
    int rn = 0;
    byte[] sk;
    byte[] rk;
    byte[] temp_k2;
    CmdToInitiator cti = null;
    Keys keys = new Keys();
    BitTrans bt;
    BitcoinUtils bu;
    AcceptChannel acceptChannel;
    AnnouncementSignatures announcementSignatures;
    ChannelAnnouncement channelAnnouncement;
    ChannelReestablish channelReestablish;
    ChannelUpdate channelUpdate;
    ClosingSigned closingSigned;
    CommitmentSigned commitmentSigned;
    msg.Error error;
    FundingCreated fundingCreated;
    FundingLocked fundingLocked;
    FundingSigned fundingSigned;
    FundingTxIdStatus fundingTxIdStatus;
    Init init;
    NodeAnnouncement nodeAnnouncement;
    OpenChannel openChannel;
    Ping ping;
    Pong pong;
    RevokeAndAck revokeAndAck;
    Shutdown shutdown;
    UpdateAddHtlc updateAddHtlc;
    UpdateFailHtlc updateFailHtlc;
    UpdateFailMalformedHtlc updateFailMalformedHtlc;
    UpdateFee updateFee;
    UpdateFulfillHtlc updateFulfillHtlc;
    
    
   
    public Peer(ActorRef manager, KeyUtils ls, String peerType) {
        System.out.println("Peer Constructor Start");
        this.manager = manager;
        this.ls = ls;
        this.peerType = peerType;
        zmqManager = ActorPool.get(ActorPool.actor.ZmqManager);
        System.out.println("Peer Constructor End");
    }
    
    public static Props props(InetSocketAddress remote, ActorRef manager) {
        return Props.create(Peer.class, remote, manager);
    }
    
    @Override
    public void preStart() {
        try{
            System.out.println("Peer preStart Start");
            bu = new BitcoinUtils();
            bt = new BitTrans("http://blazon:blazon123@192.168.1.101:7332/", bu);
            h = Hash.sha256(constants.Constants.protocolName.getBytes());
            ck = h;
            h = Hash.sha256(Utils.concatAll(h, constants.Constants.prolouge.getBytes()));
            System.out.println("Peer preStart End");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CmdToInitiator.class, message -> {
                    try{
                        System.out.println("Peer CmdToInitiator.class => " + message);
                        cti = message;
                        manager.tell(TcpMessage.connect(message.getAddress()), getSelf()); 
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(Tcp.Bound.class, message -> {
                    try{
                        System.out.println("Peer Bound.class => " + message);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(Tcp.CommandFailed.class, message -> {
                    try{
                        System.err.println("Peer CommandFailed.class => " + message);
                        getContext().stop(getSelf());
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(Tcp.Connected.class, conn -> {
                    try{
                        System.out.println("Peer Connected.class => " + conn);
                        if(peerType.equals(constants.Constants.INITIATOR)){
                            getSender().tell(TcpMessage.register(getSelf()), getSelf());
                            try{
                                //----------------------Initiator Act 1 Start--------------------------------------
                                h = Hash.sha256(Utils.concatAll(h, cti.getRspubBytes()));
                                System.out.println("Peer h : " + byteArrayToHexString(h));

                                e = new KeyUtils();
                                System.out.println("Peer Act 1 ePrv : " + byteArrayToHexString(e.prv()));
                                System.out.println("Peer Act 1 ePub : " + byteArrayToHexString(e.pub()));
                                
                                h = Hash.sha256(Utils.concatAll(h, e.pub()));
                                System.out.println("Peer Act 1 h : " + byteArrayToHexString(h));       

                                byte[] ss = doECDH(e.prv(), cti.getRspubBytes());
                                System.out.println("Peer Act 1 ss : " + byteArrayToHexString(ss));

                                byte[] okm = HKDF.doHKDF(ck, ss);
                                System.out.println("Peer Act 1 okm : " + byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                                ck = Arrays.copyOfRange(okm, 0, 32);
                                byte[] temp_k1 = Arrays.copyOfRange(okm, 32, okm.length);
                                System.out.println("Peer Act 1 ck : " + byteArrayToHexString(ck));
                                System.out.println("Peer Act 1 temp_k1 : " + byteArrayToHexString(temp_k1));
                                
                                byte[][] c = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(temp_k1, Utils.getNonceFromInt(0), new byte[0], h);
                                System.out.println("Peer Act 1 cipher length : "+c[0].length+", value : "+byteArrayToHexString(c[0]));
                                System.out.println("Peer Act 1 tag length : "+c[1].length+", value : "+byteArrayToHexString(c[1]));

                                h = Hash.sha256(Utils.concatAll(h, c));

                                byte[] m = new byte[1];
                                m[0] = 0;
                                byte[] act1Output = Utils.concatAll(Utils.concatAll(m, e.pub()), c);
                                System.out.println("Peer Act 1 act1Output length : "+act1Output.length+", value : "+byteArrayToHexString(act1Output));

                                getSender().tell(TcpMessage.write(ByteString.fromArray(act1Output)), getSelf());
                                //----------------------Initiator Act 1 End----------------------------------------+
                                getContext().become(act2(getSender()));
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        if(peerType.equals(constants.Constants.RESPONDER)){
                            manager.tell(conn, getSelf());
                            getSender().tell(TcpMessage.register(getSelf()), getSelf());
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(Tcp.Received.class, message -> {
                    try{
                        h = Hash.sha256(Utils.concatAll(h, ls.pub()));
                        System.out.println("Peer Received.class => " + message);
                        byte[] data = message.data().toArray();
                        System.out.println("Peer data length : " + data.length + ", value : " + byteArrayToHexString(data));
                        if(data.length == 50){
                            //----------------------Responder Act 1 Start--------------------------------------
                            byte[] v = Arrays.copyOfRange(data, 0, 1);
                            System.out.println("Peer Act 1 v length : " + v.length + ", value : " + byteArrayToHexString(v));
                            byte[] re = Arrays.copyOfRange(data, 1, 34);
                            System.out.println("Peer Act 1 re length : " + re.length + ", value : " + byteArrayToHexString(re));
                            byte[] c = Arrays.copyOfRange(data, 34, 50);
                            System.out.println("Peer Act 1 c length : " + c.length + ", value : " + byteArrayToHexString(c));

                            h = Hash.sha256(Utils.concatAll(h, re));
                            System.out.println("Peer Act 1 h : " + byteArrayToHexString(h));

                            byte[] ss = ECDH_BC.doECDH(ls.prv(), re);
                            System.out.println("Peer Act 1 ss : " + byteArrayToHexString(ss));

                            byte[] okm = HKDF.doHKDF(ck, ss);
                            System.out.println("Peer Act 1 okm : " + byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                            ck = Arrays.copyOfRange(okm, 0, 32);
                            byte[] temp_k1 = Arrays.copyOfRange(okm, 32, okm.length);
                            System.out.println("Peer Act 1 ck : " + byteArrayToHexString(ck));
                            System.out.println("Peer Act 1 temp_k1 : " + byteArrayToHexString(temp_k1));

                            byte[] p = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(temp_k1, Utils.getNonceFromInt(0), new byte[0], h, c);
                            System.out.println("Peer Act 1 plain length : "+p.length+", value : "+byteArrayToHexString(p));

                            h = Hash.sha256(Utils.concatAll(h, c));
                            System.out.println("Peer Act 1 h : " + byteArrayToHexString(h));
                            //----------------------Responder Act 1 End----------------------------------------

                            //----------------------Responder Act 2 Start--------------------------------------
                            e = new KeyUtils();
                            System.out.println("Peer Act 2 ePrv : " + byteArrayToHexString(e.prv()));
                            System.out.println("Peer Act 2 ePub : " + byteArrayToHexString(e.pub()));

                            h = Hash.sha256(Utils.concatAll(h, e.pub()));
                            System.out.println("Peer Act 2 h : " + byteArrayToHexString(h));       

                            ss = ECDH_BC.doECDH(e.prv(), re);
                            System.out.println("Peer Act 2 ss : " + byteArrayToHexString(ss));

                            byte[] okm2 = HKDF.doHKDF(ck, ss);
                            System.out.println("Peer Act 2 okm : " + byteArrayToHexString(okm2) + ", okm Length : " + okm2.length);
                            ck = Arrays.copyOfRange(okm2, 0, 32);
                            temp_k2 = Arrays.copyOfRange(okm2, 32, okm2.length);
                            System.out.println("Peer Act 2 ck : " + byteArrayToHexString(ck));
                            System.out.println("Peer Act 2 temp_k2 : " + byteArrayToHexString(temp_k2));

                            byte[][] c2 = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(temp_k2, Utils.getNonceFromInt(0), new byte[0], h);
                            System.out.println("Peer Act 2 cipher length : "+c2[0].length+", value : "+byteArrayToHexString(c2[0]));
                            System.out.println("Peer Act 2 tag length : "+c2[1].length+", value : "+byteArrayToHexString(c2[1]));

                            h = Hash.sha256(Utils.concatAll(h, c2));

                            byte[] m = new byte[1];
                            m[0] = 0;
                            byte[] act2Output = Utils.concatAll(Utils.concatAll(m, e.pub()), c2);
                            System.out.println("Peer Act 2 act2Output length : "+act2Output.length+", value : "+byteArrayToHexString(act2Output));

                            getSender().tell(TcpMessage.write(ByteString.fromArray(act2Output)), getSelf());
                            //----------------------Responder Act 2 End----------------------------------------
                            getContext().become(act3(getSender()));
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(ByteString.class, message -> {
                    try{
                        System.out.println("Peer ByteString.class => " + message);
                        if(peerType.equals(constants.Constants.INITIATOR)){
                            manager.tell(TcpMessage.connect(remote), getSelf());
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .matchAny(messageAny -> {
                    try{
                        System.out.println("Peer msgAny => " + messageAny);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .build();
    }
    
    private AbstractActor.Receive act2(final ActorRef connection) {
        return receiveBuilder()
                .match(ByteString.class, msg -> {
                    System.out.println("Peer act2 ByteString.class => " + msg);
                })
                .match(Tcp.CommandFailed.class, msg -> {
                    // OS kernel socket buffer was full
                    System.out.println("Peer act2 CommandFailed.class => " + msg);
                })
                .match(Tcp.Received.class, msg -> {
                    System.out.println("Peer act2 Received.class => " + msg);
                    
                    byte[] data = msg.data().toArray();
                    System.out.println("Peer act2 data length : " + data.length + ", value : " + byteArrayToHexString(data));
                    
                    if(data.length == 50){
                        //----------------------Initiator Act 2 Start--------------------------------------
                        byte[] v = Arrays.copyOfRange(data, 0, 1);
                        System.out.println("Peer act2 Act 2 v length : " + v.length + ", value : " + byteArrayToHexString(v));
                        byte[] re = Arrays.copyOfRange(data, 1, 34);
                        System.out.println("Peer act2 Act 2 re length : " + re.length + ", value : " + byteArrayToHexString(re));
                        byte[] c = Arrays.copyOfRange(data, 34, 50);
                        System.out.println("Peer act2 Act 2 c length : " + c.length + ", value : " + byteArrayToHexString(c));

                        h = Hash.sha256(Utils.concatAll(h, re));
                        System.out.println("Peer act2 Act 2 h : " + byteArrayToHexString(h));
                        
                        byte[] ss = ECDH_BC.doECDH(e.prv(), re);
                        System.out.println("Peer act2 Act 2 ss : " + byteArrayToHexString(ss));

                        byte[] okm = HKDF.doHKDF(ck, ss);
                        System.out.println("Peer act2 Act 2 okm : " + byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        ck = Arrays.copyOfRange(okm, 0, 32);
                        temp_k2 = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Peer act2 Act 2 ck : " + byteArrayToHexString(ck));
                        System.out.println("Peer act2 Act 2 temp_k2 : " + byteArrayToHexString(temp_k2));
                        
                        byte[] p = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(temp_k2, Utils.getNonceFromInt(0), new byte[0], h, c);
                        System.out.println("Peer act2 Act 2 plain length : "+p.length+", value : "+byteArrayToHexString(p));
                        
                        h = Hash.sha256(Utils.concatAll(h, c));
                        System.out.println("Peer act2 Act 2 h : " + byteArrayToHexString(h));
                        //----------------------Initiator Act 2 End----------------------------------------
                        
                        //----------------------Initiator Act 3 Start--------------------------------------
                        byte[][] c1 = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(temp_k2, Utils.getNonceFromInt(1), ls.pub(), h);
                        System.out.println("Peer act2 Act 3 cipher length : "+c1[0].length+", value : "+byteArrayToHexString(c1[0]));
                        System.out.println("Peer act2 Act 3 tag length : "+c1[1].length+", value : "+byteArrayToHexString(c1[1]));
                        
                        h = Hash.sha256(Utils.concatAll(h, c1));
                        System.out.println("Peer act2 Act 3 h : " + byteArrayToHexString(h));
                        
                        ss = ECDH_BC.doECDH(ls.prv(), re);
                        System.out.println("Peer act2 Act 3 ss : " + byteArrayToHexString(ss));
                        
                        okm = HKDF.doHKDF(ck, ss);
                        System.out.println("Peer act2 Act 3 okm : " + byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        ck = Arrays.copyOfRange(okm, 0, 32);
                        byte[] temp_k3 = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Peer act2 Act 3 ck : " + byteArrayToHexString(ck));
                        System.out.println("Peer act2 Act 3 temp_k3 : " + byteArrayToHexString(temp_k3));
                        
                        byte[][] t = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(temp_k3, Utils.getNonceFromInt(0), new byte[0], h);
                        System.out.println("Peer act2 Act 3 t_cipher length : "+t[0].length+", value : "+byteArrayToHexString(t[0]));
                        System.out.println("Peer act2 Act 3 t_tag length : "+t[1].length+", value : "+byteArrayToHexString(t[1]));
                        
                        byte[] okm_k = HKDF.doHKDF(ck, new byte[0]);
                        System.out.println("Peer act2 Act 3 okm_k : " + byteArrayToHexString(okm_k) + ", Length : " + okm_k.length);
                        sk = Arrays.copyOfRange(okm_k, 0, 32);
                        rk = Arrays.copyOfRange(okm_k, 32, okm.length);
                        System.out.println("Peer act2 Act 3 sk (encryption key for initiator) : " + byteArrayToHexString(sk));
                        System.out.println("Peer act2 Act 3 rk (decryption key for initiator) : " + byteArrayToHexString(rk));
                        
                        byte[] m = new byte[1];
                        m[0] = 0;
                        byte[] act3Output = Utils.concatAll(Utils.concatAll(m, c1), t);
                        System.out.println("Peer act2 Act 3 act3Output length : "+act3Output.length+", value : "+byteArrayToHexString(act3Output));
                        connection.tell(TcpMessage.write(ByteString.fromArray(act3Output)), getSelf());
                        //----------------------Initiator Act 3 End----------------------------------------
                        
                        byte[][] lc = getCipher(hexStringToByteArray("0007"));
                        byte[][] cipher = getCipher(hexStringToByteArray("00100000000108"));
                        byte[] finalCipher = Utils.concatAll(Utils.concatAll(new byte[0], lc), cipher);
                        System.out.println("Peer act2 Init finalCipher length : "+finalCipher.length+", value : "+byteArrayToHexString(finalCipher));
                        connection.tell(TcpMessage.write(ByteString.fromArray(finalCipher)), getSelf());
                        getContext().become(communication(connection));
                    }else{
                        
                    }
                    
                })
                .matchEquals("close", msg -> {
                    System.out.println("Peer act2 close => " + msg);
                    connection.tell(TcpMessage.close(), getSelf());
                })
                .match(Tcp.ConnectionClosed.class, msg -> {
                    System.out.println("Peer act2 ConnectionClosed.class " + msg);
                    getContext().stop(getSelf());
                })
                .matchAny(msg -> {
                    System.out.println("Peer act2 Msg Any => " + msg);
                })
                .build();
    }
    
    private AbstractActor.Receive act3(final ActorRef connection) {
        return receiveBuilder()
                .match(ByteString.class, msg -> {
                    System.out.println("Peer act3 ByteString.class => " + msg);
                })
                .match(Tcp.CommandFailed.class, msg -> {
                    // OS kernel socket buffer was full
                    System.out.println("Peer act3 CommandFailed.class => " + msg);
                })
                .match(Tcp.Received.class, msg -> {
                    System.out.println("Peer act3 Received.class => " + msg);
                    
                    byte[] data = msg.data().toArray();
                    System.out.println("Peer act3 data length : " + data.length + ", value : " + byteArrayToHexString(data));
                    
                    if(data.length >= 66){
                        //----------------------Responder Act 3 Start--------------------------------------
                        byte[] v = Arrays.copyOfRange(data, 0, 1);
                        System.out.println("Peer act3 Act 3 v length : " + v.length + ", value : " + byteArrayToHexString(v));
                        byte[] c = Arrays.copyOfRange(data, 1, 50);
                        System.out.println("Peer act3 Act 3 c length : " + c.length + ", value : " + byteArrayToHexString(c));
                        byte[] t = Arrays.copyOfRange(data, 50, 66);
                        
                        System.out.println("Peer act3 Act 3 t length : " + t.length + ", value : " + byteArrayToHexString(t));
                        
                        byte[] ccipher = Arrays.copyOfRange(c, 0, 33);
                        System.out.println("Peer act3 Act 3 ccipher length : " + ccipher.length + ", value : " + byteArrayToHexString(ccipher));
                        byte[] cmac = Arrays.copyOfRange(c, 33, c.length);
                        System.out.println("Peer act3 Act 3 cmac length : " + cmac.length + ", value : " + byteArrayToHexString(cmac));
                        
                        byte[] rlsPub = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(temp_k2, Utils.getNonceFromInt(1), ccipher, h, cmac);
                        System.out.println("Peer act3 Act 3 rlsPub length : "+rlsPub.length+", value : "+byteArrayToHexString(rlsPub));
                        
                        h = Hash.sha256(Utils.concatAll(h, c));
                        System.out.println("Peer act3 Act 3 h : " + byteArrayToHexString(h));
                        
                        byte[] ss = ECDH_BC.doECDH(e.prv(), rlsPub);
                        System.out.println("Peer act3 Act 3 ss : " + byteArrayToHexString(ss));
                        
                        byte[] okm = HKDF.doHKDF(ck, ss);
                        System.out.println("Peer act3 Act 3 okm : " + byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        ck = Arrays.copyOfRange(okm, 0, 32);
                        byte[] temp_k3 = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Peer act3 Act 3 ck : " + byteArrayToHexString(ck));
                        System.out.println("Peer act3 Act 3 temp_k3 : " + byteArrayToHexString(temp_k3));
                        
                        byte[] p = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(temp_k3, Utils.getNonceFromInt(0), new byte[0], h, t);
                        System.out.println("Peer act3 Act 3 p : "+p.length+", value : "+byteArrayToHexString(p));
                        
                        okm = HKDF.doHKDF(ck, new byte[0]);
                        System.out.println("Peer act3 Act 3 okm : " + byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        rk = Arrays.copyOfRange(okm, 0, 32);
                        sk = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Peer act3 Act 3 rk (decryption key for responder): " + byteArrayToHexString(rk));
                        System.out.println("Peer act3 Act 3 sk (encryption key for responder) : " + byteArrayToHexString(sk));
                        //----------------------Responder Act 3 End----------------------------------------
                        
                        if(data.length > 66){
                            byte[] dmsg = Arrays.copyOfRange(data, 66, data.length);
                            System.out.println("Peer act3 Communication dmsg length : " + dmsg.length + ", value : " + byteArrayToHexString(dmsg));
                            
                            byte[] m = getPlain(dmsg);
                            System.out.println("Peer act3 Communication m length : " + m.length + ", value : " + byteArrayToHexString(m));
                            byte[] type = Arrays.copyOfRange(m, 0, 2);
                            byte[] original = Arrays.copyOfRange(m, 2, m.length);
                            System.out.println("Peer act3 Communication type length : " + type.length + ", value : " + byteArrayToHexString(type));
                            System.out.println("Peer act3 Communication original length : " + original.length + ", value : " + byteArrayToHexString(original));
                            
                            ByteBuffer typew = ByteBuffer.wrap(m);
                            short typep = typew.getShort();
                            System.out.println("Peer act3 Communication type : " + typep);
                            
                            System.out.println("Peer act3 Communication original : " + ByteUtils.toBase58(original));
                            System.out.println("Peer act3 Communication original : " + Utils.convertHexToString(byteArrayToHexString(original)));
                        }
                        
                        byte[][] lc = getCipher(hexStringToByteArray("0007"));
                        byte[][] cipher = getCipher(hexStringToByteArray("00100000000108"));
                        byte[] finalCipher = Utils.concatAll(Utils.concatAll(new byte[0], lc), cipher);
                        System.out.println("Peer act3 Init finalCipher length : "+finalCipher.length+", value : "+byteArrayToHexString(finalCipher));
                        connection.tell(TcpMessage.write(ByteString.fromArray(finalCipher)), getSelf());
                        
                        getContext().become(communication(connection));
                    }
                })
                .matchEquals("close", msg -> {
                    System.out.println("Peer act3 close => " + msg);
                    connection.tell(TcpMessage.close(), getSelf());
                })
                .match(Tcp.ConnectionClosed.class, msg -> {
                    System.out.println("Peer act3 ConnectionClosed.class " + msg);
                    getContext().stop(getSelf());
                })
                .matchAny(msg -> {
                    System.out.println("Peer act3 Msg Any => " + msg);
                })
                .build();
    }
    
    private AbstractActor.Receive communication(final ActorRef connection) {
        return receiveBuilder()
                .match(ByteString.class, msg -> {
                    System.out.println("Peer communication ByteString.class => " + msg);
                })
                .match(Tcp.CommandFailed.class, msg -> {
                    // OS kernel socket buffer was full
                    System.out.println("Peer communication CommandFailed.class => " + msg);
                })
                .match(Tcp.Received.class, msg -> {
                    System.out.println("Peer communication Received.class => " + msg);
                    byte[] data = msg.data().toArray();
                    System.out.println("Peer communication data length : " + data.length + ", value : " + byteArrayToHexString(data));
                    
                    byte[] m = getPlain(data);
                    System.out.println("Peer communication Communication m length : " + m.length + ", value : " + byteArrayToHexString(m));
                    byte[] type = Arrays.copyOfRange(m, 0, 2);
                    byte[] original = Arrays.copyOfRange(m, 2, m.length);
                    System.out.println("Peer communication Communication type length : " + type.length + ", value : " + byteArrayToHexString(type));
                    System.out.println("Peer communication Communication original length : " + original.length + ", value : " + byteArrayToHexString(original));

                    ByteBuffer typew = ByteBuffer.wrap(type);
                    short typep = typew.getShort();
                    System.out.println("Peer communication Communication type : " + typep);
                    
                    switch(typep){
                        case 16 : System.out.println("Init Message");
                            init = new Init(original);
                            break;
                        case 17 : System.out.println("Error Message");
                            error = new msg.Error(original);
                            break;
                        case 18 : System.out.println("Ping Message");
                            ping = new Ping(original);
                            byte[] pongMsg = getPongMsg(original);
                            connection.tell(TcpMessage.write(ByteString.fromArray(pongMsg)), getSelf());
                            break;
                        case 19 : System.out.println("Pong Message");
                            pong = new Pong(original);
                            break;
                        case 32 : System.out.println("Open Channel Message");
                            openChannel = new OpenChannel(original);
                            acceptChannel = new AcceptChannel(openChannel, keys);
                            keys.generate(openChannel);
                            connection.tell(TcpMessage.write(ByteString.fromArray(getCipherPacket(acceptChannel.getMessage()))), getSelf());
                            break;
                        case 33 : System.out.println("Accept Channel Message");
                            acceptChannel = new AcceptChannel(original);
                            keys.generate(acceptChannel);
                            byte[] ctn33 = Hash.sha256(Utils.concatAll(openChannel.getPaymentBasepoint(), acceptChannel.getPaymentBasepoint()));
                            System.out.println("Accept Channel Message => ctn : "+Utils.byteArrayToHexString(ctn33));
                            byte[] seq33 = Arrays.copyOfRange(ctn33, 26, ctn33.length-3);
                            System.out.println("Accept Channel Message => seq : "+Utils.byteArrayToHexString(seq33));
                            byte[] locktime33 = Arrays.copyOfRange(ctn33, 29, ctn33.length);
                            System.out.println("Accept Channel Message => locktime : "+Utils.byteArrayToHexString(locktime33));
                            fundingCreated = new FundingCreated(openChannel, acceptChannel, bt, keys, seq33, locktime33);
                            connection.tell(TcpMessage.write(ByteString.fromArray(getCipherPacket(fundingCreated.getMessage()))), getSelf());
                            break;
                        case 34 : System.out.println("Funding Created Message");
                            fundingCreated = new FundingCreated(original);
                            byte[] ctn = Hash.sha256(Utils.concatAll(openChannel.getPaymentBasepoint(), acceptChannel.getPaymentBasepoint()));
                            System.out.println("Funding Created Message => ctn : "+Utils.byteArrayToHexString(ctn));
                            byte[] seq = Arrays.copyOfRange(ctn, 26, ctn.length-3);
                            System.out.println("Funding Created Message => seq : "+Utils.byteArrayToHexString(seq));
                            byte[] locktime = Arrays.copyOfRange(ctn, 29, ctn.length);
                            System.out.println("Funding Created Message => locktime : "+Utils.byteArrayToHexString(locktime));
                            if(bt.sigVerify(fundingCreated.getFundingTxid(), fundingCreated.getFundingOutputIndex(), openChannel.getFundingSatoshis(), openChannel.getToSelfDelay(), keys.getRevocationPubKey(), keys.getLocalDelayedPubKey(), keys.getRemotePubKey(), openChannel.getFundingPubkey(), acceptChannel.getFundingPubkey(), fundingCreated.getSignatureA(), openChannel.getFeeratePerKw(), seq, locktime)){
                                fundingTxIdStatus = new FundingTxIdStatus(Utils.byteArrayToHexString(fundingCreated.getFundingTxid()), getSelf(), ByteBuffer.wrap(acceptChannel.getMinimumDepth()).getInt());
                                zmqManager.tell(fundingTxIdStatus, getSelf());
                                byte[] sig = bt.sigGenerate(fundingCreated.getFundingTxid(), fundingCreated.getFundingOutputIndex(), openChannel.getFundingSatoshis(), openChannel.getToSelfDelay(), keys.getRevocationPubKey(), keys.getTheirDelayedPubKey(), keys.getRemotePubKey(), openChannel.getFundingPubkey(), acceptChannel.getFundingPubkey(), fundingCreated.getSignatureA(), openChannel.getFeeratePerKw(), seq, locktime, keys.getLocalFundingPrvKey());
                                fundingSigned = new FundingSigned(fundingCreated, sig);
                                System.out.println("Funding Created Message => fundingSigned Signature : "+Utils.byteArrayToHexString(sig));
                                connection.tell(TcpMessage.write(ByteString.fromArray(getCipherPacket(fundingSigned.getMessage()))), getSelf());
                            }else{
                                System.out.println("Funding Created Failed. Invalid Signature");
                            }
                            break;
                        case 35 : System.out.println("Funding Signed Message");
                            fundingSigned = new FundingSigned(original);
                            break;
                        case 36 : System.out.println("Funding Locked Message");
                            fundingLocked = new FundingLocked(original);
                            break;
                        case 38 : System.out.println("Shutdown Message");
                            shutdown = new Shutdown(original);
                            break;
                        case 39 : System.out.println("Closing Signed Message");
                            closingSigned = new ClosingSigned(original);
                            break;
                        case 128 : System.out.println("Update Add Htlc Message");
                            updateAddHtlc = new UpdateAddHtlc(original);
                            break;
                        case 130 : System.out.println("Update Fulfill Htlc Message");
                            updateFulfillHtlc = new UpdateFulfillHtlc(original);
                            break;
                        case 131 : System.out.println("Update Fail Htlc Message");
                            updateFailHtlc = new UpdateFailHtlc(original);
                            break;
                        case 132 : System.out.println("Commitment Signed Message");
                            commitmentSigned = new CommitmentSigned(original);
                            break;
                        case 133 : System.out.println("Revoke And Ack Message");
                            revokeAndAck = new RevokeAndAck(original);
                            break;
                        case 134 : System.out.println("Update Fee Message");
                            updateFee = new UpdateFee(original);
                            break;
                        case 135 : System.out.println("Update Fail Malformed Htlc Message");
                            updateFailMalformedHtlc = new UpdateFailMalformedHtlc(original);
                            break;
                        case 136 : System.out.println("Channel Reestablish Message");
                            channelReestablish = new ChannelReestablish(original);
                            break;
                        case 256 : System.out.println("Channel Announcement Message");
                            channelAnnouncement = new ChannelAnnouncement(original);
                            break; 
                        case 257 : System.out.println("Node Announcement Message");
                            nodeAnnouncement = new NodeAnnouncement(original);
                            break; 
                        case 258 : System.out.println("Channel Update Message");
                            channelUpdate = new ChannelUpdate(original);
                            break; 
                        case 259 : System.out.println("Announcement Signatures Message");
                            announcementSignatures = new AnnouncementSignatures(original);
                            break; 
                        default : System.out.println("In Default type = "+typep);
                            break;
                    }

                    System.out.println("Peer communication Communication original.toBase58 : " + ByteUtils.toBase58(original));
                    //System.out.println("Peer communication Communication original : " + Utils.convertHexToString(byteArrayToHexString(original)));
                })
                .match(InterceptPublishMessage.class, message -> {
                    System.out.println("Initiator InterceptPublishMessage.class => " + message.getTopicName());
                    if(message.getTopicName().equals("ligntning/initiator/openchannel")){
                        String msg = new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8"));
                        String[] msgA = msg.split(",");
                        if(msgA.length == 4){// IP, port, pubKey, amount
                            openChannel = new OpenChannel(bt, keys, msgA[3]);
                            connection.tell(TcpMessage.write(ByteString.fromArray(getCipherPacket(openChannel.getMessage()))), getSelf());
                        }
                    }
                })
                .match(FundingTxIdStatus.class, message -> {
                    System.out.println("Peer FundingTxIdStatus.class => " + message.getFtxid());
                })
                .matchEquals("close", msg -> {
                    System.out.println("Peer communication close => " + msg);
                    connection.tell(TcpMessage.close(), getSelf());
                })
                .matchEquals("SEND_PING", msg -> {
                    System.out.println("Peer communication SEND_PING => " + msg);
                    byte[] pingMsg = getCipherPacket(hexStringToByteArray("001200100000"));
                    System.out.println("Peer SEND_PING pingMsg length : "+pingMsg.length+", value : "+byteArrayToHexString(pingMsg));
                    connection.tell(TcpMessage.write(ByteString.fromArray(pingMsg)), getSelf());
                    /*byte[][] lc = getCipher(hexStringToByteArray("0006"));
                    byte[][] cipher = getCipher(hexStringToByteArray("001200100000"));
                    byte[] finalCipher = Utils.concatAll(Utils.concatAll(new byte[0], lc), cipher);
                    System.out.println("Peer SEND_PING Ping finalCipher length : "+finalCipher.length+", value : "+byteArrayToHexString(finalCipher));
                    connection.tell(TcpMessage.write(ByteString.fromArray(finalCipher)), getSelf());*/
                })
                .match(Tcp.ConnectionClosed.class, msg -> {
                    System.out.println("Peer communication ConnectionClosed.class " + msg);
                    getContext().stop(getSelf());
                })
                .matchAny(msg -> {
                    System.out.println("Peer communication Msg Any => " + msg);
                })
                .build();
    }
    
    public byte[] getPlain(byte[] cipher){
        byte[] lc = Arrays.copyOfRange(cipher, 0, 18);
        System.out.println("Peer getPlain lc length : " + lc.length + ", value : " + byteArrayToHexString(lc));
        byte[] lcd = Arrays.copyOfRange(lc, 0, 2);
        byte[] lcm = Arrays.copyOfRange(lc, 2, 18);
        byte[] lb = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(rk, Utils.getNonceFromInt(rn), lcd, new byte[0], lcm);
        rn++;
        System.out.println("Peer getPlain lb : " + byteArrayToHexString(lb));
        short l = ByteBuffer.wrap(lb).getShort();
        System.out.println("Peer getPlain data length : " + l);

        byte[] cmsg = Arrays.copyOfRange(cipher, 18, 18+l+16);
        System.out.println("Peer getPlain cmsg length : " + cmsg.length + ", value : " + byteArrayToHexString(cmsg));
        byte[] cmsgd = Arrays.copyOfRange(cmsg, 0, l);
        byte[] cmsgm = Arrays.copyOfRange(cmsg, l, l+16);
        
        byte[] plain = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(rk, Utils.getNonceFromInt(rn), cmsgd, new byte[0], cmsgm);
        rn++;
        return plain;
    }
    
    public byte[] getCipherPacket(byte[] plain){
        byte[][] lc = getCipher(Utils.getHexOfShortWithBytes((short) plain.length, 2));
        byte[][] cipher = getCipher(plain);
        byte[] finalCipher = Utils.concatAll(Utils.concatAll(new byte[0], lc), cipher);
        System.out.println("Peer getCipherPacket length : "+finalCipher.length+", value : "+byteArrayToHexString(finalCipher));
        return finalCipher;
    }
    
    public byte[][] getCipher(byte[] plain){
        byte[][] cipher = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(sk, Utils.getNonceFromInt(sn), plain, new byte[0]);
        sn++;
        return cipher;
    }
    
    public byte[][] getCipherWithAd(byte[] plain, byte[] ad){
        byte[][] cipher = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(sk, Utils.getNonceFromInt(sn), plain, ad);
        sn++;
        return cipher;
    }
    
    public byte[] getPongMsg(byte[] pingMsg){
        byte[] bytePongBytes = Arrays.copyOfRange(pingMsg, 0, 2);
        //byte[] byteslen = Arrays.copyOfRange(pingMsg, 2, 4);
        short numPongBytes = ByteBuffer.wrap(bytePongBytes).getShort();
        System.out.println("Peer getPongMsg numPongBytes : " + numPongBytes);
        byte[] bytesmsg = new byte[numPongBytes];
        byte[] byteslen = Utils.getHexOfShortWithBytes((short) numPongBytes, 2);
        byte[] msg = Utils.concatAll(byteslen, bytesmsg);
        byte[] type = Utils.getHexOfShortWithBytes((short) 19, 2);
        byte[] pongMsg = Utils.concatAll(type, msg);
        /*byte[][] lc = getCipher(Utils.getHexOfShortWithBytes((short) pongMsg.length, 2));
        byte[][] cipher = getCipher(pongMsg);
        byte[] finalCipher = Utils.concatAll(Utils.concatAll(new byte[0], lc), cipher);*/
        byte[] finalCipher = getCipherPacket(pongMsg);
        System.out.println("Peer getPongMsg length : "+finalCipher.length+", value : "+byteArrayToHexString(finalCipher));
        return finalCipher;
    }
}
