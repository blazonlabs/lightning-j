/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.tcp;

/**
 *
 * @author neerajnagi
 */
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.io.Tcp.Bound;
import akka.io.Tcp.CommandFailed;
import akka.io.Tcp.Connected;
import akka.io.Tcp.Received;
import akka.io.TcpMessage;
import akka.util.ByteString;
import ec.ByteUtils;
import ec.ChaCha20Poly1305;
import ec.ECDH_BC;
import static ec.HKDF.hkdfExpand;
import static ec.HKDF.hkdfExtract;
import ec.Hash;
import ec.KeyUtils;
import ec.Utils;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.security.KeyPair;
import java.util.Arrays;

public class Responder extends AbstractActor {
    
    final ActorRef manager;
    byte[] h;
    byte[] ck;
    //KeyPair epair;
    byte[] lsPrv;
    byte[] lsPub;
    byte[] temp_k2;
    long rn = 0;
    byte sn = 0;
    byte[] ePrv = null ;
    byte[] ePub = null;
    
    public Responder(ActorRef manager, byte[] lsPrv, byte[] lsPub) {//, ActorRef handler) {
        System.out.println("Responder Constructor Start");
        this.manager = manager;
        this.lsPrv = lsPrv;
        this.lsPub = lsPub;
        System.out.println("Responder Constructor End");
    }

    public static Props props(ActorRef manager) {
        return Props.create(Responder.class, manager);
    }

    @Override
    public void preStart() throws Exception {
        //final ActorRef tcp = Tcp.get(getContext().getSystem()).manager();
        System.out.println("Responder PreStart Start");
        //epair = new KeyUtils().getNewKeyPair();
        //lsPrv = KeyUtils.savePrivateKey(epair.getPrivate());
        //lsPub = KeyUtils.savePublicKey(epair.getPublic());
        //System.out.println("Responder ls.prv : " + Utils.byteArrayToHexString(lsPrv));
        //System.out.println("Responder ls.pub : " + Utils.byteArrayToHexString(lsPub));
        h = Hash.sha256(constants.Constants.protocolName.getBytes());
        ck = h;
        h = Hash.sha256(Utils.concatAll(h, constants.Constants.prolouge.getBytes()));
        h = Hash.sha256(Utils.concatAll(h, lsPub));
        manager.tell(TcpMessage.bind(getSelf(), new InetSocketAddress(9735), 100), getSelf());
        System.out.println("Responder PreStart End");
        
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Received.class, msg -> {
                    System.out.println("Responder Received.class => " + msg);
                    
                    //byte[] data = Utils.hexStringToByteArray(msg.data().decodeString("UTF-8"));
                    byte[] data = msg.data().toArray();
                    System.out.println("Responder data length : " + data.length + ", value : " + Utils.byteArrayToHexString(data));
                    
                    if(data.length == 50){
                        //----------------------Responder Act 1 Start--------------------------------------
                        byte[] v = Arrays.copyOfRange(data, 0, 1);
                        System.out.println("Responder Act 1 v length : " + v.length + ", value : " + Utils.byteArrayToHexString(v));
                        byte[] re = Arrays.copyOfRange(data, 1, 34);
                        System.out.println("Responder Act 1 re length : " + re.length + ", value : " + Utils.byteArrayToHexString(re));
                        byte[] c = Arrays.copyOfRange(data, 34, 50);
                        System.out.println("Responder Act 1 c length : " + c.length + ", value : " + Utils.byteArrayToHexString(c));

                        h = Hash.sha256(Utils.concatAll(h, re));
                        System.out.println("Responder Act 1 h : " + Utils.byteArrayToHexString(h));
                        
                        byte[] ss = ECDH_BC.doECDH(lsPrv, re);
                        System.out.println("Responder Act 1 ss : " + Utils.byteArrayToHexString(ss));

                        byte[] prk = hkdfExtract(ck, ss);
                        System.out.println("Responder Act 1 prk : " + Utils.byteArrayToHexString(prk));
                        byte[] okm = hkdfExpand(prk, new byte[0], 64);
                        System.out.println("Responder Act 1 okm : " + Utils.byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        ck = Arrays.copyOfRange(okm, 0, 32);
                        byte[] temp_k1 = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Responder Act 1 ck : " + Utils.byteArrayToHexString(ck));
                        System.out.println("Responder Act 1 temp_k1 : " + Utils.byteArrayToHexString(temp_k1));
                        
                        byte[] nonce = Utils.hexStringToByteArray("000000000000000000000000");
                        byte[] p = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(temp_k1, nonce, new byte[0], h, c);
                        System.out.println("Responder Act 1 plain length : "+p.length+", value : "+Utils.byteArrayToHexString(p));
                        
                        h = Hash.sha256(Utils.concatAll(h, c));
                        System.out.println("Responder Act 1 h : " + Utils.byteArrayToHexString(h));
                        //----------------------Responder Act 1 End----------------------------------------
                        
                        //----------------------Responder Act 2 Start--------------------------------------
                        KeyPair e = new KeyUtils().getNewKeyPair();
                        ePrv = KeyUtils.savePrivateKey(e.getPrivate());
                        ePub = KeyUtils.savePublicKey(e.getPublic());
                        //ePrv = Utils.hexStringToByteArray("2222222222222222222222222222222222222222222222222222222222222222");
                        //ePub = Utils.hexStringToByteArray("02466d7fcae563e5cb09a0d1870bb580344804617879a14949cf22285f1bae3f27");
                        System.out.println("Responder Act 2 ePrv : " + Utils.byteArrayToHexString(ePrv));
                        System.out.println("Responder Act 2 ePub : " + Utils.byteArrayToHexString(ePub));

                        h = Hash.sha256(Utils.concatAll(h, ePub));
                        System.out.println("Responder Act 2 h : " + Utils.byteArrayToHexString(h));       

                        ss = ECDH_BC.doECDH(ePrv, re);
                        System.out.println("Responder Act 2 ss : " + Utils.byteArrayToHexString(ss));

                        byte[] prk2 = hkdfExtract(ck, ss);
                        System.out.println("Responder Act 2 prk : " + Utils.byteArrayToHexString(prk2));
                        byte[] okm2 = hkdfExpand(prk2, new byte[0], 64);
                        System.out.println("Responder Act 2 okm : " + Utils.byteArrayToHexString(okm2) + ", okm Length : " + okm2.length);
                        ck = Arrays.copyOfRange(okm2, 0, 32);
                        temp_k2 = Arrays.copyOfRange(okm2, 32, okm2.length);
                        System.out.println("Responder Act 2 ck : " + Utils.byteArrayToHexString(ck));
                        System.out.println("Responder Act 2 temp_k2 : " + Utils.byteArrayToHexString(temp_k2));
                        
                        byte[] plaintext = new byte[0];
                        byte[][] c2 = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(temp_k2, nonce, plaintext, h);
                        System.out.println("Responder Act 2 cipher length : "+c2[0].length+", value : "+Utils.byteArrayToHexString(c2[0]));
                        System.out.println("Responder Act 2 tag length : "+c2[1].length+", value : "+Utils.byteArrayToHexString(c2[1]));
                        
                        h = Hash.sha256(Utils.concatAll(h, c2));
                        
                        byte[] m = new byte[1];
                        m[0] = 0;
                        byte[] act2Output = Utils.concatAll(Utils.concatAll(m, ePub), c2);
                        System.out.println("Responder Act 2 act2Output length : "+act2Output.length+", value : "+Utils.byteArrayToHexString(act2Output));
                        
                        getSender().tell(TcpMessage.write(ByteString.fromArray(act2Output)), getSelf());
                        //----------------------Responder Act 2 End----------------------------------------
                    }
                    if(data.length >= 66){
                        //----------------------Responder Act 3 Start--------------------------------------
                        byte[] v = Arrays.copyOfRange(data, 0, 1);
                        System.out.println("Responder Act 3 v length : " + v.length + ", value : " + Utils.byteArrayToHexString(v));
                        byte[] c = Arrays.copyOfRange(data, 1, 50);
                        System.out.println("Responder Act 3 c length : " + c.length + ", value : " + Utils.byteArrayToHexString(c));
                        byte[] t = Arrays.copyOfRange(data, 50, 66);
                        
                        System.out.println("Responder Act 3 t length : " + t.length + ", value : " + Utils.byteArrayToHexString(t));
                        
                        byte[] ccipher = Arrays.copyOfRange(c, 0, 33);
                        System.out.println("Responder Act 3 ccipher length : " + ccipher.length + ", value : " + Utils.byteArrayToHexString(ccipher));
                        byte[] cmac = Arrays.copyOfRange(c, 33, c.length);
                        System.out.println("Responder Act 3 cmac length : " + cmac.length + ", value : " + Utils.byteArrayToHexString(cmac));
                        
                        byte[] nonce1 = Utils.hexStringToByteArray("000000000100000000000000");
                        byte[] rlsPub = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(temp_k2, nonce1, ccipher, h, cmac);
                        System.out.println("Responder Act 3 rlsPub length : "+rlsPub.length+", value : "+Utils.byteArrayToHexString(rlsPub));
                        
                        h = Hash.sha256(Utils.concatAll(h, c));
                        System.out.println("Responder Act 3 h : " + Utils.byteArrayToHexString(h));
                        
                        byte[] ss = ECDH_BC.doECDH(ePrv, rlsPub);
                        System.out.println("Responder Act 3 ss : " + Utils.byteArrayToHexString(ss));
                        
                        byte[] prk = hkdfExtract(ck, ss);
                        System.out.println("Responder Act 3 prk : " + Utils.byteArrayToHexString(prk));
                        byte[] okm = hkdfExpand(prk, new byte[0], 64);
                        System.out.println("Responder Act 3 okm : " + Utils.byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        ck = Arrays.copyOfRange(okm, 0, 32);
                        byte[] temp_k3 = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Responder Act 3 ck : " + Utils.byteArrayToHexString(ck));
                        System.out.println("Responder Act 3 temp_k3 : " + Utils.byteArrayToHexString(temp_k3));
                        
                        byte[] nonce = Utils.hexStringToByteArray("000000000000000000000000");
                        byte[] p = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(temp_k3, nonce, new byte[0], h, t);
                        System.out.println("Responder Act 3 p : "+p.length+", value : "+Utils.byteArrayToHexString(p));
                        
                        prk = hkdfExtract(ck, new byte[0]);
                        System.out.println("Responder Act 3 prk : " + Utils.byteArrayToHexString(prk));
                        okm = hkdfExpand(prk, new byte[0], 64);
                        System.out.println("Responder Act 3 okm : " + Utils.byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        byte[] rk = Arrays.copyOfRange(okm, 0, 32);
                        byte[] sk = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Responder Act 3 rk (decryption key for responder): " + Utils.byteArrayToHexString(rk));
                        System.out.println("Responder Act 3 sk (encryption key for responder) : " + Utils.byteArrayToHexString(sk));
                        //----------------------Responder Act 3 End----------------------------------------
                        
                        if(data.length > 66){
                            byte[] dmsg = Arrays.copyOfRange(data, 66, data.length);
                            System.out.println("Responder Communication dmsg length : " + dmsg.length + ", value : " + Utils.byteArrayToHexString(dmsg));
                            
                            byte[] lc = Arrays.copyOfRange(dmsg, 0, 18);
                            System.out.println("Responder Communication lc length : " + lc.length + ", value : " + Utils.byteArrayToHexString(lc));
                            byte[] lcd = Arrays.copyOfRange(lc, 0, 2);
                            byte[] lcm = Arrays.copyOfRange(lc, 2, 18);
                            byte[] sn = Utils.hexStringToByteArray("000000000000000000000000");
                            byte[] lb = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(rk, sn, lcd, new byte[0], lcm);
                            System.out.println("Responder Communication lb : " + Utils.byteArrayToHexString(lb));
                            ByteBuffer lw = ByteBuffer.wrap(lb);
                            short l = lw.getShort();
                            System.out.println("Responder Communication data length : " + l);
                            
                            byte[] cmsg = Arrays.copyOfRange(dmsg, 18, 18+l+16);
                            System.out.println("Responder Communication cmsg length : " + cmsg.length + ", value : " + Utils.byteArrayToHexString(cmsg));
                            byte[] cmsgd = Arrays.copyOfRange(cmsg, 0, l);
                            byte[] cmsgm = Arrays.copyOfRange(cmsg, l, l+16);
                            sn = Utils.hexStringToByteArray("000000000100000000000000");
                            byte[] m = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(rk, sn, cmsgd, new byte[0], cmsgm);
                            System.out.println("Responder Communication m length : " + m.length + ", value : " + Utils.byteArrayToHexString(m));
                            byte[] type = Arrays.copyOfRange(m, 0, 2);
                            byte[] original = Arrays.copyOfRange(m, 2, m.length);
                            System.out.println("Responder Communication type length : " + type.length + ", value : " + Utils.byteArrayToHexString(type));
                            System.out.println("Responder Communication original length : " + original.length + ", value : " + Utils.byteArrayToHexString(original));
                            
                            ByteBuffer typew = ByteBuffer.wrap(m);
                            short typep = typew.getShort();
                            System.out.println("Responder Communication type : " + typep);
                            
                            System.out.println("Responder Communication original : " + ByteUtils.toBase58(original));
                            System.out.println("Responder Communication original : " + Utils.convertHexToString(Utils.byteArrayToHexString(original)));
                        }
                    }
                })
                .match(Bound.class, msg -> {
                    System.out.println("Responder Bound.class => " + msg);
                    manager.tell(msg, getSelf());

                })
                .match(CommandFailed.class, msg -> {
                    System.out.println("Responder CommandFailed.class => " + msg);
                    getContext().stop(getSelf());

                })
                .match(Connected.class, conn -> {
                    System.out.println("Responder Connected.class => " + conn.remoteAddress());
                    manager.tell(conn, getSelf());
                    //final ActorRef handler = getContext().actorOf(Props.create(Listener.class));
                    getSender().tell(TcpMessage.register(getSelf()), getSelf());
                })
                .matchAny(msgAny -> {
                    try{
                        System.out.println("Responder msgAny => " + msgAny.toString());
                        
                        
                        
                       // ByteString msg = (ByteString) msgAny;
                       // System.out.println("Responder msg => " + msg.decodeString("UTF-8"));
                        
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    
                    
                    
                })
                .build();
    }

}
