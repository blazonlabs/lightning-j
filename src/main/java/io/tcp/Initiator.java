/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.tcp;

import java.net.InetSocketAddress;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.io.Tcp.CommandFailed;
import akka.io.Tcp.Connected;
import akka.io.Tcp.ConnectionClosed;
import akka.io.Tcp.Received;
import akka.io.TcpMessage;
import akka.util.ByteString;
import ec.ByteUtils;
import ec.ChaCha20Poly1305;
import ec.ECDH_BC;
import static ec.HKDF.hkdfExpand;
import static ec.HKDF.hkdfExtract;
import ec.Hash;
import ec.KeyUtils;
import ec.Utils;
import java.nio.ByteBuffer;
import java.security.KeyPair;
import java.util.Arrays;

/**
 *
 * @author neerajnagi
 */
public class Initiator extends AbstractActor {

    final InetSocketAddress remote;
    final ActorRef manager;
    byte[] h;
    byte[] ck;
    //KeyPair epair;
    byte[] lsPrv;
    byte[] lsPub;
    byte[] ePrv;
    byte[] ePub;
    int sn = 0;
    int rn = 0;
    ByteString imsg;
    byte[] sk;
    byte[] rk;

    public static Props props(InetSocketAddress remote, ActorRef listener) {
        return Props.create(Initiator.class, remote, listener);
    }

    public Initiator(InetSocketAddress remote, ActorRef manager, byte[] lsPrv, byte[] lsPub) {
        System.err.println("Initiator Constructor Start");
        this.remote = remote;
        this.manager = manager;
        this.lsPrv = lsPrv;
        this.lsPub = lsPub;
        //manager.tell(TcpMessage.connect(remote), getSelf());
        System.err.println("Initiator Constructor End");
    }

    @Override
    public void preStart() throws Exception {
        //final ActorRef tcp = Tcp.get(getContext().getSystem()).manager();
        System.out.println("Initiator PreStart Start");
        //epair = new KeyUtils().getNewKeyPair();
        //lsPrv = KeyUtils.savePrivateKey(epair.getPrivate());
        //lsPub = KeyUtils.savePublicKey(epair.getPublic());
        //System.out.println("Initiator ls.prv : " + ECDH_BC.byteArrayToHexString(lsPrv));
        //System.out.println("Initiator ls.pub : " + ECDH_BC.byteArrayToHexString(lsPub));
        h = Hash.sha256(constants.Constants.protocolName.getBytes());
        ck = h;
        h = Hash.sha256(Utils.concatAll(h, constants.Constants.prolouge.getBytes()));
        System.out.println("Initiator PreStart End");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CommandFailed.class, msg -> {
                    //listener.tell("Initiator failed", getSelf());
                    System.err.println("Initiator CommandFailed.class => " + msg);
                    getContext().stop(getSelf());
                })
                .match(Connected.class, message -> {
                    System.out.println("Initiator Connected.class => " + message);
                    //listener.tell(msg, getSelf());
                    getSender().tell(TcpMessage.register(getSelf()), getSelf());
                    try{
                        System.out.println("Initiator ByteString.class => " + imsg.decodeString("UTF-8"));
                        
                        //----------------------Initiator Act 1 Start--------------------------------------
                        byte[] rspub = Utils.hexStringToByteArray(imsg.decodeString("UTF-8"));
                        System.out.println("Initiator rspub : " + Utils.byteArrayToHexString(rspub));

                        h = Hash.sha256(Utils.concatAll(h, rspub));
                        System.out.println("Initiator h : " + Utils.byteArrayToHexString(h));

                        KeyPair e = new KeyUtils().getNewKeyPair();
                        ePrv = KeyUtils.savePrivateKey(e.getPrivate());
                        ePub = KeyUtils.savePublicKey(e.getPublic());
                        //ePrv = ECDH_BC.hexStringToByteArray("001212121212121212121212121212121212121212121212121212121212121212");
                        //ePub = ECDH_BC.hexStringToByteArray("036360e856310ce5d294e8be33fc807077dc56ac80d95d9cd4ddbd21325eff73f7");
                        System.out.println("Initiator Act 1 ePrv : " + Utils.byteArrayToHexString(ePrv));
                        System.out.println("Initiator Act 1 ePub : " + Utils.byteArrayToHexString(ePub));

                        h = Hash.sha256(Utils.concatAll(h, ePub));
                        System.out.println("Initiator Act 1 h : " + Utils.byteArrayToHexString(h));       

                        byte[] ss = ECDH_BC.doECDH(ePrv, rspub);
                        System.out.println("Initiator Act 1 ss : " + Utils.byteArrayToHexString(ss));

                        byte[] prk = hkdfExtract(ck, ss);
                        System.out.println("Initiator Act 1 prk : " + Utils.byteArrayToHexString(prk));
                        byte[] okm = hkdfExpand(prk, new byte[0], 64);
                        System.out.println("Initiator Act 1 okm : " + Utils.byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        ck = Arrays.copyOfRange(okm, 0, 32);
                        byte[] temp_k1 = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Initiator Act 1 ck : " + Utils.byteArrayToHexString(ck));
                        System.out.println("Initiator Act 1 temp_k1 : " + Utils.byteArrayToHexString(temp_k1));
                        
                        byte[] nonce = Utils.hexStringToByteArray("000000000000000000000000");
                        byte[] plaintext = new byte[0];
                        byte[][] c = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(temp_k1, nonce, plaintext, h);
                        System.out.println("Initiator Act 1 cipher length : "+c[0].length+", value : "+Utils.byteArrayToHexString(c[0]));
                        System.out.println("Initiator Act 1 tag length : "+c[1].length+", value : "+Utils.byteArrayToHexString(c[1]));
                        
                        h = Hash.sha256(Utils.concatAll(h, c));
                        
                        byte[] m = new byte[1];
                        m[0] = 0;
                        byte[] act1Output = Utils.concatAll(Utils.concatAll(m, ePub), c);
                        System.out.println("Initiator Act 1 act1Output length : "+act1Output.length+", value : "+Utils.byteArrayToHexString(act1Output));
                        
                        //getSender().tell(TcpMessage.write(ByteString.fromString(ECDH_BC.convertHexToString(ECDH_BC.byteArrayToHexString(act1Output)))), getSelf());
                        getSender().tell(TcpMessage.write(ByteString.fromArray(act1Output)), getSelf());
                        //----------------------Initiator Act 1 End----------------------------------------+
                        getContext().become(act2(getSender()));
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    //getContext().become(connected(getSender()));
                })
                .match(String.class, msg -> {
                    System.out.println("Initiator String.class => " + msg);
                    //listener.tell(msg, getSelf());
                })
                .match(ByteString.class, msg -> {
                    imsg = msg;
                   manager.tell(TcpMessage.connect(remote), getSelf()); 
                    
                    //connection.tell(TcpMessage.write((ByteString) msg), getSelf());
                })
                .matchAny(msg -> {
                    System.out.println("Initiator Any Msg => " + msg);
                    //listener.tell(msg, getSelf());
                })
                .build();
    }

    private Receive act2(final ActorRef connection) {
        return receiveBuilder()
                .match(ByteString.class, msg -> {

                    System.out.println("Initiator ByteString.class => " + msg);
                    
                    byte[] initdata = msg.toArray();
                    System.out.println("Initiator ByteString.class initdata length : " + initdata.length + ", value : " + Utils.byteArrayToHexString(initdata));
                    
                    
                    //connection.tell(TcpMessage.write((ByteString) msg), getSelf());
                })
                .match(CommandFailed.class, msg -> {
                    // OS kernel socket buffer was full
                    System.out.println("Initiator CommandFailed.class => " + msg);
                })
                .match(Received.class, msg -> {
                    System.out.println("Initiator Received.class => " + msg);
                    
                    //byte[] data = ECDH_BC.hexStringToByteArray(msg.data().decodeString("UTF-8"));
                    byte[] data = msg.data().toArray();
                    System.out.println("Initiator data length : " + data.length + ", value : " + Utils.byteArrayToHexString(data));
                    
                    if(data.length == 50){
                        //----------------------Initiator Act 2 Start--------------------------------------
                        byte[] v = Arrays.copyOfRange(data, 0, 1);
                        System.out.println("Initiator Act 2 v length : " + v.length + ", value : " + Utils.byteArrayToHexString(v));
                        byte[] re = Arrays.copyOfRange(data, 1, 34);
                        System.out.println("Initiator Act 2 re length : " + re.length + ", value : " + Utils.byteArrayToHexString(re));
                        byte[] c = Arrays.copyOfRange(data, 34, 50);
                        System.out.println("Initiator Act 2 c length : " + c.length + ", value : " + Utils.byteArrayToHexString(c));

                        h = Hash.sha256(Utils.concatAll(h, re));
                        System.out.println("Initiator Act 2 h : " + Utils.byteArrayToHexString(h));
                        
                        byte[] ss = ECDH_BC.doECDH(ePrv, re);
                        System.out.println("Initiator Act 2 ss : " + Utils.byteArrayToHexString(ss));

                        byte[] prk = hkdfExtract(ck, ss);
                        System.out.println("Initiator Act 2 prk : " + Utils.byteArrayToHexString(prk));
                        byte[] okm = hkdfExpand(prk, new byte[0], 64);
                        System.out.println("Initiator Act 2 okm : " + Utils.byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        ck = Arrays.copyOfRange(okm, 0, 32);
                        byte[] temp_k2 = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Initiator Act 2 ck : " + Utils.byteArrayToHexString(ck));
                        System.out.println("Initiator Act 2 temp_k2 : " + Utils.byteArrayToHexString(temp_k2));
                        
                        byte[] nonce = Utils.hexStringToByteArray("000000000000000000000000");
                        byte[] p = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(temp_k2, nonce, new byte[0], h, c);
                        System.out.println("Initiator Act 2 plain length : "+p.length+", value : "+Utils.byteArrayToHexString(p));
                        
                        h = Hash.sha256(Utils.concatAll(h, c));
                        System.out.println("Initiator Act 2 h : " + Utils.byteArrayToHexString(h));
                        //----------------------Initiator Act 2 End----------------------------------------
                        
                        //----------------------Initiator Act 3 Start--------------------------------------
                        byte[] nonce1 = Utils.hexStringToByteArray("000000000100000000000000");
                        byte[][] c1 = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(temp_k2, nonce1, lsPub, h);
                        System.out.println("Initiator Act 3 cipher length : "+c1[0].length+", value : "+Utils.byteArrayToHexString(c1[0]));
                        System.out.println("Initiator Act 3 tag length : "+c1[1].length+", value : "+Utils.byteArrayToHexString(c1[1]));
                        
                        h = Hash.sha256(Utils.concatAll(h, c1));
                        System.out.println("Initiator Act 3 h : " + Utils.byteArrayToHexString(h));
                        
                        ss = ECDH_BC.doECDH(lsPrv, re);
                        System.out.println("Initiator Act 3 ss : " + Utils.byteArrayToHexString(ss));
                        
                        prk = hkdfExtract(ck, ss);
                        System.out.println("Initiator Act 3 prk : " + Utils.byteArrayToHexString(prk));
                        okm = hkdfExpand(prk, new byte[0], 64);
                        System.out.println("Initiator Act 3 okm : " + Utils.byteArrayToHexString(okm) + ", okm Length : " + okm.length);
                        ck = Arrays.copyOfRange(okm, 0, 32);
                        byte[] temp_k3 = Arrays.copyOfRange(okm, 32, okm.length);
                        System.out.println("Initiator Act 3 ck : " + Utils.byteArrayToHexString(ck));
                        System.out.println("Initiator Act 3 temp_k3 : " + Utils.byteArrayToHexString(temp_k3));
                        
                        byte[][] t = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(temp_k3, nonce, new byte[0], h);
                        System.out.println("Initiator Act 3 t_cipher length : "+t[0].length+", value : "+Utils.byteArrayToHexString(t[0]));
                        System.out.println("Initiator Act 3 t_tag length : "+t[1].length+", value : "+Utils.byteArrayToHexString(t[1]));
                        
                        byte[] prk_k = hkdfExtract(ck, new byte[0]);
                        System.out.println("Initiator Act 3 prk_k : " + Utils.byteArrayToHexString(prk_k));
                        byte[] okm_k = hkdfExpand(prk_k, new byte[0], 64);
                        System.out.println("Initiator Act 3 okm_k : " + Utils.byteArrayToHexString(okm_k) + ", Length : " + okm_k.length);
                        sk = Arrays.copyOfRange(okm_k, 0, 32);
                        rk = Arrays.copyOfRange(okm_k, 32, okm.length);
                        System.out.println("Initiator Act 3 sk (encryption key for initiator) : " + Utils.byteArrayToHexString(sk));
                        System.out.println("Initiator Act 3 rk (decryption key for initiator) : " + Utils.byteArrayToHexString(rk));
                        
                        byte[] m = new byte[1];
                        m[0] = 0;
                        byte[] act3Output = Utils.concatAll(Utils.concatAll(m, c1), t);
                        System.out.println("Initiator Act 3 act3Output length : "+act3Output.length+", value : "+Utils.byteArrayToHexString(act3Output));
                        //connection.tell(TcpMessage.write(ByteString.fromString(ECDH_BC.byteArrayToHexString(act3Output))), getSelf());
                        connection.tell(TcpMessage.write(ByteString.fromArray(act3Output)), getSelf());
                        //----------------------Initiator Act 3 End----------------------------------------
                        
                        byte[] plain = Utils.hexStringToByteArray("00100000000108");
                        byte[] sn = Utils.hexStringToByteArray("000000000000000000000000");
                        byte[] l = Utils.hexStringToByteArray("0007");
                        byte[][] lc = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(sk, sn, l, new byte[0]);

                        sn = Utils.hexStringToByteArray("000000000100000000000000");
                        byte[][] cipher = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(sk, sn, plain, new byte[0]);

                        byte[] finalCipher = Utils.concatAll(Utils.concatAll(new byte[0], lc), cipher);
                        System.out.println("Initiator Init finalCipher length : "+finalCipher.length+", value : "+Utils.byteArrayToHexString(finalCipher));
                        connection.tell(TcpMessage.write(ByteString.fromArray(finalCipher)), getSelf());
                        
                    }else{
                        System.out.println("Initiator Communication dmsg length : " + data.length + ", value : " + Utils.byteArrayToHexString(data));
                            
                        byte[] lc = Arrays.copyOfRange(data, 0, 18);
                        System.out.println("Initiator Communication lc length : " + lc.length + ", value : " + Utils.byteArrayToHexString(lc));
                        byte[] lcd = Arrays.copyOfRange(lc, 0, 2);
                        byte[] lcm = Arrays.copyOfRange(lc, 2, 18);
                        //byte[] sn = hexStringToByteArray("000000000000000000000000");
                        byte[] sn = Utils.getNonceFromInt(rn);
                        rn++;
                        System.out.println("Initiator Communication nonce length : " + sn.length + ", value : " + Utils.byteArrayToHexString(sn));
                        byte[] lb = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(rk, sn, lcd, new byte[0], lcm);
                        System.out.println("Initiator Communication lb : " + Utils.byteArrayToHexString(lb));
                        ByteBuffer lw = ByteBuffer.wrap(lb);
                        short l = lw.getShort();
                        System.out.println("Initiator Communication data length : " + l);
                            
                        byte[] cmsg = Arrays.copyOfRange(data, 18, 18+l+16);
                        System.out.println("Initiator Communication cmsg length : " + cmsg.length + ", value : " + Utils.byteArrayToHexString(cmsg));
                        byte[] cmsgd = Arrays.copyOfRange(cmsg, 0, l);
                        byte[] cmsgm = Arrays.copyOfRange(cmsg, l, l+16);
                        
                        //sn = hexStringToByteArray("000000000100000000000000");
                        sn = Utils.getNonceFromInt(rn);
                        rn++;
                        System.out.println("Initiator Communication nonce length : " + sn.length + ", value : " + Utils.byteArrayToHexString(sn));
                        byte[] m = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(rk, sn, cmsgd, new byte[0], cmsgm);
                        System.out.println("Initiator Communication m length : " + m.length + ", value : " + Utils.byteArrayToHexString(m));
                        byte[] type = Arrays.copyOfRange(m, 0, 2);
                        byte[] original = Arrays.copyOfRange(m, 2, m.length);
                        System.out.println("Initiator Communication type length : " + type.length + ", value : " + Utils.byteArrayToHexString(type));
                        System.out.println("Initiator Communication original length : " + original.length + ", value : " + Utils.byteArrayToHexString(original));
                            
                        ByteBuffer typew = ByteBuffer.wrap(m);
                        short typep = typew.getShort();
                        System.out.println("Initiator Communication type : " + typep);
                            
                        System.out.println("Initiator Communication original : " + ByteUtils.toBase58(original));
                        System.out.println("Initiator Communication original : " + Utils.convertHexToString(Utils.byteArrayToHexString(original)));
                    }
                    
                })
                .matchEquals("close", msg -> {
                    System.out.println("Initiator close => " + msg);
                    connection.tell(TcpMessage.close(), getSelf());
                })
                .match(ConnectionClosed.class, msg -> {
                    System.out.println("Initiator ConnectionClosed.class " + msg);
                    getContext().stop(getSelf());
                })
                .matchAny(msg -> {
                    System.out.println("Initiator Msg Any => " + msg);
                    //connection.tell(TcpMessage.write((ByteString) msg), getSelf());
                })
                .build();
    }

}
