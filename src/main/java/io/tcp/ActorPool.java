/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.tcp;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.RoundRobinPool;
import java.util.HashMap;
import misc.ActorSystemContainer;

/**
 *
 * @author Abhimanyu
 */
public class ActorPool {

    final static ActorSystem system = ActorSystemContainer.getInstance().getSystem();

    public static enum actor {
        Peer, ZmqManager, MqttServer, InitiatorTemp, Initiator, ResponderTemp, Responder
    };

    public static HashMap<actor, ActorRef> actRef = new HashMap<actor, ActorRef>();
    public static HashMap<String, ActorRef> actRefByString = new HashMap<String, ActorRef>();

    public synchronized static ActorRef get(actor enu) {
        ActorRef ret = null;
        try {
            switch (enu) {
                default:
                    if (actRef.containsKey(enu)) {
                        ret = actRef.get(enu);
                    } else {
                        ret = system.actorOf(Props.create(Class.forName("actors." + enu.toString())).withDispatcher("dispatcher.blocking-dispatcher"), enu.toString());
                        actRef.put(enu, ret);
                    }
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error in ActorPool : ");
            e.printStackTrace();
        }
        return ret;
    }
    
    public synchronized static ActorRef get(actor enu, String path) {
        ActorRef ret = null;
        try {
            switch (enu) {
                default:
                    if (actRef.containsKey(enu)) {
                        ret = actRef.get(enu);
                    } else {
                        ret = system.actorOf(Props.create(Class.forName(path + "." + enu.toString())), enu.toString());
                        actRef.put(enu, ret);
                    }
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error in ActorPool : ");
            e.printStackTrace();
        }
        return ret;
    }

    public synchronized static ActorRef get(String className, String actorName) {
        ActorRef ret = null;
        try {
            if (actRefByString.containsKey(className + "_" + actorName)) {
                ret = actRefByString.get(className + "_" + actorName);
            } else {
                ret = system.actorOf(Props.create(Class.forName("actors." + className)).withDispatcher("dispatcher.blocking-dispatcher"), actorName);
                actRefByString.put(className + "_" + actorName, ret);
            }
        } catch (Exception e) {
            System.out.println("Error in ActorPool get(String, String): ");
            e.printStackTrace();
        }

        return ret;
    }

    public synchronized static ActorRef get(String className, String actorName, int poolSize) {
        ActorRef ret = null;
        try {
            if (actRefByString.containsKey(className + "_" + actorName)) {
                ret = actRefByString.get(className + "_" + actorName);
            } else {
                //	ret = system.actorOf(Props.create(Class.forName("actors."+className)), actorName);
                ret = system.actorOf(new RoundRobinPool(poolSize).props(Props.create(Class.forName(className)).withDispatcher("dispatcher.blocking-dispatcher")), actorName);
                actRefByString.put(className + "_" + actorName, ret);
            }
        } catch (Exception e) {
            System.out.println("Error in ActorPool get(String, String): ");
            e.printStackTrace();
        }

        return ret;
    }
    
    public synchronized static void set(actor enu, ActorRef ref) {
        actRef.put(enu, ref);
    }
}
