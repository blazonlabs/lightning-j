/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.tcp;

/**
 *
 * @author neerajnagi
 */
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.io.Tcp.Bound;
import akka.io.Tcp.CommandFailed;
import akka.io.Tcp.Connected;
import akka.io.Tcp.Received;
import akka.io.TcpMessage;
import ec.KeyUtils;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import misc.ActorSystemContainer;
import scala.concurrent.duration.Duration;

public class ResponderTemp extends AbstractActor {

    final ActorSystem system = ActorSystemContainer.getInstance().getSystem();
    final ActorRef manager;
    KeyUtils ls;
    HashMap<String, ActorRef> peers = new HashMap<String, ActorRef>();
    
    public ResponderTemp(ActorRef manager, KeyUtils ls) {//, ActorRef handler) {
        System.out.println("Responder Constructor Start");
        this.manager = manager;
        this.ls = ls;
        System.out.println("Responder Constructor End");
    }

    public static Props props(ActorRef manager) {
        return Props.create(ResponderTemp.class, manager);
    }

    @Override
    public void preStart() throws Exception {
        System.out.println("Responder PreStart Start");
        manager.tell(TcpMessage.bind(getSelf(), new InetSocketAddress(9735), 100), getSelf());
        system.scheduler().schedule(Duration.Zero(), Duration.create(30, TimeUnit.SECONDS  ), getSelf(), "SEND_PING", system.dispatcher(), null);
        System.out.println("Responder PreStart End");
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(Received.class, message -> {
                    System.out.println("Responder Received.class => " + message);
                })
                .match(Bound.class, message -> {
                    System.out.println("Responder Bound.class => " + message);
                    manager.tell(message, getSelf());
                })
                .match(CommandFailed.class, message -> {
                    System.out.println("Responder CommandFailed.class => " + message);
                    getContext().stop(getSelf());
                })
                .match(Connected.class, conn -> {
                    System.out.println("Responder Connected.class => " + conn.toString());
                    manager.tell(conn, getSelf());
                    final ActorRef handler = getContext().actorOf(Props.create(Peer.class, manager, ls, constants.Constants.RESPONDER));
                    peers.put(conn.toString(), handler);
                    getSender().tell(TcpMessage.register(handler), getSelf());
                })
                .matchEquals("SEND_PING", message -> {
                    System.out.println("Responder SEND_PING => " + message);
                    if(message.equals("SEND_PING")){
                        System.out.println("Responder getSelf => " + getSelf().path().toStringWithoutAddress());
                        ActorSelection ps = system.actorSelection(getSelf().path().toStringWithoutAddress()+"/*");
                        System.out.println("Responder SEND_PING => " + ps.toString());
                        ps.tell("SEND_PING", getSelf());
                    }
                })
                .match(String.class, message -> {
                    try{
                        System.out.println("Responder String.class => " + message);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .matchAny(msgAny -> {
                    try{
                        System.out.println("Responder msgAny => " + msgAny);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .build();
    }

}
