/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.tcp;

import java.net.InetSocketAddress;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.io.Tcp.CommandFailed;
import akka.io.Tcp.Connected;
import akka.io.TcpMessage;
import akka.util.ByteString;
import ec.KeyUtils;
import io.moquette.interception.messages.InterceptPublishMessage;
import io.netty.buffer.ByteBufUtil;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import misc.ActorSystemContainer;
import msg.CmdToInitiator;
import scala.concurrent.duration.Duration;

/**
 *
 * @author neerajnagi
 */
public class InitiatorTemp extends AbstractActor {

    final ActorSystem system = ActorSystemContainer.getInstance().getSystem();
    final InetSocketAddress remote = null;
    final ActorRef manager;
    KeyUtils ls;
    HashMap<String, ActorRef> peers = new HashMap<String, ActorRef>();
    
    public static Props props(InetSocketAddress remote, ActorRef listener) {
        return Props.create(InitiatorTemp.class, remote, listener);
    }

    public InitiatorTemp(ActorRef manager, KeyUtils ls) {
        System.out.println("Initiator Constructor Start");
        this.manager = manager;
        this.ls = ls;
        System.out.println("Initiator Constructor End");
    }

    @Override
    public void preStart() throws Exception {
        System.out.println("Initiator PreStart Start");
        system.scheduler().schedule(Duration.Zero(), Duration.create(30, TimeUnit.SECONDS  ), getSelf(), "SEND_PING", system.dispatcher(), null);
        System.out.println("Initiator PreStart End");
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(CommandFailed.class, message -> {
                    System.out.println("Initiator CommandFailed.class => " + message);
                    getContext().stop(getSelf());
                })
                .match(Connected.class, message -> {
                    System.out.println("Initiator Connected.class => " + message);
                })
                .match(CmdToInitiator.class, message -> {
                    System.out.println("Initiator CmdToInitiator.class => " + message);
                    final ActorRef handler = getContext().actorOf(Props.create(Peer.class, manager, ls, constants.Constants.INITIATOR));
                    String conn = message.getIp() + "_" + message.getPort() + "_" + message.getRspub();
                    peers.put(conn, handler);
                    handler.tell(message, getSelf());
                })
                .match(InterceptPublishMessage.class, message -> {
                    System.out.println("Initiator InterceptPublishMessage.class => " + message.getTopicName());
                    if(message.getTopicName().equals("ligntning/initiator/openchannel")){
                        String msg = new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8"));
                        String[] msgA = msg.split(",");
                        if(msgA.length == 4){// IP, port, pubKey, amount
                            String conn = msgA[0] + "_" + msgA[1] + "_" + msgA[2];
                            if(peers.containsKey(conn)){
                                peers.get(conn).tell(message, getSelf());
                            }
                        }
                    }
                })
                .match(InetSocketAddress.class, addr -> {
                    System.out.println("Initiator InetSocketAddress.class => " + addr.toString());
                })
                .match(ByteString.class, message -> {
                    System.out.println("Initiator ByteString.class => " + message);
                })
                .matchEquals("SEND_PING", message -> {
                    System.out.println("Initiator SEND_PING => " + message);
                    if(message.equals("SEND_PING")){
                        System.out.println("Initiator getSelf => " + getSelf().path().toStringWithoutAddress());
                        ActorSelection ps = system.actorSelection(getSelf().path().toStringWithoutAddress()+"/*");
                        System.out.println("Initiator SEND_PING => " + ps.toString());
                        ps.tell("SEND_PING", getSelf());
                    }
                })
                .matchAny(message -> {
                    System.out.println("Initiator Any Msg => " + message);
                })
                .build();
    }
}
