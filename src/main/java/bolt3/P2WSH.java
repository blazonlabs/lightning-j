/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bolt3;

/*
unsigned
02000000
01
a70c54e52301921900ac3e4bad0acb7d685f1fdfca71b8688a1a3f8c293ad6e100000000
00
ffffffff
01
805fed5302000000
19
76a914e8ebbf20ad0d51c525c23f5835cdb1e36cd12d4b88ac
00000000

signed
02000000
00
01
01
a70c54e52301921900ac3e4bad0acb7d685f1fdfca71b8688a1a3f8c293ad6e100000000
00
ffffffff
01
805fed5302000000
19
76a914e8ebbf20ad0d51c525c23f5835cdb1e36cd12d4b88ac
04
00
48
3045022100e8bbbf9cc9888c4a07ccd3f79c9c8837b81b64a0b4b5daffd38a0b5d3b9895e0022011420c1c7a10fe13deaef6ee1b802013297bcc2e41c7ba6972025c7ae09dd5d801
47
304402201dbcc5382f92ac20d3ce335b094ac7f62b8e765a7c877756c6dd0ecefcd9eeac02201f6c51668af506dcd38ee7369197e114580f2ae1ea111535c6b65f25ef7eb8da01
47
5221032fe658bffbd541b08f4302994857e6e45a3ff682d42c5213b5955b80905c30162103ff4d94b2e80e19346bb73ae58c4838253662611ad06bd4cbcf26b4d5a2a7add252ae
00000000

*/

/**
 *
 * @author neerajnagi
 */
//single input singleoutput 
import java.math.BigInteger;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import static org.junit.Assert.*;
import ec.ECDH_BC;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import javax.xml.bind.DatatypeConverter;
import opcodes.ScriptOpCodes;
import org.bitcoin.NativeSecp256k1;
import org.bitcoin.NativeSecp256k1Util;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Sha256Hash;


public class P2WSH extends TXN{
    Bolt3Util.NetworkType netType;
    String version="02000000";
    String marker="00";
    String flag="01";
    String inputCount="01";
    String prevTxid;
    String prevVout;
    String voutConst="000000";
    String scriptSigSize="00";
    String sequence="ffffffff";
    String outputCount="01";
    String amount;
    String satoshis;
    String scriptPubKeySize;
    String scriptPubKey;
    String numberOfWitness;
    String witness;
    String locktime="00000000";
    
    public String hextoDoubleHash(String _){
        
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(_))));
    }
    
    public String signedHex(ECKey e, String pubKey, String amt){

        System.out.println(String.format("pubKey: %s",pubKey));
        String scriptPub = pubKey.substring(2);
        System.out.println(String.format("pKey: %s",scriptPub));
        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
        String outputs = String.format("%s%s%s",this.satoshis,this.scriptPubKeySize,this.scriptPubKey);
        
        String hashPrevouts = hextoDoubleHash(prevOuts);
        String hashSequence = hextoDoubleHash(this.sequence);
        String scriptCode = String.format("1976a9%s88ac",scriptPub);
        String amount= P2PKH.reverse(P2PKH.btcToSatoshi(amt));;
        String hashOutputs= hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
        String msg = hextoDoubleHash(hashPreImage);
        
        Sha256Hash hash = Sha256Hash.wrap(msg);
        ECKey.ECDSASignature s = e.sign(hash);
        byte[] res = s.encodeToDER();
        String test = DatatypeConverter.printHexBinary(res);
        String sig = test.toLowerCase();
        
        System.out.println(String.format("--->> %s",sig));
        sig = sig + "01";
        String publicKey = ec.Utils.byteArrayToHexString(e.getPubKey());
        this.numberOfWitness="02";
        this.witness = String.format("%s%s%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(sig.length()/2)),sig,P2PKH.compactSizeUint(BigInteger.valueOf(publicKey.length()/2)),publicKey);

        
        return ec.Utils.byteArrayToHexString(this.hex2());
    }
    
    public String signedHex(ECKey[] e, String redeemScript, String amt){

        String signatures = signature(e, redeemScript, amt);
        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
        int witnessLength = e.length + 2;
        this.numberOfWitness=String.format("%02X",witnessLength);
        this.witness = String.format("00%s%s%s",signatures,P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);

        
        return ec.Utils.byteArrayToHexString(this.hex2());
    }
    
    public String signature(ECKey[] e, String redeemScript, String amt){

        System.out.println(String.format("redeem script: %s",redeemScript));
        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
        System.out.println(String.format("redeem script hex: %s",redeemScriptHex));
        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
        String outputs = String.format("%s%s%s",this.satoshis,this.scriptPubKeySize,this.scriptPubKey);
        
        String hashPrevouts = hextoDoubleHash(prevOuts);
        String hashSequence = hextoDoubleHash(this.sequence);
        String scriptCode = String.format("%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);
        String amount= P2PKH.reverse(P2PKH.btcToSatoshi(amt));;
        String hashOutputs= hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
        String msg = hextoDoubleHash(hashPreImage);
        int arrLength = e.length;
        String[] sig = new String[arrLength];
        String signatures="";
        for (int i = 0; i < e.length; i++) {
            
            Sha256Hash hash = Sha256Hash.wrap(msg);
            ECKey.ECDSASignature s = e[i].sign(hash);
            byte[] res = s.encodeToDER();
            String test = DatatypeConverter.printHexBinary(res);
            sig[i] = test.toLowerCase();
        
            System.out.println(String.format("--->> %s",sig[i]));
            sig[i] = sig[i] + "01";
            signatures = signatures + P2PKH.compactSizeUint(BigInteger.valueOf(sig[i].length()/2)) + sig[i];
        }
        return signatures;
    }
    
    public boolean verifySignature(String redeemScript, String amt, String signature, String pubKey) throws NativeSecp256k1Util.AssertFailException{

        System.out.println(String.format("redeem script: %s",redeemScript));
        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
        System.out.println(String.format("redeem script hex: %s",redeemScriptHex));
        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
        String outputs = String.format("%s%s%s",this.satoshis,this.scriptPubKeySize,this.scriptPubKey);
        
        String hashPrevouts = hextoDoubleHash(prevOuts);
        String hashSequence = hextoDoubleHash(this.sequence);
        String scriptCode = String.format("%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);
        String amount= P2PKH.reverse(amt);
        String hashOutputs= hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
        String msg = hextoDoubleHash(hashPreImage);
       
            
        //Sha256Hash hash = Sha256Hash.wrap(msg);
        //ECKey.ECDSASignature s;
        
        boolean result = NativeSecp256k1.verify(ec.Utils.hexStringToByteArray(msg), ec.Utils.hexStringToByteArray(amount), ec.Utils.hexStringToByteArray(pubKey));
        
        return result;
    }
    
    public static String scriptToHash(String _){
        String scriptHex = P2PKH.scriptToHex(_);
           
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Utils.hexStringToByteArray(scriptHex)));
    }
    
    public P2WSH(){}
    
    public  P2WSH(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String redeemScript, String amount){
        this.netType = netType;
        this.prevTxid = P2PKH.reverse(prevTxid);
        this.prevVout = P2PKH.reverse(prevVout);
        this.satoshis = P2PKH.reverse(P2PKH.btcToSatoshi(amount));
        
        this.scriptPubKey = String.format("0020%s", scriptToHash(redeemScript));
        this.scriptPubKeySize = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length()/2));
    }
    
    public  P2WSH(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String redeemScript, String amount, String flag){
        this.netType = netType;
        this.prevTxid = P2PKH.reverse(prevTxid);
        this.prevVout = P2PKH.reverse(prevVout);
        this.satoshis = P2PKH.reverse(amount);
        
        this.scriptPubKey = String.format("0020%s", scriptToHash(redeemScript));
        this.scriptPubKeySize = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length()/2));
    }
    
    public  byte[] hex(){
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(byteStream);
        pw.write(version);
        pw.write(inputCount);
        pw.write(prevTxid);
        pw.write(prevVout);
        pw.write(voutConst);
        pw.write(scriptSigSize);
        pw.write(sequence);
        pw.write(outputCount);
        pw.write(satoshis);
        pw.write(scriptPubKeySize);
        pw.write(scriptPubKey);
        pw.write(locktime);
        pw.flush();
        //System.out.println(byteStream.toString());
        return ec.Utils.hexStringToByteArray(byteStream.toString());
    }
    
    public  byte[] hex2(){
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(byteStream);
        pw.write(version);
        pw.write(marker);
        pw.write(flag);
        pw.write(inputCount);
        pw.write(prevTxid);
        pw.write(prevVout);
        pw.write(voutConst);
        pw.write(scriptSigSize);
        pw.write(sequence);
        pw.write(outputCount);
        pw.write(satoshis);
        pw.write(scriptPubKeySize);
        pw.write(scriptPubKey);
        pw.write(numberOfWitness);
        pw.write(witness);
        pw.write(locktime);
        pw.flush();
        //System.out.println(byteStream.toString());
        return ec.Utils.hexStringToByteArray(byteStream.toString());
    }
    
    public static void main(String args[]) throws Exception{
        
        //System.out.println(pkhFromBC32Address("2MvUdNwyzWRnSoULDAZcuf3qBFS2DEWvdmq"));
        //Bech32Util bu = Bech32Util.getInstance();
        //SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        //Pair<byte[], byte[]> temp = bu.bech32Decode("bcrt1q7kzlltmmdpwwtt6f72aer03czgmqylg8maj42p");
        //System.out.println(String.format("%s     %s",ec.Utils.byteArrayToHexString(temp.getLeft()),ec.Utils.byteArrayToHexString(temp.getRight())));
        //System.out.println(ec.Utils.byteArrayToHexString(Bolt3Util.fromBase58("f585ffaf7b685ce5af49f2bb91be381236027d07")));
        //System.out.println(ec.Utils.byteArrayToHexString( su.decode("bcrt","bcrt1q7kzlltmmdpwwtt6f72aer03czgmqylg8maj42p" ).getRight() ));
    }
}