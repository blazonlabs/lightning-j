/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bolt3;

/*
unsigned
02000000
01
f92c3e01e748077b9f3ab5f27cb288f1c515ef2cee64857eb9bbcf59e92e7042
00000000
00
ffffffff
01
c0a1fc5302000000
16
0014cb3216a4a1998e545f96e35f3cd8fadee13640bc
00000000

signed
02000000
00
01
01
f92c3e01e748077b9f3ab5f27cb288f1c515ef2cee64857eb9bbcf59e92e704200000000
00
ffffffff
01
c0a1fc5302000000
16
0014cb3216a4a1998e545f96e35f3cd8fadee13640bc
02
47
3044022028a1b8957abc66647465e5835ea11f1892979e214855a2adc6a7d4b90b397bec02200a4b48472e9a07df46d9ae97fe33125daba4d810b7e1e5257b3c2e7810f696be01
21
0396047fa0932c618c67e7bad0c0e4c05c9f3096f49ea6ebf4bb04ef6f2dfefa0b
00000000

*/

/**
 *
 * @author neerajnagi
 */
//single input singleoutput 
import java.math.BigInteger;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import static org.junit.Assert.*;
import ec.ECDH_BC;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import javax.xml.bind.DatatypeConverter;
import opcodes.ScriptOpCodes;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Sha256Hash;


public class P2WPKH extends TXN{
    String version="02000000";
    String marker="00";
    String flag="01";
    String inputCount="01";
    String prevTxid;
    String prevVout;
    String voutConst="000000";
    String scriptSigSize="00";
    String sequence="ffffffff";
    String outputCount="01";
    String amount;
    String satoshis;
    String scriptPubKeySize;
    String scriptPubKey;
    String numberOfWitness;
    String witness;
    String locktime="00000000";
    
    public static String pkhFromBC32Address(String _) throws Exception{
        
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        return ec.Utils.byteArrayToHexString( su.decode("bcrt",_ ).getRight() );
    }
    
    public String hextoDoubleHash(String _){
        
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(_))));
    }
    
    public String signedHex(ECKey e, String pubKey, String amt){

        System.out.println(String.format("pubKey: %s",pubKey));
        String scriptPub = pubKey.substring(2);
        System.out.println(String.format("pKey: %s",scriptPub));
        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
        String outputs = String.format("%s%s%s",this.satoshis,this.scriptPubKeySize,this.scriptPubKey);
        
        String hashPrevouts = hextoDoubleHash(prevOuts);
        String hashSequence = hextoDoubleHash(this.sequence);
        String scriptCode = String.format("1976a9%s88ac",scriptPub);
        String amount= P2PKH.reverse(P2PKH.btcToSatoshi(amt));;
        String hashOutputs= hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
        String msg = hextoDoubleHash(hashPreImage);
        
        Sha256Hash hash = Sha256Hash.wrap(msg);
        ECKey.ECDSASignature s = e.sign(hash);
        byte[] res = s.encodeToDER();
        String test = DatatypeConverter.printHexBinary(res);
        String sig = test.toLowerCase();
        
        System.out.println(String.format("--->> %s",sig));
        sig = sig + "01";
        String publicKey = ec.Utils.byteArrayToHexString(e.getPubKey());
        this.numberOfWitness="02";
        this.witness = String.format("%s%s%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(sig.length()/2)),sig,P2PKH.compactSizeUint(BigInteger.valueOf(publicKey.length()/2)),publicKey);

        
        return ec.Utils.byteArrayToHexString(this.hex2());
    }
 
    public String signedHex(ECKey[] e, String redeemScript, String amt){

        System.out.println(String.format("redeem script: %s",redeemScript));
        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
        System.out.println(String.format("redeem script hex: %s",redeemScriptHex));
        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
        String outputs = String.format("%s%s%s",this.satoshis,this.scriptPubKeySize,this.scriptPubKey);
        
        String hashPrevouts = hextoDoubleHash(prevOuts);
        String hashSequence = hextoDoubleHash(this.sequence);
        String scriptCode = String.format("%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);
        String amount= P2PKH.reverse(P2PKH.btcToSatoshi(amt));;
        String hashOutputs= hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
        String msg = hextoDoubleHash(hashPreImage);
        int arrLength = e.length;
        String[] sig = new String[arrLength];
        String signatures="";
        for (int i = 0; i < e.length; i++) {
            
            Sha256Hash hash = Sha256Hash.wrap(msg);
            ECKey.ECDSASignature s = e[i].sign(hash);
            byte[] res = s.encodeToDER();
            String test = DatatypeConverter.printHexBinary(res);
            sig[i] = test.toLowerCase();
        
            System.out.println(String.format("--->> %s",sig[i]));
            sig[i] = sig[i] + "01";
            signatures = signatures + P2PKH.compactSizeUint(BigInteger.valueOf(sig[i].length()/2)) + sig[i];
        }
        
        this.numberOfWitness="04";
        this.witness = String.format("00%s%s%s",signatures,P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);

        
        return ec.Utils.byteArrayToHexString(this.hex2());
    }
    
    public P2WPKH(){}
    
    public  P2WPKH(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String rxAddress, String amount) throws Exception{
        this.prevTxid = P2PKH.reverse(prevTxid);
        this.prevVout = P2PKH.reverse(prevVout);
        this.amount = amount;
        /*_amount = _amount * 100000000;
        this.amount = _amount.longValue();*/
        this.satoshis = P2PKH.reverse(P2PKH.btcToSatoshi(amount));
        this.scriptPubKey = String.format("00%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(pkhFromBC32Address(rxAddress).length()/2)) ,pkhFromBC32Address(rxAddress));
        this.scriptPubKeySize = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length()/2));
        
    }
    /*
     public P2WPKH(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String rxAddress, String amount, String scriptSig){
        this.prevTxid = P2PKH.reverse(prevTxid);
        this.prevVout = P2PKH.reverse(prevVout);
        this.satoshis = P2PKH.reverse(P2PKH.btcToSatoshi(amount));
        this.scriptPubKey = String.format("00%s%s", pkhFromAddress(rxAddress));
        this.scriptPubKeySize = compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length()/2));
        //<sig><pk>
        this.scriptSig = scriptToHex( scriptSig);
        this.scriptSigSize = compactSizeUint(BigInteger.valueOf(scriptSig.length()/2));
    }
    */
    public  byte[] hex(){
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(byteStream);
        pw.write(version);
        pw.write(inputCount);
        pw.write(prevTxid);
        pw.write(prevVout);
        pw.write(voutConst);
        pw.write(scriptSigSize);
        pw.write(sequence);
        pw.write(outputCount);
        pw.write(satoshis);
        pw.write(scriptPubKeySize);
        pw.write(scriptPubKey);
        pw.write(locktime);
        pw.flush();
        //System.out.println(byteStream.toString());
        return ec.Utils.hexStringToByteArray(byteStream.toString());
    }
    
    public  byte[] hex2(){
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(byteStream);
        pw.write(version);
        pw.write(marker);
        pw.write(flag);
        pw.write(inputCount);
        pw.write(prevTxid);
        pw.write(prevVout);
        pw.write(voutConst);
        pw.write(scriptSigSize);
        pw.write(sequence);
        pw.write(outputCount);
        pw.write(satoshis);
        pw.write(scriptPubKeySize);
        pw.write(scriptPubKey);
        pw.write(numberOfWitness);
        pw.write(witness);
        pw.write(locktime);
        pw.flush();
        //System.out.println(byteStream.toString());
        return ec.Utils.hexStringToByteArray(byteStream.toString());
    }
    
    public static void main(String args[]) throws Exception{
        
        //System.out.println(pkhFromBC32Address("2MvUdNwyzWRnSoULDAZcuf3qBFS2DEWvdmq"));
        //Bech32Util bu = Bech32Util.getInstance();
        //SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        //Pair<byte[], byte[]> temp = bu.bech32Decode("bcrt1q7kzlltmmdpwwtt6f72aer03czgmqylg8maj42p");
        //System.out.println(String.format("%s     %s",ec.Utils.byteArrayToHexString(temp.getLeft()),ec.Utils.byteArrayToHexString(temp.getRight())));
        //System.out.println(ec.Utils.byteArrayToHexString(Bolt3Util.fromBase58("f585ffaf7b685ce5af49f2bb91be381236027d07")));
        //System.out.println(ec.Utils.byteArrayToHexString( su.decode("bcrt","bcrt1q7kzlltmmdpwwtt6f72aer03czgmqylg8maj42p" ).getRight() ));
    }
}