/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bolt3;

/*
unsigned
02000000
01
a70c54e52301921900ac3e4bad0acb7d685f1fdfca71b8688a1a3f8c293ad6e100000000
00
ffffffff
01
805fed5302000000
19
76a914e8ebbf20ad0d51c525c23f5835cdb1e36cd12d4b88ac
00000000

signed
02000000
01
a70c54e52301921900ac3e4bad0acb7d685f1fdfca71b8688a1a3f8c293ad6e100000000
da
00
48
3045022100e8bbbf9cc9888c4a07ccd3f79c9c8837b81b64a0b4b5daffd38a0b5d3b9895e0022011420c1c7a10fe13deaef6ee1b802013297bcc2e41c7ba6972025c7ae09dd5d801
47
304402201dbcc5382f92ac20d3ce335b094ac7f62b8e765a7c877756c6dd0ecefcd9eeac02201f6c51668af506dcd38ee7369197e114580f2ae1ea111535c6b65f25ef7eb8da01
47
5221032fe658bffbd541b08f4302994857e6e45a3ff682d42c5213b5955b80905c30162103ff4d94b2e80e19346bb73ae58c4838253662611ad06bd4cbcf26b4d5a2a7add252ae
ffffffff
01
805fed5302000000
19
76a914e8ebbf20ad0d51c525c23f5835cdb1e36cd12d4b88ac
00000000


*/

/**
 *
 * @author neerajnagi
 */
//single input singleoutput 
import static bolt3.RPC.scriptPubKeyFromTxid;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import static org.junit.Assert.*;
import ec.ECDH_BC;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import opcodes.ScriptOpCodes;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Sha256Hash;
import javax.xml.bind.DatatypeConverter;
import org.bitcoinj.core.NetworkParameters;


public class P2SH extends TXN{
    Bolt3Util.NetworkType netType;
    String version="02000000";
    String inputCount="01";
    String prevTxid;
    String prevVout;
    String voutConst="000000";
    String scriptSigSize="00";
    String scriptSig="";
    String sequence="ffffffff";
    String outputCount="01";
    String satoshis;
    String scriptPubKeySize;
    String scriptPubKey;
    String locktime="00000000";
    
    public static String scriptToAddress(Bolt3Util.NetworkType netType, String script){
        String scriptHash = scriptToHash(script);
        if(netType == Bolt3Util.NetworkType.MAINNET)
            scriptHash = "05"+scriptHash;
        else
            scriptHash = "c4"+scriptHash;    
        String _tmp = ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(scriptHash))));
        return Bolt3Util.toBase58( ec.Utils.hexStringToByteArray(scriptHash+_tmp.substring(0, 8)));
        
    }
    
    public String hextoDoubleHash(String _){
        this.scriptSig = _;
        this.scriptSigSize = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptSig.length()/2));
        
        String doubleHash = ec.Utils.byteArrayToHexString(this.hex()) + "01000000";
        System.out.println(String.format("hex for hash: %s", doubleHash));
        
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(doubleHash))));
    }
    
    public String signedHex(ECKey e, String pubKey){
        
        //String pubKey = scriptPubKeyFromTxid(e.toAddress(NetworkParameters.regTests()).toBase58(), P2PKH.reverse(this.prevTxid));
        System.out.println(String.format("pubKey: %s",pubKey));
        String msg = hextoDoubleHash(pubKey);
        System.out.println(String.format("double hash: %s",msg));
        
        Sha256Hash hash = Sha256Hash.wrap(msg);
        ECKey.ECDSASignature s = e.sign(hash);
        byte[] res = s.encodeToDER();
        String test = DatatypeConverter.printHexBinary(res);
        String sig = test.toLowerCase();
        
        System.out.println(String.format("--->> %s",sig));
        sig = sig + "01";
        String publicKey = ec.Utils.byteArrayToHexString(e.getPubKey());
        this.scriptSig = String.format("%s%s%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(sig.length()/2)),sig,P2PKH.compactSizeUint(BigInteger.valueOf(publicKey.length()/2)),publicKey);
        this.scriptSigSize = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptSig.length()/2));
        
        return ec.Utils.byteArrayToHexString(this.hex());
    }
    
    public static String scriptToHash(String _){
        String opcode[]= _.split(" ");
        String scriptHex = P2PKH.scriptToHex(_);
           
        return ec.Utils.byteArrayToHexString(ec.Hash.ripemd160(ec.Hash.sha256(ec.Utils.hexStringToByteArray(scriptHex))));
    }
    
    public P2SH(){}
    
    public  P2SH(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String redeemScript, String amount){
        this.netType = netType;
        this.prevTxid = P2PKH.reverse(prevTxid);
        this.prevVout = P2PKH.reverse(prevVout);
        this.satoshis = P2PKH.reverse(P2PKH.btcToSatoshi(amount));
        
        this.scriptPubKey = String.format("a914%s87", scriptToHash(redeemScript));
        this.scriptPubKeySize = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length()/2));
        //<sig><pk>
        //this.scriptSig = String.format("%s%s%s%s", compactSizeUint(BigInteger.valueOf(txSig.length()/2)), txSig, compactSizeUint(BigInteger.valueOf(txPub.length()/2)), txPub );
        //this.scriptSigSize = compactSizeUint(BigInteger.valueOf(scriptSig.length()/2));
    }
    
    public  byte[] hex(){
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(byteStream);
        pw.write(version);
        pw.write(inputCount);
        pw.write(prevTxid);
        pw.write(prevVout);
        pw.write(voutConst);
        pw.write(scriptSigSize);
        pw.write(scriptSig);
        pw.write(sequence);
        pw.write(outputCount);
        pw.write(satoshis);
        pw.write(scriptPubKeySize);
        pw.write(scriptPubKey);
        pw.write(locktime);
        pw.flush();
        //System.out.println(byteStream.toString());
        return ec.Utils.hexStringToByteArray(byteStream.toString());
    }
    public static void main(String args[]){
        /*
        System.out.println(reverse("abbccd"));
       // assertEquals(reverse(btcToSatoshi("99.99")), "c0a1fc5302000000" );
        System.out.println(reverse(btcToSatoshi("99.99")));
      //  assertEquals(pkhFromAddress("moMuJ3RbscwteWyPdkK7R7HVikSTDJdYbS"), "560AC217D3202D393EE155B5F76F6F1BA14FB673");
        System.out.println(pkhFromAddress("cUea2TZarTwKfEBaubiGeDtXA3izXeXJ7ziYEBco1bQgTGR3gDE8"));
       // assertEquals(compactSizeUint(new BigInteger("200")) , "c8");
        System.out.println(compactSizeUint(new BigInteger("200")));
        P2SH p2sh = new P2SH(Bolt3Util.NetworkType.REGTEST, "3adbc01a3f37ac77e7584acb6c51490f239843a83121f78ed62a0dc481f84efd", "01" ,"OP_2 OP_EQUALVERIFY", "4.99");
       System.out.println( ec.Utils.byteArrayToHexString(p2sh.hex()));
       System.out.println(ScriptOpCodes.getOpCode("OP_PUSHDATA1"));
       byte[] temp = ec.Utils.hexStringToByteArray("5288");
       System.out.println(ec.Utils.byteArrayToHexString(ec.Hash.ripemd160(ec.Hash.sha256(temp))));
       System.out.println(scriptToHash("OP_2 88"));
       */
    }
}
