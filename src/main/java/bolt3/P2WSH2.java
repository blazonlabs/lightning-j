/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bolt3;

/*
unsigned


signed


*/

/**
 *
 * @author neerajnagi
 */
//single input singleoutput 
import java.math.BigInteger;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import static org.junit.Assert.*;
import ec.ECDH_BC;
import ec.Utils;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import javax.xml.bind.DatatypeConverter;
import opcodes.ScriptOpCodes;
import org.bitcoin.NativeSecp256k1;
import org.bitcoin.NativeSecp256k1Util;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Sha256Hash;


public class P2WSH2 extends TXN{
    Bolt3Util.NetworkType netType;
    String version="02000000";
    String marker="00";
    String flag="01";
    String inputCount="01";
    String prevTxid;
    String prevVout;
    String voutConst="0000";
    String scriptSigSize="00";
    String sequence="ffffffff";
    String outputCount="01";
    String amount;
    String satoshis1;
    String scriptPubKeySize1;
    String scriptPubKey1;
    //String satoshis2;
    //String scriptPubKeySize2;
    //String scriptPubKey2;
    String numberOfWitness;
    String witness;
    String locktime="00000000";
    
    public String hextoDoubleHash(String _){
        
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(_))));
    }
    
//    public String signedHex(ECKey e, String pubKey, String amt){
//
//        System.out.println(String.format("pubKey: %s",pubKey));
//        String scriptPub = pubKey.substring(2);
//        System.out.println(String.format("pKey: %s",scriptPub));
//        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
//        String outputs = String.format("%s%s%s",this.satoshis,this.scriptPubKeySize,this.scriptPubKey);
//        
//        String hashPrevouts = hextoDoubleHash(prevOuts);
//        String hashSequence = hextoDoubleHash(this.sequence);
//        String scriptCode = String.format("1976a9%s88ac",scriptPub);
//        String amount= P2PKH.reverse(P2PKH.btcToSatoshi(amt));;
//        String hashOutputs= hextoDoubleHash(outputs);
//        String nHashType="01000000";
//        
//        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
//        String msg = hextoDoubleHash(hashPreImage);
//        
//        Sha256Hash hash = Sha256Hash.wrap(msg);
//        ECKey.ECDSASignature s = e.sign(hash);
//        byte[] res = s.encodeToDER();
//        String test = DatatypeConverter.printHexBinary(res);
//        String sig = test.toLowerCase();
//        
//        System.out.println(String.format("--->> %s",sig));
//        sig = sig + "01";
//        String publicKey = ec.Utils.byteArrayToHexString(e.getPubKey());
//        this.numberOfWitness="02";
//        this.witness = String.format("%s%s%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(sig.length()/2)),sig,P2PKH.compactSizeUint(BigInteger.valueOf(publicKey.length()/2)),publicKey);
//
//        
//        return ec.Utils.byteArrayToHexString(this.hex2());
//    }
    
//    public String signedHex(ECKey[] e, String redeemScript, String amt){
//
//        String signatures = signature(e, redeemScript, amt);
//        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
//        int witnessLength = e.length + 2;
//        this.numberOfWitness=String.format("%02X",witnessLength);
//        this.witness = String.format("00%s%s%s",signatures,P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);
//
//        
//        return ec.Utils.byteArrayToHexString(this.hex2());
//    }
//    
//    public String signature(ECKey[] e, String redeemScript, String amt){
//
//        System.out.println(String.format("redeem script: %s",redeemScript));
//        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
//        System.out.println(String.format("redeem script hex: %s",redeemScriptHex));
//        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
//        String outputs = String.format("%s%s%s",this.satoshis,this.scriptPubKeySize,this.scriptPubKey);
//        
//        String hashPrevouts = hextoDoubleHash(prevOuts);
//        String hashSequence = hextoDoubleHash(this.sequence);
//        String scriptCode = String.format("%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);
//        String amount= P2PKH.reverse(P2PKH.btcToSatoshi(amt));;
//        String hashOutputs= hextoDoubleHash(outputs);
//        String nHashType="01000000";
//        
//        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
//        String msg = hextoDoubleHash(hashPreImage);
//        int arrLength = e.length;
//        String[] sig = new String[arrLength];
//        String signatures="";
//        for (int i = 0; i < e.length; i++) {
//            
//            Sha256Hash hash = Sha256Hash.wrap(msg);
//            ECKey.ECDSASignature s = e[i].sign(hash);
//            byte[] res = s.encodeToDER();
//            String test = DatatypeConverter.printHexBinary(res);
//            sig[i] = test.toLowerCase();
//        
//            System.out.println(String.format("--->> %s",sig[i]));
//            sig[i] = sig[i] + "01";
//            signatures = signatures + P2PKH.compactSizeUint(BigInteger.valueOf(sig[i].length()/2)) + sig[i];
//        }
//        return signatures;
//    }
    
    public String signingHash(String redeemScript, String amt){
        System.out.println(String.format("verifySignature redeem script: %s",redeemScript));
        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
        System.out.println(String.format("verifySignature redeem script hex: %s",redeemScriptHex));
        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
        //String outputs = String.format("%s%s%s%s%s%s",this.satoshis1,this.scriptPubKeySize1,this.scriptPubKey1,this.satoshis2,this.scriptPubKeySize2,this.scriptPubKey2);
        String outputs = String.format("%s%s%s",this.satoshis1,this.scriptPubKeySize1,this.scriptPubKey1);
        
        String hashPrevouts = hextoDoubleHash(prevOuts);
        String hashSequence = hextoDoubleHash(this.sequence);
        String scriptCode = String.format("%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);
        String amount= P2PKH.reverse(amt);
        String hashOutputs= hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
        System.out.println("verifySignature hashPreImage : "+hashPreImage);
        String msg = hextoDoubleHash(hashPreImage);
        System.out.println("verifySignature msg : "+msg);
        return msg;
    }
    
    public boolean verifySignature(String redeemScript, String amt, String signature, String pubKey) throws NativeSecp256k1Util.AssertFailException{

//        System.out.println(String.format("verifySignature redeem script: %s",redeemScript));
//        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
//        System.out.println(String.format("verifySignature redeem script hex: %s",redeemScriptHex));
//        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
//        //String outputs = String.format("%s%s%s%s%s%s",this.satoshis1,this.scriptPubKeySize1,this.scriptPubKey1,this.satoshis2,this.scriptPubKeySize2,this.scriptPubKey2);
//        String outputs = String.format("%s%s%s",this.satoshis1,this.scriptPubKeySize1,this.scriptPubKey1);
//        
//        String hashPrevouts = hextoDoubleHash(prevOuts);
//        String hashSequence = hextoDoubleHash(this.sequence);
//        String scriptCode = String.format("%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);
//        String amount= P2PKH.reverse(amt);
//        String hashOutputs= hextoDoubleHash(outputs);
//        String nHashType="01000000";
//        
//        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
//        System.out.println("verifySignature hashPreImage : "+hashPreImage);
//        String msg = hextoDoubleHash(hashPreImage);
//        System.out.println("verifySignature msg : "+msg);
        
        String msg = signingHash(redeemScript, amt);

        boolean result = ECDH_BC.verify(ec.Utils.hexStringToByteArray(msg), ec.Utils.hexStringToByteArray(signature), ec.Utils.hexStringToByteArray(pubKey));
        return result;
    }
    
    public static String scriptToHash(String _){
        String scriptHex = P2PKH.scriptToHex(_);
           
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Utils.hexStringToByteArray(scriptHex)));
    }
    
    public P2WSH2(){}
    
    //public  P2WSH2(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String pubKey , String amount1 ,String escrowScript, String amount2){
    public  P2WSH2(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String pubKey , String amount1, String seq, String locktime){
        this.netType = netType;
        this.prevTxid = prevTxid;//P2PKH.reverse(prevTxid);
        this.prevVout = P2PKH.reverse(prevVout);
        this.satoshis1 = P2PKH.reverse(amount1);
        this.sequence = P2PKH.reverse("80"+seq);
        this.locktime = P2PKH.reverse("20"+locktime);
        this.scriptPubKey1 = String.format("0014%s", ec.Utils.byteArrayToHexString(ec.Hash.ripemd160(ec.Hash.sha256(ec.Utils.hexStringToByteArray(pubKey)))));
        this.scriptPubKeySize1 = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptPubKey1.length()/2));
        System.out.println("P2WSH2 raw transaction : "+Utils.byteArrayToHexString(hex()));
//        this.satoshis2 = P2PKH.reverse(amount2);
//        
//        this.scriptPubKey2 = String.format("0020%s", scriptToHash(escrowScript));
//        this.scriptPubKeySize2 = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptPubKey2.length()/2));
        
    }
    
    public  P2WSH2(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String pubKey , String amount1, String seq, String locktime, String escrowScript){
        this.netType = netType;
        this.prevTxid = prevTxid;//P2PKH.reverse(prevTxid);
        this.prevVout = P2PKH.reverse(prevVout);
        this.satoshis1 = P2PKH.reverse(amount1);
        this.sequence = P2PKH.reverse("80"+seq);
        this.locktime = P2PKH.reverse("20"+locktime);
        this.scriptPubKey1 = String.format("0020%s", scriptToHash(escrowScript));
        this.scriptPubKeySize1 = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptPubKey1.length()/2));
        System.out.println("P2WSH2 raw transaction : "+Utils.byteArrayToHexString(hex()));
//        this.satoshis2 = P2PKH.reverse(amount2);
//        
//        this.scriptPubKey2 = String.format("0020%s", scriptToHash(escrowScript));
//        this.scriptPubKeySize2 = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptPubKey2.length()/2));
        
    }
    
//    public  P2WSH2(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String redeemScript, String amount, String flag){
//        this.netType = netType;
//        this.prevTxid = P2PKH.reverse(prevTxid);
//        this.prevVout = P2PKH.reverse(prevVout);
//        this.satoshis = P2PKH.reverse(amount);
//        
//        this.scriptPubKey = String.format("0020%s", scriptToHash(redeemScript));
//        this.scriptPubKeySize = P2PKH.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length()/2));
//    }
    
    public  byte[] hex(){
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(byteStream);
        pw.write(version);
        pw.write(inputCount);
        pw.write(prevTxid);
        pw.write(prevVout);
        pw.write(voutConst);
        pw.write(scriptSigSize);
        pw.write(sequence);
        pw.write(outputCount);
        pw.write(satoshis1);
        pw.write(scriptPubKeySize1);
        pw.write(scriptPubKey1);
//        pw.write(satoshis2);
//        pw.write(scriptPubKeySize2);
//        pw.write(scriptPubKey2);
        pw.write(locktime);
        pw.flush();
        //System.out.println(byteStream.toString());
        return ec.Utils.hexStringToByteArray(byteStream.toString());
    }
    
    public  byte[] hex2(){
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(byteStream);
        pw.write(version);
        pw.write(marker);
        pw.write(flag);
        pw.write(inputCount);
        pw.write(prevTxid);
        pw.write(prevVout);
        pw.write(voutConst);
        pw.write(scriptSigSize);
        pw.write(sequence);
        pw.write(outputCount);
        pw.write(satoshis1);
        pw.write(scriptPubKeySize1);
        pw.write(scriptPubKey1);
//        pw.write(satoshis2);
//        pw.write(scriptPubKeySize2);
//        pw.write(scriptPubKey2);
        pw.write(numberOfWitness);
        pw.write(witness);
        pw.write(locktime);
        pw.flush();
        //System.out.println(byteStream.toString());
        return ec.Utils.hexStringToByteArray(byteStream.toString());
    }
    
    public static void main(String args[]) throws Exception{
        
        //System.out.println(pkhFromBC32Address("2MvUdNwyzWRnSoULDAZcuf3qBFS2DEWvdmq"));
        //Bech32Util bu = Bech32Util.getInstance();
        //SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        //Pair<byte[], byte[]> temp = bu.bech32Decode("bcrt1q7kzlltmmdpwwtt6f72aer03czgmqylg8maj42p");
        //System.out.println(String.format("%s     %s",ec.Utils.byteArrayToHexString(temp.getLeft()),ec.Utils.byteArrayToHexString(temp.getRight())));
        //System.out.println(ec.Utils.byteArrayToHexString(Bolt3Util.fromBase58("f585ffaf7b685ce5af49f2bb91be381236027d07")));
        //System.out.println(ec.Utils.byteArrayToHexString( su.decode("bcrt","bcrt1q7kzlltmmdpwwtt6f72aer03czgmqylg8maj42p" ).getRight() ));
    }
}