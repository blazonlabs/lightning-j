/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bolt3;

/*
unsigned
02000000
01
990c437e3172c3ffaffa9f189ff35e6d9c77126c70ea0ed6a9246c9d9ee07f26
01000000
00
ffffffff
01
c0a1fc5302000000
19
76a91414db7fdbe296a068b6fac45d06c614e5014a30cb88ac
00000000

signed
02000000
01
990c437e3172c3ffaffa9f189ff35e6d9c77126c70ea0ed6a9246c9d9ee07f26
01000000
6a
47
30440220644d6ea83a6859722ef943bceec2a2a970b29c91e7f18ed0e2a4bbcaa40278ce02206b4fcce11ddac462b23b19f4d042215c92fc9283a84d8a742fe650da978646ac01
21
0332773327a776d8afd25af5c33841f666c54796a5adcd9d41d2bf34db51e10794
ffffffff
01
c0a1fc5302000000
19
76a91414db7fdbe296a068b6fac45d06c614e5014a30cb88ac
00000000



*/

/**
 *
 * @author neerajnagi
 */
//single input singleoutput 

import static bolt3.RPC.scriptPubKeyFromTxid;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import static org.junit.Assert.*;
import ec.ECDH_BC;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import javax.xml.bind.DatatypeConverter;
import opcodes.ScriptOpCodes;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;


public class P2PKH extends TXN{
    String version="02000000";
    String inputCount="01";
    String prevTxid;
    String prevVout;
    String voutConst="000000";
    String scriptSigSize="00";
    String scriptSig="";
    String sequence="ffffffff";
    String outputCount="01";
    String amount;
    String satoshis;
    String scriptPubKeySize;
    String scriptPubKey;
    String locktime="00000000";
    
    public static String reverse(String _){
        byte[] ret = ec.Utils.hexStringToByteArray(_);
        ArrayUtils.reverse(ret);
        return ec.Utils.byteArrayToHexString(ret);
    }
    
    public static String btcToSatoshi(String _){
        String base = "0000000000000000";
        Double btc = Double.parseDouble(_);
        btc = btc * 100000000;
        Long _satoshis = btc.longValue();
        String __ = Long.toHexString(_satoshis);
        return String.format("%s%s", base.substring(0,16-__.length()) ,__ );
    }
    
    public static String pkhFromAddress(String _){
        
        try {
            byte[] __ = Bolt3Util.fromBase58(_);
            return Bolt3Util.toHex(Arrays.copyOfRange(__, 1,__.length-4 ));
        } catch (Exception ex) {
            Logger.getLogger(P2PKH.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }
    /*
    public static String hextoDoubleHash(String prePubKey, P2PKH p){
        p.scriptSig = prePubKey;
        p.scriptSigSize = compactSizeUint(BigInteger.valueOf(p.scriptSig.length()/2));
        
        String doubleHash = ec.Utils.byteArrayToHexString(p.hex()) + "01000000";
        System.out.println(String.format("hex for hash: %s", doubleHash));
        
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(doubleHash))));
    }
    */
    public String hextoDoubleHash(String _){
        this.scriptSig = _;
        this.scriptSigSize = compactSizeUint(BigInteger.valueOf(this.scriptSig.length()/2));
        
        String doubleHash = ec.Utils.byteArrayToHexString(this.hex()) + "01000000";
        System.out.println(String.format("hex for hash: %s", doubleHash));
        
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(doubleHash))));
    }
    
    public String signedHex(ECKey e, String pubKey){

        //String pubKey = scriptPubKeyFromTxid(e.toAddress(reg).toBase58(), reverse(this.prevTxid));
        System.out.println(String.format("pubKey: %s",pubKey));
        String msg = hextoDoubleHash(pubKey);
        System.out.println(String.format("double hash: %s",msg));
        
        Sha256Hash hash = Sha256Hash.wrap(msg);
        ECKey.ECDSASignature s = e.sign(hash);
        byte[] res = s.encodeToDER();
        String test = DatatypeConverter.printHexBinary(res);
        String sig = test.toLowerCase();
        
        System.out.println(String.format("--->> %s",sig));
        sig = sig + "01";
        String publicKey = ec.Utils.byteArrayToHexString(e.getPubKey());
        this.scriptSig = String.format("%s%s%s%s",compactSizeUint(BigInteger.valueOf(sig.length()/2)),sig,compactSizeUint(BigInteger.valueOf(publicKey.length()/2)),publicKey);
        this.scriptSigSize = compactSizeUint(BigInteger.valueOf(this.scriptSig.length()/2));
        
        return ec.Utils.byteArrayToHexString(this.hex());
    }
    
    
    public String signedHex( ECKey[] e, String redScript){
        
        String redeemScript = scriptToHex(redScript);
        String msg = this.hextoDoubleHash(redeemScript);
        System.out.println(String.format("double hash: %s",msg));
        String[] sig = new String[2];
        for (int i = 0; i < 2; i++) {
            Sha256Hash hash = Sha256Hash.wrap(msg);
            ECKey.ECDSASignature s = e[i].sign(hash);
            byte[] res = s.encodeToDER();
            String test = DatatypeConverter.printHexBinary(res);
            sig[i] = test.toLowerCase();
            sig[i] = sig[i] + "01";
        }
        
        
        System.out.println(String.format("Sig1\n%s\nSig2\n%s",sig[0],sig[1]));
        this.scriptSig = String.format("00%s%s%s%s%s%s",compactSizeUint(BigInteger.valueOf(sig[0].length()/2)),sig[0],compactSizeUint(BigInteger.valueOf(sig[1].length()/2)),sig[1],compactSizeUint(BigInteger.valueOf(redeemScript.length()/2)),redeemScript);
        this.scriptSigSize = compactSizeUint(BigInteger.valueOf(this.scriptSig.length()/2));
        
        return ec.Utils.byteArrayToHexString(this.hex());
    }
    
    public static String scriptToHex( String _){
        String opcode[]= _.split(" ");
        String scriptHex = "";
        int x = 0xff;
        for(int i=0;i<opcode.length;i++){
            x = ScriptOpCodes.getOpCode(opcode[i]);
            if(x!=0xff)
                scriptHex = scriptHex + Integer.toHexString(x);
            else if(x==0xff)
                scriptHex = scriptHex + compactSizeUint(BigInteger.valueOf(opcode[i].length()/2)) + opcode[i];
        }
        System.out.println(scriptHex);
        return scriptHex;
    }
    
    public static String compactSizeUint(BigInteger x){
        
        String ret="";
        String base="00";
        if(x.compareTo(new BigInteger("0"))>=0 && x.compareTo(new BigInteger("252")) <= 0){
            String temp = String.format("%x",x.and(new BigInteger("ff",16)));
            ret = String.format("%s%s", base.substring(0,2-temp.length()) ,temp );
        }
        else if(x.compareTo(new BigInteger("253"))>=0 && x.compareTo(new BigInteger("65535")) <= 0)
            ret = String.format("%x",x.and(new BigInteger("ffff",16)));
        else if(x.compareTo(new BigInteger("65536"))>=0 && x.compareTo(new BigInteger("4294967295")) <= 0)
            ret = String.format("%x",x.and(new BigInteger("ffffffff",16)));        
        else if(x.compareTo(new BigInteger("4294967296"))>=0 && x.compareTo(new BigInteger("18446744073709551615")) <= 0)
            ret = String.format("%x",x.and(new BigInteger("ffffffffffffffff",16)));
        return ret;
    }
    public P2PKH(){}
    
    public  P2PKH(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String rxAddress, String amount){
        this.prevTxid = reverse(prevTxid);
        this.prevVout = reverse(prevVout);
        this.amount = amount;
        /*_amount = _amount * 100000000;
        this.amount = _amount.longValue();*/
        this.satoshis = reverse(btcToSatoshi(amount));
        this.scriptPubKey = String.format("76a914%s88ac", pkhFromAddress(rxAddress));
        this.scriptPubKeySize = compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length()/2));
        //<sig><pk>
        //this.scriptSig = String.format("%s%s%s%s", compactSizeUint(BigInteger.valueOf(txSig.length()/2)), txSig, compactSizeUint(BigInteger.valueOf(txPub.length()/2)), txPub );
        //this.scriptSigSize = compactSizeUint(BigInteger.valueOf(scriptSig.length()/2));
    }
     public P2PKH(Bolt3Util.NetworkType netType, String prevTxid, String prevVout, String rxAddress, String amount, String scriptSig){
        this.prevTxid = reverse(prevTxid);
        this.prevVout = reverse(prevVout);
        this.satoshis = reverse(btcToSatoshi(amount));
        this.scriptPubKey = String.format("76a914%s88ac", pkhFromAddress(rxAddress));
        this.scriptPubKeySize = compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length()/2));
        //<sig><pk>
        this.scriptSig = scriptToHex( scriptSig);
        this.scriptSigSize = compactSizeUint(BigInteger.valueOf(scriptSig.length()/2));
    }   
    public  byte[] hex(){
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(byteStream);
        pw.write(version);
        pw.write(inputCount);
        pw.write(prevTxid);
        pw.write(prevVout);
        pw.write(voutConst);
        pw.write(scriptSigSize);
        pw.write(scriptSig);
        pw.write(sequence);
        pw.write(outputCount);
        pw.write(satoshis);
        pw.write(scriptPubKeySize);
        pw.write(scriptPubKey);
        pw.write(locktime);
        pw.flush();
        //System.out.println(byteStream.toString());
        return ec.Utils.hexStringToByteArray(byteStream.toString());
    }
    public static void main(String args[]){
        System.out.println(reverse("abbccd"));
       // assertEquals(reverse(btcToSatoshi("99.99")), "c0a1fc5302000000" );
        System.out.println(reverse(btcToSatoshi("99.99")));
      //  assertEquals(pkhFromAddress("moMuJ3RbscwteWyPdkK7R7HVikSTDJdYbS"), "560AC217D3202D393EE155B5F76F6F1BA14FB673");
        System.out.println(pkhFromAddress("2MvUdNwyzWRnSoULDAZcuf3qBFS2DEWvdmq"));
       // assertEquals(compactSizeUint(new BigInteger("200")) , "c8");
        System.out.println(compactSizeUint(new BigInteger("200")));
       // P2PKH p2pkh = new P2PKH("96e5bea192d4589a6573fb883dd13e53b0b0671160453315bda006414bf72b5b", "01" ,"mqzWC6cjM2j3EqomyHALdTBG8U4vC3vHrm", "4.99","0309bf661d954948d2975997a560411bdfc7461b21ba04918f870e6ef598df6f4e" ,"304402200081f5df8d340c5f5da1d46c2f6c1b794ce24869f5b84d2bd0fcdbd90b43c70c0220446a76c2ae296141d899bf9cf4859d1f0d773224fffefdc70c1fafa3ed772ed001");
      // System.out.println( ec.Utils.byteArrayToHexString(p2pkh.hex()));
    }
}