/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bolt3;

import bitcoin.BitcoinClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ec.ECDH_BC;
import ec.Utils;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import org.bitcoinj.core.Address;
import wf.bitcoin.javabitcoindrpcclient.BitcoinJSONRPCClientNew;
import wf.bitcoin.javabitcoindrpcclient.BitcoinJSONRPCClient.*;
import wf.bitcoin.javabitcoindrpcclient.BitcoindRpcClient;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.DatatypeConverter;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey.ECDSASignature;
import org.bitcoinj.core.Sha256Hash;
import static org.bitcoinj.core.Sha256Hash.newDigest;
import wf.bitcoin.javabitcoindrpcclient.BitcoindRpcClient.ExtendedTxInput;
import wf.bitcoin.javabitcoindrpcclient.BitcoindRpcClient.Unspent;
import wf.bitcoin.javabitcoindrpcclient.BitcoindRpcClient.Transaction;
/**
 *
 * @author neerajnagi
 * 
 */

public class RPC {
    
    static BitcoinJSONRPCClientNew bitcoin;
    
    public static void printUTXO(String addr){
        List<Unspent> lU =  bitcoin.listUnspent(1, 9999999, addr);
        lU.forEach((Unspent u) -> {System.out.println(String.format("%s %s %s %s", u.address(), u.amount(),u.txid(), u.vout()));});
    }
    
    public static String voutFromAddressTxid(String addr , String txid)
    {
        final String[]  vout = new String[1];
        vout[0]="";
        List<Unspent> lU =  bitcoin.listUnspent(1, 9999999, addr);
        lU.forEach((Unspent u) -> {
            if(u.txid().equalsIgnoreCase(txid))
                vout[0] = String.format("%02X",u.vout());
        });
        
        return vout[0];

    }
    
    public static String scriptPubKeyFromTxid(String addr , String txid)
    {
        System.out.println(String.format("conaddr: %s",addr));
        final String[]  pubKey = new String[1];
        pubKey[0]="";
        List<Unspent> lU =  bitcoin.listUnspent(1, 9999999, addr);
        lU.forEach((Unspent u) -> {
            if(u.txid().equalsIgnoreCase(txid))
                pubKey[0] = String.format("%s",u.scriptPubKey());
        });
        
        return pubKey[0];

    }
    
    
    public static String amountFromTxid(String addr , String txid)
    {
        System.out.println(String.format("conaddr: %s",addr));
        final String[]  amt = new String[1];
        amt[0]="";
        List<Unspent> lU =  bitcoin.listUnspent(1, 9999999, addr);
        lU.forEach((Unspent u) -> {
            if(u.txid().equalsIgnoreCase(txid))
                amt[0] = String.format("%s",u.amount());
        });
        
        return amt[0];

    }
    
    public static void testP2PKH(String args[]){
        try{
            bitcoin = new BitcoinJSONRPCClientNew("http://blazon:blazon123@192.168.1.101:7332/");
        }catch(Exception e){
            System.out.println(e);
        }
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairA = new ECKey();
        ECKey pairB = new ECKey();
        String addrA = pairA.toAddress(reg).toBase58();
        String addrB = pairB.toAddress(reg).toBase58();
        System.out.println("\n\nTestcase 1 results:\n");
        System.out.println(String.format("A add \n%s\nB addr \n%s",addrA, addrB));
        String txid0 = bitcoin.sendToAddress(addrA, 100 );
        bitcoin.generate(5);
        bitcoin.importAddress(addrA, null, true);
        bitcoin.importAddress(addrB, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to A \n%s",txid0));
        printUTXO(addrA);
        printUTXO(addrB);
        System.out.println(String.format("vout for %s %s",addrA,txid0));
        System.out.println(voutFromAddressTxid(addrA, txid0));
        P2PKH txn1 = new P2PKH(Bolt3Util.NetworkType.REGTEST , txid0, voutFromAddressTxid(addrA, txid0) , addrB , "99.99" );
        String[] privateKeyArray = {pairA.getPrivateKeyAsWiF(reg)  };
        System.out.println(String.format("transactionHex is \n%s\nprivateKey of A is \n%s",ec.Utils.byteArrayToHexString(txn1.hex()) ,pairA.getPrivateKeyAsWiF(reg)) );
        String txn1Signed = bitcoin.signRawTransaction(ec.Utils.byteArrayToHexString(txn1.hex()), null, Arrays.asList(privateKeyArray));
        System.out.println(String.format("signedRawTransaction is\n%s",txn1Signed ));
        //String txid1 = bitcoin.sendRawTransaction(txn1Signed);
        bitcoin.generate(5);
        printUTXO(addrA);
        printUTXO(addrB);
    }
    
    public static void testP2PKHCustom(String args[]){
        try{
            bitcoin = new BitcoinJSONRPCClientNew("http://blazon:blazon123@192.168.1.101:7332/");
        }catch(Exception e){
            System.out.println(e);
        }
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairA = new ECKey();
        ECKey pairB = new ECKey();
        String addrA = pairA.toAddress(reg).toBase58();
        String addrB = pairB.toAddress(reg).toBase58();
        System.out.println("\n\nTestcase 2 results:\n");
        System.out.println(String.format("A add \n%s\nB addr \n%s",addrA, addrB));
        String txid0 = bitcoin.sendToAddress(addrA, 100 );
        bitcoin.generate(5);
        bitcoin.importAddress(addrA, null, true);
        bitcoin.importAddress(addrB, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to A \n%s",txid0));
        printUTXO(addrA);
        printUTXO(addrB);
        System.out.println(String.format("vout for %s %s",addrA,txid0));
        System.out.println(voutFromAddressTxid(addrA, txid0));
        P2PKH txn1 = new P2PKH(Bolt3Util.NetworkType.REGTEST , txid0, voutFromAddressTxid(addrA, txid0) , addrB , "99.99" );
        System.out.println(String.format("transactionHex is \n%s",ec.Utils.byteArrayToHexString(txn1.hex())));
        
        
        String sigHex = txn1.signedHex(pairA,scriptPubKeyFromTxid(addrA , txid0));
        //p.sginedHex()
        
        
        System.out.println(String.format("signed hex \n%s",sigHex));
        String txid1 = bitcoin.sendRawTransaction(sigHex);
        System.out.println(String.format("TXId from A to B\n%s",txid1));
        bitcoin.generate(5);
        printUTXO(addrA);
        printUTXO(addrB);
    }
    
    public static void testP2SH(String args[]){
        try{
            bitcoin = new BitcoinJSONRPCClientNew("http://blazon:blazon123@192.168.1.101:7332/");
        }catch(Exception e){
            System.out.println(e);
        }
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairX = new ECKey();
        ECKey pairY = new ECKey();
        String addrX = pairX.toAddress(reg).toBase58();
        String addrY = pairY.toAddress(reg).toBase58();
        System.out.println("\n\nTestcase 3 results:\n");
        System.out.println(String.format("X addr \n%s\nY addr \n%s",addrX, addrY));
        String fundingTX = bitcoin.sendToAddress(addrX, 100 );
        bitcoin.generate(50);
        bitcoin.importAddress(addrX, null, true);
        bitcoin.importAddress(addrY, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to X \n%s",fundingTX));
        printUTXO(addrX);
        printUTXO(addrY);
        System.out.println(String.format("vout for %s %s",addrX,fundingTX));
        System.out.println(voutFromAddressTxid(addrX, fundingTX));
        String redeemScript = "OP_2 OP_EQUALVERIFY";
        P2SH txn = new P2SH(Bolt3Util.NetworkType.REGTEST, fundingTX, voutFromAddressTxid(addrX, fundingTX) , redeemScript , "99.99" );
        String[] privateKeyX = {pairX.getPrivateKeyAsWiF(reg)  };
        System.out.println(String.format("transactionHex is \n%s\nprivateKey of X is \n%s",ec.Utils.byteArrayToHexString(txn.hex()) ,pairX.getPrivateKeyAsWiF(reg)) );
        String txnSigned = bitcoin.signRawTransaction(ec.Utils.byteArrayToHexString(txn.hex()), null, Arrays.asList(privateKeyX));
        System.out.println(String.format("signedRawTransaction is\n%s",txnSigned ));
        String txidXToRedeem = bitcoin.sendRawTransaction(txnSigned);
        String redeemAddr = P2SH.scriptToAddress( txn.netType,redeemScript );
        bitcoin.importAddress(redeemAddr, null, true);
        System.out.println("redeem address is \n "+redeemAddr);
        bitcoin.generate(50);
       
        System.out.println(String.format("TXId from X to Redeem script is \n%s",txidXToRedeem));
        printUTXO(addrX);
        printUTXO(addrY);
        printUTXO(redeemAddr);
        System.out.println(String.format("vout for %s %s",redeemAddr,txidXToRedeem));
        System.out.println(voutFromAddressTxid(redeemAddr, txidXToRedeem));
        P2PKH txn2 = new P2PKH(Bolt3Util.NetworkType.REGTEST, txidXToRedeem,voutFromAddressTxid(redeemAddr, txidXToRedeem) , addrY, "99.98", "OP_FALSE OP_2 5288");
        System.out.println(String.format("signedRawTransaction is\n%s",ec.Utils.byteArrayToHexString(txn2.hex()) ));
        String txidXToY = bitcoin.sendRawTransaction(ec.Utils.byteArrayToHexString(txn2.hex()));
        bitcoin.generate(50);
        System.out.println(String.format("TXId from X to Y is \n%s",txidXToY));
        printUTXO(addrX);
        printUTXO(addrY);
        printUTXO(redeemAddr);
    }

    public static void testP2SHMultiSig(String args[]){
        try{
            bitcoin = new BitcoinJSONRPCClientNew("http://blazon:blazon123@192.168.1.101:7332/");
        }catch(Exception e){
            System.out.println(e);
        }
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairU = new ECKey();
        ECKey pairV = new ECKey();
        String addrU = pairU.toAddress(reg).toBase58();
        String addrV = pairV.toAddress(reg).toBase58();
        System.out.println("\n\nTestcase 4 results:\n");
        System.out.println(String.format("U addr \n%s\nV addr \n%s",addrU, addrV));
        String fundingTX = bitcoin.sendToAddress(addrU, 100 );
        bitcoin.generate(50);
        bitcoin.importAddress(addrU, null, true);
        bitcoin.importAddress(addrV, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to U \n%s",fundingTX));
        printUTXO(addrU);
        printUTXO(addrV);
        System.out.println(String.format("vout for %s %s",addrU,fundingTX));
        System.out.println(voutFromAddressTxid(addrU, fundingTX));
        String redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",ec.Utils.byteArrayToHexString(pairU.getPubKey()),ec.Utils.byteArrayToHexString(pairV.getPubKey()));
        P2SH txnUtoMs = new P2SH(Bolt3Util.NetworkType.REGTEST, fundingTX, voutFromAddressTxid(addrU, fundingTX) , redeemScript , "99.99" );
        String[] privateKeyX = {pairU.getPrivateKeyAsWiF(reg)};
        System.out.println(String.format("transactionHex is \n%s\nprivateKey of U is \n%s",ec.Utils.byteArrayToHexString(txnUtoMs.hex()) ,pairU.getPrivateKeyAsWiF(reg)) );
        String txnUtoMsSigned = bitcoin.signRawTransaction(ec.Utils.byteArrayToHexString(txnUtoMs.hex()), null, Arrays.asList(privateKeyX));
        System.out.println(String.format("signedRawTransaction is\n%s",txnUtoMsSigned ));
        String txidUtoMs = bitcoin.sendRawTransaction(txnUtoMsSigned);
        String multiSigAddr = P2SH.scriptToAddress( txnUtoMs.netType,redeemScript );
        //String multiSigAddr = P2SH.scriptToHash(redeemScript );
        bitcoin.importAddress(multiSigAddr, null, true);
        System.out.println("redeem address is \n"+multiSigAddr);
        bitcoin.generate(50);
        System.out.println(String.format("TXId from U to Multi sig is \n%s",txidUtoMs));
        printUTXO(addrU);
        printUTXO(addrV);
        printUTXO(multiSigAddr);
        System.out.println(String.format("vout for %s %s",multiSigAddr,txidUtoMs));
        System.out.println(voutFromAddressTxid(multiSigAddr, txidUtoMs));
        P2PKH txnMstoV = new P2PKH(Bolt3Util.NetworkType.REGTEST, txidUtoMs,voutFromAddressTxid(multiSigAddr, txidUtoMs) , addrV, "99.98");
        String[] privateKeyUV = {pairU.getPrivateKeyAsWiF(reg), pairV.getPrivateKeyAsWiF(reg)};
        System.out.println(String.format("transactionHex is \n%s\nprivateKey of U is \n%s\nprivateKey of V is \n%s",ec.Utils.byteArrayToHexString(txnMstoV.hex()) ,pairU.getPrivateKeyAsWiF(reg), pairV.getPrivateKeyAsWiF(reg)) );
               //ExtendedTxInput[] extdInput ={ new ExtendedTxInput(txnMstoV.prevTxid, Integer.parseInt(txnMstoV.prevVout), txnUtoMs.scriptPubKey , P2PKH.scriptToHex(redeemScript), new BigDecimal("99.98"))};
               ExtendedTxInput[] extdInput ={ new ExtendedTxInput(txidUtoMs, Integer.parseInt(voutFromAddressTxid(multiSigAddr, txidUtoMs)), txnUtoMs.scriptPubKey , P2PKH.scriptToHex(redeemScript), new BigDecimal("99.99"))};
        List<ExtendedTxInput> inputs = Arrays.asList(extdInput);
        
        String txnMstoVSigned = bitcoin.signRawTransaction(ec.Utils.byteArrayToHexString(txnMstoV.hex()), inputs, Arrays.asList(privateKeyUV));
        System.out.println(String.format("signedRawTransaction is\n%s",txnMstoVSigned ));        
        String txidMsToV = bitcoin.sendRawTransaction(txnMstoVSigned);
        bitcoin.generate(50);
        System.out.println(String.format("TXId from Multi sig to V is \n%s",txidMsToV));
        printUTXO(addrU);
        printUTXO(addrV);
        printUTXO(multiSigAddr);
        
            
    } 
    
    public static void testP2SHMultiSigCustom(String args[]){
        try{
            bitcoin = new BitcoinJSONRPCClientNew("http://blazon:blazon123@192.168.1.101:7332/");
        }catch(Exception e){
            System.out.println(e);
        }
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairU = new ECKey();
        ECKey pairV = new ECKey();
        String addrU = pairU.toAddress(reg).toBase58();
        String addrV = pairV.toAddress(reg).toBase58();
        System.out.println("\n\nTestcase 5 results:\n");
        System.out.println(String.format("U addr \n%s\nV addr \n%s",addrU, addrV));
        String fundingTX = bitcoin.sendToAddress(addrU, 50 );
        bitcoin.generate(50);
        bitcoin.importAddress(addrU, null, true);
        bitcoin.importAddress(addrV, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to U \n%s",fundingTX));
        printUTXO(addrU);
        printUTXO(addrV);
        System.out.println(String.format("vout for %s %s",addrU,fundingTX));
        System.out.println(voutFromAddressTxid(addrU, fundingTX));
        String redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",ec.Utils.byteArrayToHexString(pairU.getPubKey()),ec.Utils.byteArrayToHexString(pairV.getPubKey()));
        P2SH txnUtoMs = new P2SH(Bolt3Util.NetworkType.REGTEST, fundingTX, voutFromAddressTxid(addrU, fundingTX) , redeemScript , "49.99" );
        System.out.println(String.format("transactionHex is \n%s",ec.Utils.byteArrayToHexString(txnUtoMs.hex())));

        
        String signedHex1 = txnUtoMs.signedHex(pairU, scriptPubKeyFromTxid(addrU, fundingTX));
        System.out.println(String.format("Signed hex1: %s",signedHex1));

        String txidUtoMs = bitcoin.sendRawTransaction(signedHex1);
        String multiSigAddr = P2SH.scriptToAddress( txnUtoMs.netType,redeemScript );
        bitcoin.importAddress(multiSigAddr, null, true);
        System.out.println("redeem address is \n"+multiSigAddr);
        bitcoin.generate(5);
        System.out.println(String.format("TXId from U to Multi sig is \n%s",txidUtoMs));
        printUTXO(addrU);
        printUTXO(addrV);
        printUTXO(multiSigAddr);
        System.out.println(String.format("vout for %s %s",multiSigAddr,txidUtoMs));
        System.out.println(voutFromAddressTxid(multiSigAddr, txidUtoMs));
        P2PKH txnMstoV = new P2PKH(Bolt3Util.NetworkType.REGTEST, txidUtoMs,voutFromAddressTxid(multiSigAddr, txidUtoMs) , addrV, "49.98");
        
        ECKey[] privateKeys = new ECKey[2];
        privateKeys[0] = pairU;
        privateKeys[1] = pairV;
        String signedHex2 = txnMstoV.signedHex(privateKeys, redeemScript);
        System.out.println(String.format("Signed hex2: %s",signedHex2));
        
        String txidMsToV = bitcoin.sendRawTransaction(signedHex2);
        bitcoin.generate(50);
        System.out.println(String.format("TXId from Multi sig to V is \n%s",txidMsToV));
        printUTXO(addrU);
        printUTXO(addrV);
        printUTXO(multiSigAddr);
            
    }
    
    public static void testP2WPKH(String args[]) throws Exception{
        try{
            bitcoin = new BitcoinJSONRPCClientNew("http://blazon:blazon123@192.168.1.101:7332/");
        }catch(Exception e){
            System.out.println(e);
        }
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairA = new ECKey();
        ECKey pairB = new ECKey();
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        byte witnessVersion=0;
        String addrA = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairA.getPubKeyHash());
        String addrB = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairB.getPubKeyHash());
        System.out.println("\n\nTestcase 6 results:\n");
        System.out.println(String.format("A add \n%s\nB addr \n%s",addrA, addrB));
        String txid0 = bitcoin.sendToAddress(addrA, 100 );
        bitcoin.generate(5);
        bitcoin.importAddress(addrA, null, true);
        bitcoin.importAddress(addrB, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to A \n%s",txid0));
        printUTXO(addrA);
        printUTXO(addrB);
        System.out.println(String.format("vout for %s %s",addrA,txid0));
        System.out.println(voutFromAddressTxid(addrA, txid0));
        P2WPKH txn1 = new P2WPKH(Bolt3Util.NetworkType.REGTEST , txid0, voutFromAddressTxid(addrA, txid0) , addrB , "99.99" );
        String[] privateKeyArray = {pairA.getPrivateKeyAsWiF(reg)  };
        System.out.println(String.format("transactionHex is \n%s\nprivateKey of A is \n%s",ec.Utils.byteArrayToHexString(txn1.hex()) ,pairA.getPrivateKeyAsWiF(reg)) );
        String txn1Signed = bitcoin.signRawTransaction(ec.Utils.byteArrayToHexString(txn1.hex()), null, Arrays.asList(privateKeyArray));
        System.out.println(String.format("signedRawTransaction is\n%s",txn1Signed ));
        String txid1 = bitcoin.sendRawTransaction(txn1Signed);
        System.out.println(String.format("Funding TXId from A to B \n%s",txid1));
        bitcoin.generate(5);
        printUTXO(addrA);
        printUTXO(addrB);
    }
    
    public static void testP2WPKHCustom(String args[]) throws Exception{
        try{
            bitcoin = new BitcoinJSONRPCClientNew("http://blazon:blazon123@192.168.1.101:7332/");
        }catch(Exception e){
            System.out.println(e);
        }
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairA = new ECKey();
        ECKey pairB = new ECKey();
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        byte witnessVersion=0;
        String addrA = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairA.getPubKeyHash());
        String addrB = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairB.getPubKeyHash());
        System.out.println("\n\nTestcase 7 results:\n");
        System.out.println(String.format("A add \n%s\nB addr \n%s",addrA, addrB));
        String txid0 = bitcoin.sendToAddress(addrA, 100 );
        bitcoin.generate(5);
        bitcoin.importAddress(addrA, null, true);
        bitcoin.importAddress(addrB, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to A \n%s",txid0));
        printUTXO(addrA);
        printUTXO(addrB);
        System.out.println(String.format("vout for %s %s",addrA,txid0));
        System.out.println(voutFromAddressTxid(addrA, txid0));
        P2WPKH txn1 = new P2WPKH(Bolt3Util.NetworkType.REGTEST , txid0, voutFromAddressTxid(addrA, txid0) , addrB , "99.99" );
        System.out.println(String.format("transactionHex is \n%s",ec.Utils.byteArrayToHexString(txn1.hex())));
        
        String signedHex1 = txn1.signedHex(pairA, scriptPubKeyFromTxid(addrA, txid0), amountFromTxid(addrA, txid0));
        System.out.println(String.format("Signed hex1: %s",signedHex1));
        
        String txid1 = bitcoin.sendRawTransaction(signedHex1);
        System.out.println(String.format("Funding TXId from A to B \n%s",txid1));
        bitcoin.generate(5);
        printUTXO(addrA);
        printUTXO(addrB);
    }
    
    public static void testP2WSH(String args[]) throws Exception{
        try{
            bitcoin = new BitcoinJSONRPCClientNew("http://blazon:blazon123@192.168.1.101:7332/");
        }catch(Exception e){
            System.out.println(e);
        }
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairA = new ECKey();
        ECKey pairB = new ECKey();
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        byte witnessVersion=0;
        String addrA = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairA.getPubKeyHash());
        String addrB = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairB.getPubKeyHash());
        System.out.println("\n\nTestcase 8 results:\n");
        System.out.println(String.format("A add \n%s\nB addr \n%s",addrA, addrB));
        String txid0 = bitcoin.sendToAddress(addrA, 100 );
        bitcoin.generate(5);
        bitcoin.importAddress(addrA, null, true);
        bitcoin.importAddress(addrB, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to A \n%s",txid0));
        printUTXO(addrA);
        printUTXO(addrB);
        System.out.println(String.format("vout for %s %s",addrA,txid0));
        System.out.println(voutFromAddressTxid(addrA, txid0));
        
        String redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",pairA.getPublicKeyAsHex(),pairB.getPublicKeyAsHex());
        P2WSH txnAtoMs = new P2WSH(Bolt3Util.NetworkType.REGTEST, txid0, voutFromAddressTxid(addrA, txid0) , redeemScript , "99.99" );
        
        String[] privateKeyA = {pairA.getPrivateKeyAsWiF(reg)};
        System.out.println(String.format("transactionHex is \n%s\nprivateKey of A is \n%s",ec.Utils.byteArrayToHexString(txnAtoMs.hex()) ,pairA.getPrivateKeyAsWiF(reg)) );
        String txnAtoMsSigned = bitcoin.signRawTransaction(ec.Utils.byteArrayToHexString(txnAtoMs.hex()), null, Arrays.asList(privateKeyA));
        System.out.println(String.format("signedRawTransaction is\n%s",txnAtoMsSigned ));
        String txidAtoMs = bitcoin.sendRawTransaction(txnAtoMsSigned);
        String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(P2WSH.scriptToHash(redeemScript)));
        //String multiSigAddr = P2SH.scriptToHash(redeemScript );
        bitcoin.importAddress(multiSigAddr, null, true);
        System.out.println("redeem address is \n"+multiSigAddr);
        bitcoin.generate(50);
        System.out.println(String.format("TXId from A to Multi sig is \n%s",txidAtoMs));
        printUTXO(addrA);
        printUTXO(addrB);
        printUTXO(multiSigAddr);
        System.out.println(String.format("vout for %s %s",multiSigAddr,txidAtoMs));
        System.out.println(voutFromAddressTxid(multiSigAddr, txidAtoMs));
        
        
        P2WPKH txnMstoB = new P2WPKH(Bolt3Util.NetworkType.REGTEST, txidAtoMs,voutFromAddressTxid(multiSigAddr, txidAtoMs) , addrB, "99.98");
        String[] privateKeyAB = {pairA.getPrivateKeyAsWiF(reg), pairB.getPrivateKeyAsWiF(reg)};
        System.out.println(String.format("transactionHex is \n%s\nprivateKey of A is \n%s\nprivateKey of B is \n%s",ec.Utils.byteArrayToHexString(txnMstoB.hex()) ,pairA.getPrivateKeyAsWiF(reg), pairB.getPrivateKeyAsWiF(reg)) );
               //ExtendedTxInput[] extdInput ={ new ExtendedTxInput(txnMstoV.prevTxid, Integer.parseInt(txnMstoV.prevVout), txnUtoMs.scriptPubKey , P2PKH.scriptToHex(redeemScript), new BigDecimal("99.98"))};
               ExtendedTxInput[] extdInput ={ new ExtendedTxInput(txidAtoMs, Integer.parseInt(voutFromAddressTxid(multiSigAddr, txidAtoMs)), txnAtoMs.scriptPubKey , P2PKH.scriptToHex(redeemScript), new BigDecimal("99.99"))};
        List<ExtendedTxInput> inputs = Arrays.asList(extdInput);
        
        String txnMstoBSigned = bitcoin.signRawTransaction(ec.Utils.byteArrayToHexString(txnMstoB.hex()), inputs, Arrays.asList(privateKeyAB));
        System.out.println(String.format("signedRawTransaction is\n%s",txnMstoBSigned ));        
        String txidMsToB = bitcoin.sendRawTransaction(txnMstoBSigned);
        bitcoin.generate(50);
        System.out.println(String.format("TXId from Multi sig to B is \n%s",txidMsToB));
        printUTXO(addrA);
        printUTXO(addrB);
        printUTXO(multiSigAddr);
        
    }
    
    public static void testP2WSHCustom(String args[]) throws Exception{
        try{
            bitcoin = new BitcoinJSONRPCClientNew("http://blazon:blazon123@192.168.1.101:7332/");
        }catch(Exception e){
            System.out.println(e);
        }
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairA = new ECKey();
        ECKey pairB = new ECKey();
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        byte witnessVersion=0;
        String addrA = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairA.getPubKeyHash());
        String addrB = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairB.getPubKeyHash());
        System.out.println("\n\nTestcase 9 results:\n");
        System.out.println(String.format("A add \n%s\nB addr \n%s",addrA, addrB));
        String txid0 = bitcoin.sendToAddress(addrA, 100 );
        bitcoin.generate(5);
        bitcoin.importAddress(addrA, null, true);
        bitcoin.importAddress(addrB, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to A \n%s",txid0));
        printUTXO(addrA);
        printUTXO(addrB);
        System.out.println(String.format("vout for %s %s",addrA,txid0));
        System.out.println(voutFromAddressTxid(addrA, txid0));     
        String redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",pairA.getPublicKeyAsHex(),pairB.getPublicKeyAsHex());
        P2WSH txnAtoMs = new P2WSH(Bolt3Util.NetworkType.REGTEST, txid0, voutFromAddressTxid(addrA, txid0) , redeemScript , "99.99" );
        String signedHex1 = txnAtoMs.signedHex(pairA, scriptPubKeyFromTxid(addrA, txid0), amountFromTxid(addrA, txid0));
        System.out.println(String.format("signed hex 1\n%s",signedHex1));
        String txidAtoMs = bitcoin.sendRawTransaction(signedHex1);
        String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(P2WSH.scriptToHash(redeemScript)));
        bitcoin.importAddress(multiSigAddr, null, true);
        System.out.println("redeem address is \n"+multiSigAddr);
        bitcoin.generate(5);
        System.out.println(String.format("TXId from A to Multi sig is \n%s",txidAtoMs));
        printUTXO(addrA);
        printUTXO(addrB);
        printUTXO(multiSigAddr);
        System.out.println(String.format("vout for %s %s",multiSigAddr,txidAtoMs));
        System.out.println(voutFromAddressTxid(multiSigAddr, txidAtoMs));
           
        P2WPKH txnMstoB = new P2WPKH(Bolt3Util.NetworkType.REGTEST, txidAtoMs,voutFromAddressTxid(multiSigAddr, txidAtoMs) , addrB, "99.98");

        ECKey[] privateKeys = new ECKey[2];
        privateKeys[0] = pairA;
        privateKeys[1] = pairB;
        String signedHex2 = txnMstoB.signedHex(privateKeys, redeemScript, amountFromTxid(multiSigAddr, txidAtoMs));
        System.out.println(String.format("signed hex 2\n%s",signedHex2));
        String txidMsToB = bitcoin.sendRawTransaction(signedHex2);
        bitcoin.generate(5);
        System.out.println(String.format("TXId from Multi sig to B is \n%s",txidMsToB));
        printUTXO(addrA);
        printUTXO(addrB);
        printUTXO(multiSigAddr);
        
    }
    
    public static void testP2WSHBolt3(byte[] fundingTXId, byte[] fundingVout, byte[] amount, byte[] selfDelay, byte[] revocationPubKey, byte[] localDelayedPubKey , byte[] pubKey1, byte[] pubKey2, ECKey pair) throws Exception{
        
    }
    
    public static void testP2WSHBolt3(String fundingTXId, String fundingVout, String amount, String selfDelay, String revocationPubKey, String localDelayedPubKey , String pubKey1, String pubKey2, ECKey pair) throws Exception{
        
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        byte witnessVersion=0;
        
        String redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",pubKey1,pubKey2);
        String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(P2WSH.scriptToHash(redeemScript)));
        bitcoin.importAddress(multiSigAddr, null, true);
        System.out.println("redeem address is \n"+multiSigAddr);
        
        
        String escrowScript = String.format("OP_IF %s OP_ELSE '%s' OP_CHECKSEQUENCEVERIFY OP_DROP %s OP_ENDIF OP_CHECKSIG",revocationPubKey, selfDelay, localDelayedPubKey );
        String escrowAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(P2WSH.scriptToHash(escrowScript)));
        bitcoin.importAddress(escrowAddr, null, true);
        System.out.println("escrow address is \n"+escrowAddr);

        
        P2WSH txn = new P2WSH(Bolt3Util.NetworkType.REGTEST, fundingTXId, fundingVout , escrowScript, amount);        
        
        
        
        
        ECKey[] keys = new ECKey[1];
        keys[0] = pair;
        String signature = txn.signature(keys, redeemScript, amount);
        System.out.println(String.format("signature\n%s",signature));
        
        
    }
    
//    public static String btcToSatoshi(String _){
//        String base = "0000000000000000";
//        Double btc = Double.parseDouble(_);
//        btc = btc * 100000000;
//        Long _satoshis = btc.longValue();
//        String __ = Long.toHexString(_satoshis);
//        return String.format("%s%s", base.substring(0,16-__.length()) ,__ );
//    }
    
    public enum Weight{
        INITIAL_WEIGHT(724);
        private int numVal;
        Weight(int numVal) {
            this.numVal = numVal;
        }
        public int getNumVal() {
            return numVal;
        }
    }
    
    public static String amountAfterFee(String amount, String fee){
        String base = "0000000000000000";
        Long amt = new BigInteger(amount, 16).longValue();
        Long feePerKW = new BigInteger(fee, 16).longValue();
        Long spendingAmt = amt - ((feePerKW * Weight.INITIAL_WEIGHT.getNumVal())/1000);
        System.out.println(amt);
        System.out.println(feePerKW);
        System.out.println(spendingAmt);
        //Long _spendAmt = spendingAmt.longValue();
        String __ = Long.toHexString(spendingAmt);
        return String.format("%s%s", base.substring(0,16-__.length()) ,__ );

    }
    
    public static boolean signatureVerify(String fundingTXId, String fundingVout, String fundingAmount, String selfDelay, String revocationPubKey, String localDelayedPubKey , String remotePubKey, String pubKey1, String pubKey2, String sig, String feePerKW, String seq, String locktime) throws Exception{
        
        BitcoinClient bc = new BitcoinClient("http://blazon:blazon123@192.168.1.101:7332/");
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        byte witnessVersion=0;
        
        String spendingAmount = amountAfterFee(fundingAmount,feePerKW);
        String redeemScript = "";
        if(ECDH_BC.compareKeys(Utils.hexStringToByteArray(pubKey1), Utils.hexStringToByteArray(pubKey2)) > 0){
            redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",pubKey2,pubKey1);
        }else{
            redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",pubKey1,pubKey2);
        }
        
        String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(P2WSH.scriptToHash(redeemScript)));
        bc.getBitcoin().importAddress(multiSigAddr, null, true);
        System.out.println("redeem address is \n"+multiSigAddr);
        
        
        String escrowScript = String.format("OP_IF %s OP_ELSE %s OP_CHECKSEQUENCEVERIFY OP_DROP %s OP_ENDIF OP_CHECKSIG",revocationPubKey, selfDelay, localDelayedPubKey );
        String escrowAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(P2WSH.scriptToHash(escrowScript)));
        bc.getBitcoin().importAddress(escrowAddr, null, true);
        System.out.println("escrow address is \n"+escrowAddr);

        
        P2WSH2 txn = new P2WSH2(Bolt3Util.NetworkType.REGTEST, fundingTXId, fundingVout , remotePubKey, spendingAmount, seq, locktime);        
        
        boolean res = txn.verifySignature(redeemScript, fundingAmount, sig, pubKey1);
        
        System.out.println(String.format("Signature verified : %B",res));
        
        return res;
    }
    
    public static byte[] signatureGenerate(String fundingTXId, String fundingVout, String fundingAmount, String selfDelay, String revocationPubKey, String localDelayedPubKey , String remotePubKey, String pubKey1, String pubKey2, String sig, String feePerKW, String seq, String locktime, byte[] localPvtKey) throws Exception{
        
        System.out.println("revocation key => "+revocationPubKey);
        System.out.println("delayed key => "+localDelayedPubKey);
        
        BitcoinClient bc = new BitcoinClient("http://blazon:blazon123@192.168.1.101:7332/");
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        byte witnessVersion=0;
        
        String spendingAmount = amountAfterFee(fundingAmount,feePerKW);
        String redeemScript = "";
        if(ECDH_BC.compareKeys(Utils.hexStringToByteArray(pubKey1), Utils.hexStringToByteArray(pubKey2)) > 0){
            redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",pubKey2,pubKey1);
        }else{
            redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",pubKey1,pubKey2);
        }
        
        String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(P2WSH.scriptToHash(redeemScript)));
        bc.getBitcoin().importAddress(multiSigAddr, null, true);
        System.out.println("redeem address is \n"+multiSigAddr);
        
        
        String escrowScript = String.format("OP_IF %s OP_ELSE %s OP_CHECKSEQUENCEVERIFY OP_DROP %s OP_ENDIF OP_CHECKSIG",revocationPubKey, P2PKH.reverse(selfDelay), localDelayedPubKey );
        System.out.println("escrowScript => "+escrowScript);
        String escrowAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(P2WSH.scriptToHash(escrowScript)));
        bc.getBitcoin().importAddress(escrowAddr, null, true);
        System.out.println("escrow address is \n"+escrowAddr);

        
        P2WSH2 txn = new P2WSH2(Bolt3Util.NetworkType.REGTEST, fundingTXId, fundingVout , remotePubKey, spendingAmount, seq, locktime, escrowScript);        
        
        //boolean res = txn.verifySignature(redeemScript, fundingAmount, sig, pubKey1);
        String hash = txn.signingHash(redeemScript, fundingAmount);
        
        byte[] localSignature = ECDH_BC.sign(Utils.hexStringToByteArray(hash), localPvtKey);
        
        
        return localSignature;
    }
    
    public static String signFromHexAndPKey(String hex, String pKey){
        
        String msg = ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(hex))));
        
        String wif =pKey;
        DumpedPrivateKey dpk = DumpedPrivateKey.fromBase58(null, wif);
        ECKey key = dpk.getKey();
        //String check = key.getPrivateKeyAsWiF(NetworkParameters.regTests());
        Sha256Hash hash = Sha256Hash.wrap(msg);
        ECDSASignature sig = key.sign(hash);
        byte[] res = sig.encodeToDER();
        String ret = DatatypeConverter.printHexBinary(res); 

        return ret;
    }
    
    public static void generateKeys() throws Exception{
                ECKey pairA = new ECKey();
                //ECKey pairV = new ECKey();
                SegwitAddressUtil su = SegwitAddressUtil.getInstance();
                byte witnessVersion=0;
                String addrA = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairA.getPubKeyHash());
                System.out.println(pairA.getPrivateKeyAsWiF(NetworkParameters.regTests()));
                System.out.println(pairA.getPrivateKeyAsHex());
                System.out.println(pairA.getPublicKeyAsHex());
                System.out.println("Address A : "+addrA);

                //System.out.println(pairV.getPrivateKeyAsWiF(NetworkParameters.regTests()));
                //System.out.println(pairV.getPrivateKeyAsHex());
                //System.out.println(pairV.getPublicKeyAsHex());

                /*
                String hex = "0200000001598e5f579891f2697d016be178c30d9c6b2c1ced52d40d4381dfadbb53281b340100000017a914d5f279229b655c5cf2279cfed9d2eff658a95eb287ffffffff01c0a1fc53020000001976a9145aad571fdc499cde422361a129d360a8f1f46da388ac0000000001000000";
                String hexDH = ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(hex))));
                
                Sha256Hash hash = Sha256Hash.wrap(hexDH);
                ECKey.ECDSASignature s = pairU.sign(hash);
                byte[] res = s.encodeToDER();
                String test = DatatypeConverter.printHexBinary(res);
                System.out.println(test);
                */
                
    }
    public static void main(String args[]) throws JsonProcessingException, Exception{
        
        /*       
        Testcase 1: Testing P2PKH transaction
        */
        
        //testP2PKH(null);
       
        /*       
        Testcase 2: Testing P2PKH transaction with our signing function
        */

        //testP2PKHCustom(null);
       
        /*       
        Testcase 3: Testing P2SH transaction        
        */
        
        //testP2SH(null);
               
        /*       
        Testcase 4: Testing P2SH MultiSig transaction       
        */
        
        //testP2SHMultiSig(null);

 
        /*       
        Testcase 5: Testing P2SH MultiSig transaction with our signing function
        */
        
        //testP2SHMultiSigCustom(null);
        
        /*       
        Testcase 6: Testing native P2WPKH transaction    
        */

        //testP2WPKH(null);
        
        /*       
        Testcase 7: Testing native P2WPKH transaction with our signing function
        */

        //testP2WPKHCustom(null);
        
        /*       
        Testcase 8: Testing native P2WSH transaction    
        */

        //testP2WSH(null);
        
        /*       
        Testcase 9: Testing native P2WSH transaction with own signing algorithm 
        */

        //testP2WSHCustom(null);

        /*       
        Testcase 10: Testing bolt3 P2WSH transactions
        */

       // testP2WSHBolt3(null);

        
        //String _sig = signFromHexAndPKey("0200000035eef8dcae96917f35be8e8150d250b93b9498421ed74d621945057c0bce9fb63bb13029ce7b1f559ef5e747fcac439f1455a2ec7c5f09b72290795e70665044bef6f8fcb6bb84f91a5b2510ec69f38188c69d35b55ed35d42d7c0d1d535b89200000000475221036599fe4bf52a609a63ac7f609cf1763064f10391f2c8be6b916478beaa42fd0b210395dce56dea0a1fbaccfd83e94f3a7dd2ccd6d9e60d5a0280c14340505a0ccc2852aec0a1fc5302000000ffffffff4c91b0be6d70f68f143daa7e20cb4cb89acad918e4395c49919f0bf9f20b0aff0000000001000000","cUtEzZaEYukhDwe9ewgBFBFJ1LwuF61VighpooqoDLjC8b5GbiS2");
        
        //System.out.println(_sig.toLowerCase());
        /*
        String s = "805fed5302000000160014efea7537f04b197cb490854b1c1af1a79d262418";
        String tmp = ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(s))));
        System.out.println(tmp);
        */
        generateKeys();
        
        
        //testP2WSHBolt3(fundTXId, fundVout, amt, selfDelay, revocationPubKey, localDelayedPubKey , pubKey1, pubKey2, pair);
//        
//        String fundingTXId="94d2b20581a99b11d366e364dfa5005b1e81ff13ca0c86a9f2285844d0b47c3f";
//        String fundingVout="0000";
//        String fundingAmount="00000000000f4240";
//        String spendingAmount="00000000000f1ee6";
//        String selfDelay="0090";
//        String feePerKW="";
//        String revocationPubKey="";
//        String localDelayedPubKey="";
//        String fundingPubKey="02655b23fa226c8539e444a3d659d6adfd0876e5eb0cabef24c53a7ae7f6f35bbc";
//        String fundeePubKey="01e30359c4ac71fdad44a2cc5bb75855fff4077b7725cae53580f8e67f9b6a6d14";
//        String sig="4b72a3f7b2c01b0782b10624e4bf375abd4d1cb4b5acd208a65c975e2bd8e9550013ac608a705a7fe696a1a78b9bb386af1fb8a12ab3abc03fe6b68f94a8c6bc";
//        
        //signatureVerify(fundingTXId, fundingVout, fundingAmount, selfDelay, revocationPubKey, localDelayedPubKey , fundingPubKey, fundeePubKey, sig, feePerKW);
        /*
        String amt = amountAfterFee("00000000000f4240", "000030d4");
        System.out.println(amt);

        */

    }
    
    
    
}

