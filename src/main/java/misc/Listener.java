/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

/**
 *
 * @author neerajnagi
 */
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;


public class Listener extends AbstractActor {

  @Override
  public Receive createReceive() {
    return receiveBuilder()
      .matchAny( m -> {
        System.out.println("Listener Msg => "+m);
      })
      .build();
  }

  @Override
  public void preStart() {
      System.out.println("Listener PreStart Done");
  }
}