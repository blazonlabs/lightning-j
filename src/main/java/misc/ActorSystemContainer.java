/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import akka.actor.ActorSystem;

/**
 *
 * @author Abhimanyu
 */
public class ActorSystemContainer {
    private ActorSystem sys;
    private ActorSystemContainer() {
        //sys = ActorSystem.create("actor",ConfigFactory.load());
        sys = ActorSystem.create("boltbasic");
	System.out.println(sys.settings());
    }

    public ActorSystem getSystem() {
        return sys;
    }

    private static ActorSystemContainer instance = null;

    public static synchronized ActorSystemContainer getInstance() {
        if (instance == null) {
            instance = new ActorSystemContainer();
        }
        return instance;
    }
}
