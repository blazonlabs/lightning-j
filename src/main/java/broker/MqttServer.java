/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package broker;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import io.moquette.BrokerConstants;
import java.util.Arrays;
import java.util.List;

import io.moquette.interception.AbstractInterceptHandler;
import io.moquette.interception.InterceptHandler;
import io.moquette.interception.messages.InterceptPublishMessage;
import io.moquette.server.Server;
import io.moquette.server.config.MemoryConfig;
import io.netty.buffer.ByteBufUtil;
import java.nio.charset.Charset;
import java.util.Properties;
import misc.ActorSystemContainer;
import msg.CmdToInitiator;

public class MqttServer extends AbstractActor {
    final ActorSystem system = ActorSystemContainer.getInstance().getSystem();
    Properties props = new Properties();
    MemoryConfig config;
    Server mqttBroker;
    List<? extends InterceptHandler> userHandlers;
    ActorRef i;
            
    class PublisherListener extends AbstractInterceptHandler {
        @Override
        public void onPublish(InterceptPublishMessage message) {
            System.out.println("MqttServer broker message intercepted, topic: " + message.getTopicName() + ", content: " + new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8")));
            if(message.getTopicName().equals("ligntning/initiator/connect")){
                String msg = new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8"));
                String[] msgA = msg.split(",");
                if(msgA.length == 3){// IP, port, pubKey
                    i.tell(new CmdToInitiator(msgA[0], Integer.parseInt(msgA[1]), msgA[2]), getSelf());
                }
            }
            if(message.getTopicName().equals("ligntning/initiator/openchannel")){
                String msg = new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8"));
                String[] msgA = msg.split(",");
                if(msgA.length == 4){// IP, port, pubKey, amount
                    i.tell(message, getSelf());
                }
            }
        }

        @Override
        public String getID() {
            return "getID() call";
        }
    }
    
    public MqttServer() {
        System.out.println("MqttServer Constructor Start");
        props.setProperty(BrokerConstants.PORT_PROPERTY_NAME, Integer.toString(8883));
        props.setProperty(BrokerConstants.HOST_PROPERTY_NAME, "0.0.0.0");
        config = new MemoryConfig(props);
        userHandlers = Arrays.asList(new PublisherListener());
        System.out.println("MqttServer Constructor End");
    }

    public static Props props() {
        return Props.create(MqttServer.class);
    }

    @Override
    public void preStart() throws Exception {
        System.out.println("MqttServer PreStart Start");
        System.out.println("MqttServer PreStart End");
    }
 
    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .matchEquals("START_MQTT_BROKER", message -> {
                    ActorRef testz = system.actorSelection("/user/ZMQManage").anchor();
                    System.out.println(testz.path().toString());
                    System.out.println("MqttServer START_MQTT_BROKER => " + message);
                    try{
                        mqttBroker = new Server();
                        mqttBroker.startServer(config, userHandlers);
                        System.out.println("MqttServer Started");
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .matchEquals("STOP_MQTT_BROKER", message -> {
                    System.out.println("MqttServer START_MQTT_BROKER => " + message);
                    try{
                        mqttBroker.stopServer();
                        System.out.println("MqttServer Stopped");
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(ActorRef.class, ar -> {
                    try{
                        System.out.println("MqttServer msgAny => " + ar.path().toString());
                        i = ar;
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .matchAny(msgAny -> {
                    try{
                        System.out.println("MqttServer msgAny => " + msgAny);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .build();
    }
}
