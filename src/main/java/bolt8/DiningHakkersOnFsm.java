package bolt8;

import akka.actor.*;

public class DiningHakkersOnFsm {
  /**
   * Some states the chopstick can be in
   */
  public static enum CS {
    Available,
    Taken
  }

  /**
   * Some state container for the chopstick
   */
  public static final class TakenBy {
    public final ActorRef hakker;
    public TakenBy(ActorRef hakker){
      this.hakker = hakker;
    }
  }

  /*
  * A chopstick is an actor, it can be taken, and put back
  */
  public static class Chopstick extends AbstractLoggingFSM<CS, TakenBy> {
    {
    startWith(CS.Available, new TakenBy(context().system().deadLetters()));
/*
    when(CS.Available,
      matchEventEquals(Take, (take, data) ->
        goTo(CS.Taken).using(new TakenBy(sender())).replying(new Taken(self()))));

    when(CS.Taken,
      matchEventEquals(Take, (take, data) ->
        stay().replying(new Busy(self()))).
      event((event, data) -> (event == Put) && (data.hakker == sender()), (event, data) ->
        goTo(CS.Available).using(new TakenBy(context().system().deadLetters()))));
*/
    initialize();
    }
  }

  public static void convertToHex(byte[] arr){
      for(byte b: arr){
        String st = String.format("%02X", b);
        System.out.print(st);
      }
  }
  
  public static void main(String[] args) {
    //ActorSystem system = ActorSystem.create();
    //Create 5 chopsticks
   // ActorRef[] chopsticks = new ActorRef[5];
    
    
    
    
  }
}
