/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class ChannelAnnouncement {
    
    private byte[] nodeSignature1;
    private byte[] nodeSignature2;
    private byte[] bitcoinSignature1;
    private byte[] bitcoinSignature2;
    private byte[] len;
    private byte[] features;
    private byte[] chainHash;
    private byte[] shortChannelId;
    private byte[] nodeId1;
    private byte[] nodeId2;
    private byte[] bitcoinKey1;
    private byte[] bitcoinKey2;
    private int type = 256;
    
    public ChannelAnnouncement(byte[] message){
        int l = 0;
        nodeSignature1 = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("ChannelAnnouncement nodeSignature1 length = " + nodeSignature1.length + ", value = " + byteArrayToHexString(nodeSignature1));
        nodeSignature2 = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("ChannelAnnouncement nodeSignature2 length = " + nodeSignature2.length + ", value = " + byteArrayToHexString(nodeSignature2));
        bitcoinSignature1 = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("ChannelAnnouncement bitcoinSignature1 length = " + bitcoinSignature1.length + ", value = " + byteArrayToHexString(bitcoinSignature1));
        bitcoinSignature2 = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("ChannelAnnouncement bitcoinSignature2 length = " + bitcoinSignature2.length + ", value = " + byteArrayToHexString(bitcoinSignature2));
        len = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(len).getShort();
        System.out.println("ChannelAnnouncement len length = " + len.length + ", value = " + byteArrayToHexString(len));
        features = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("ChannelAnnouncement features length = " + features.length + ", value = " + byteArrayToHexString(features));
        chainHash = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("ChannelAnnouncement chainHash length = " + chainHash.length + ", value = " + byteArrayToHexString(chainHash));
        shortChannelId = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("ChannelAnnouncement shortChannelId length = " + shortChannelId.length + ", value = " + byteArrayToHexString(shortChannelId));
        nodeId1 = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("ChannelAnnouncement nodeId1 length = " + nodeId1.length + ", value = " + byteArrayToHexString(nodeId1));
        nodeId2 = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("ChannelAnnouncement nodeId2 length = " + nodeId2.length + ", value = " + byteArrayToHexString(nodeId2));
        bitcoinKey1 = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("ChannelAnnouncement bitcoinKey1 length = " + bitcoinKey1.length + ", value = " + byteArrayToHexString(bitcoinKey1));
        bitcoinKey2 = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("ChannelAnnouncement bitcoinKey2 length = " + bitcoinKey2.length + ", value = " + byteArrayToHexString(bitcoinKey2));
        System.out.println("ChannelAnnouncement l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(getNodeSignature1());
            outputStream.write(getNodeSignature2());
            outputStream.write(getBitcoinSignature1());
            outputStream.write(getBitcoinSignature2());
            outputStream.write(getLen());
            outputStream.write(getFeatures());
            outputStream.write(getChainHash());
            outputStream.write(getShortChannelId());
            outputStream.write(getNodeId1());
            outputStream.write(getNodeId2());
            outputStream.write(getBitcoinKey1());
            outputStream.write(getBitcoinKey2());
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the nodeSignature1
     */
    public byte[] getNodeSignature1() {
        return nodeSignature1;
    }

    /**
     * @param nodeSignature1 the nodeSignature1 to set
     */
    public void setNodeSignature1(byte[] nodeSignature1) {
        this.nodeSignature1 = nodeSignature1;
    }

    /**
     * @return the nodeSignature2
     */
    public byte[] getNodeSignature2() {
        return nodeSignature2;
    }

    /**
     * @param nodeSignature2 the nodeSignature2 to set
     */
    public void setNodeSignature2(byte[] nodeSignature2) {
        this.nodeSignature2 = nodeSignature2;
    }

    /**
     * @return the bitcoinSignature1
     */
    public byte[] getBitcoinSignature1() {
        return bitcoinSignature1;
    }

    /**
     * @param bitcoinSignature1 the bitcoinSignature1 to set
     */
    public void setBitcoinSignature1(byte[] bitcoinSignature1) {
        this.bitcoinSignature1 = bitcoinSignature1;
    }

    /**
     * @return the bitcoinSignature2
     */
    public byte[] getBitcoinSignature2() {
        return bitcoinSignature2;
    }

    /**
     * @param bitcoinSignature2 the bitcoinSignature2 to set
     */
    public void setBitcoinSignature2(byte[] bitcoinSignature2) {
        this.bitcoinSignature2 = bitcoinSignature2;
    }

    /**
     * @return the len
     */
    public byte[] getLen() {
        return len;
    }

    /**
     * @param len the len to set
     */
    public void setLen(byte[] len) {
        this.len = len;
    }

    /**
     * @return the features
     */
    public byte[] getFeatures() {
        return features;
    }

    /**
     * @param features the features to set
     */
    public void setFeatures(byte[] features) {
        this.features = features;
    }

    /**
     * @return the chainHash
     */
    public byte[] getChainHash() {
        return chainHash;
    }

    /**
     * @param chainHash the chainHash to set
     */
    public void setChainHash(byte[] chainHash) {
        this.chainHash = chainHash;
    }

    /**
     * @return the shortChannelId
     */
    public byte[] getShortChannelId() {
        return shortChannelId;
    }

    /**
     * @param shortChannelId the shortChannelId to set
     */
    public void setShortChannelId(byte[] shortChannelId) {
        this.shortChannelId = shortChannelId;
    }

    /**
     * @return the nodeId1
     */
    public byte[] getNodeId1() {
        return nodeId1;
    }

    /**
     * @param nodeId1 the nodeId1 to set
     */
    public void setNodeId1(byte[] nodeId1) {
        this.nodeId1 = nodeId1;
    }

    /**
     * @return the nodeId2
     */
    public byte[] getNodeId2() {
        return nodeId2;
    }

    /**
     * @param nodeId2 the nodeId2 to set
     */
    public void setNodeId2(byte[] nodeId2) {
        this.nodeId2 = nodeId2;
    }

    /**
     * @return the bitcoinKey1
     */
    public byte[] getBitcoinKey1() {
        return bitcoinKey1;
    }

    /**
     * @param bitcoinKey1 the bitcoinKey1 to set
     */
    public void setBitcoinKey1(byte[] bitcoinKey1) {
        this.bitcoinKey1 = bitcoinKey1;
    }

    /**
     * @return the bitcoinKey2
     */
    public byte[] getBitcoinKey2() {
        return bitcoinKey2;
    }

    /**
     * @param bitcoinKey2 the bitcoinKey2 to set
     */
    public void setBitcoinKey2(byte[] bitcoinKey2) {
        this.bitcoinKey2 = bitcoinKey2;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
