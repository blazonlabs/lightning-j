/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import akka.actor.ActorRef;

/**
 *
 * @author Abhimanyu
 */
public class FundingTxIdStatus {
    private String ftxid;
    private ActorRef peer;
    private int minimumDepth;
    private boolean txIdError;
    private boolean txIdVerifed;
    private String txIdErrorMessage;
    private boolean txInSystem;
    
    public FundingTxIdStatus(String ftxid, ActorRef peer, int minimumDepth){
        this.ftxid = ftxid;
        this.peer = peer;
        this.minimumDepth = minimumDepth;
        this.txIdError = false;
        this.txIdVerifed = false;
        this.txIdErrorMessage = "";
        this.txInSystem = false;
    }

    /**
     * @return the ftxid
     */
    public String getFtxid() {
        return ftxid;
    }

    /**
     * @return the peer
     */
    public ActorRef getPeer() {
        return peer;
    }

    /**
     * @return the minimumDepth
     */
    public int getMinimumDepth() {
        return minimumDepth;
    }

    /**
     * @return the txIdError
     */
    public boolean isTxIdError() {
        return txIdError;
    }

    /**
     * @param txIdError the txIdError to set
     */
    public void setTxIdError(boolean txIdError) {
        this.txIdError = txIdError;
    }

    /**
     * @return the txIdVerifed
     */
    public boolean isTxIdVerifed() {
        return txIdVerifed;
    }

    /**
     * @param txIdVerifed the txIdVerifed to set
     */
    public void setTxIdVerifed(boolean txIdVerifed) {
        this.txIdVerifed = txIdVerifed;
    }

    /**
     * @return the txIdErrorMessage
     */
    public String getTxIdErrorMessage() {
        return txIdErrorMessage;
    }

    /**
     * @param txIdErrorMessage the txIdErrorMessage to set
     */
    public void setTxIdErrorMessage(String txIdErrorMessage) {
        this.txIdErrorMessage = txIdErrorMessage;
    }

    /**
     * @return the txInSystem
     */
    public boolean isTxInSystem() {
        return txInSystem;
    }

    /**
     * @param txInSystem the txInSystem to set
     */
    public void setTxInSystem(boolean txInSystem) {
        this.txInSystem = txInSystem;
    }
    
}
