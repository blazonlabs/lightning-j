/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import bitcoin.BitTrans;
import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class FundingCreated {
    
    private byte[] temporaryChannelId;
    private byte[] fundingTxid;
    private byte[] fundingOutputIndex;
    private byte[] signature;
    private byte[] signatureA;
    private int type = 34;
    
    public FundingCreated(byte[] message){
        int l = 0;
        temporaryChannelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("FundingCreated temporaryChannelId length = " + temporaryChannelId.length + ", value = " + byteArrayToHexString(temporaryChannelId));
        fundingTxid = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("FundingCreated fundingTxid length = " + fundingTxid.length + ", value = " + byteArrayToHexString(fundingTxid));
        fundingOutputIndex = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("FundingCreated fundingOutputIndex length = " + fundingOutputIndex.length + ", value = " + byteArrayToHexString(fundingOutputIndex));
        signature = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("FundingCreated signature length = " + signature.length + ", value = " + byteArrayToHexString(signature));
        byte[] r = Arrays.copyOfRange(signature, 0, 32);
        byte[] s = Arrays.copyOfRange(signature, 32, signature.length);
        r = Utils.concatAll(Utils.hexStringToByteArray("30440220"), r);
        s = Utils.concatAll(Utils.hexStringToByteArray("0220"), s);
        signatureA = Utils.concatAll(r, s);
        System.out.println("FundingCreated signatureA length = " + signatureA.length + ", value = " + byteArrayToHexString(signatureA));
        System.out.println("FundingCreated l = "+l);
    }

    public FundingCreated(OpenChannel openChannel, AcceptChannel acceptChannel, BitTrans bt, Keys keys, byte[] seq, byte[] locktime){
        this.fundingTxid = bt.txidGenerate(Utils.hexStringToByteArray("d259090563d6b79a0b14012d4f191818ba0cc83e37600d9e08fc8559f049e4d2"), openChannel.getFundingSatoshis(), openChannel.getFundingPubkey(), acceptChannel.getFundingPubkey(), seq, locktime, Utils.hexStringToByteArray("e956b51a072dac4ccf7db722330695932dcdabb1afc626764fbe63810e9b9214"));
        this.temporaryChannelId = openChannel.getTemporaryChannelId();
        this.fundingOutputIndex = Utils.hexStringToByteArray("0000");
        this.signature = bt.sigGenerate(this.fundingTxid, this.fundingOutputIndex, openChannel.getFundingSatoshis() , keys.getTheirRemotePubKey() ,openChannel.getFundingPubkey(), acceptChannel.getFundingPubkey(), openChannel.getFeeratePerKw() ,seq, locktime, Utils.hexStringToByteArray("e956b51a072dac4ccf7db722330695932dcdabb1afc626764fbe63810e9b9214"));
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(temporaryChannelId);
            outputStream.write(fundingTxid);
            outputStream.write(fundingOutputIndex);
            outputStream.write(signature);
            System.out.println("FundingCreated Message : "+Utils.byteArrayToHexString(outputStream.toByteArray( )));
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the temporaryChannelId
     */
    public byte[] getTemporaryChannelId() {
        return temporaryChannelId;
    }

    /**
     * @param temporaryChannelId the temporaryChannelId to set
     */
    public void setTemporaryChannelId(byte[] temporaryChannelId) {
        this.temporaryChannelId = temporaryChannelId;
    }

    /**
     * @return the fundingTxid
     */
    public byte[] getFundingTxid() {
        return fundingTxid;
    }

    /**
     * @param fundingTxid the fundingTxid to set
     */
    public void setFundingTxid(byte[] fundingTxid) {
        this.fundingTxid = fundingTxid;
    }

    /**
     * @return the fundingOutputIndex
     */
    public byte[] getFundingOutputIndex() {
        return fundingOutputIndex;
    }

    /**
     * @param fundingOutputIndex the fundingOutputIndex to set
     */
    public void setFundingOutputIndex(byte[] fundingOutputIndex) {
        this.fundingOutputIndex = fundingOutputIndex;
    }

    /**
     * @return the signature
     */
    public byte[] getSignature() {
        return signature;
    }

    /**
     * @param signature the signature to set
     */
    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the signatureA
     */
    public byte[] getSignatureA() {
        return signatureA;
    }

    /**
     * @param signatureA the signatureA to set
     */
    public void setSignatureA(byte[] signatureA) {
        this.signatureA = signatureA;
    }
    
    
}
