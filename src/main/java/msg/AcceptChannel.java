/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.KeyUtils;
import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class AcceptChannel {
    
    private byte[] temporaryChannelId;
    private byte[] dustLimitSatoshis;
    private byte[] maxHtlcValueInFlightMsat;
    private byte[] channelReserveSatoshis;
    private byte[] htlcMinimumMsat;
    private byte[] minimumDepth;
    private byte[] toSelfDelay;
    private byte[] maxAcceptedHtlcs;
    private byte[] fundingPubkey;
    private byte[] revocationBasepoint;
    private byte[] paymentBasepoint;
    private byte[] delayedPaymentBasepoint;
    private byte[] htlcBasepoint;
    private byte[] firstPerCommitmentPoint;
    private byte[] shutdownLen;
    private byte[] shutdownScriptpubkey;
    private int type = 33;
    
    public AcceptChannel(byte[] message){
        int l = 0;
        temporaryChannelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("AcceptChannel temporaryChannelId length = " + temporaryChannelId.length + ", value = " + byteArrayToHexString(temporaryChannelId));
        dustLimitSatoshis = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("AcceptChannel dustLimitSatoshis length = " + dustLimitSatoshis.length + ", value = " + byteArrayToHexString(dustLimitSatoshis));
        maxHtlcValueInFlightMsat = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("AcceptChannel maxHtlcValueInFlightMsat length = " + maxHtlcValueInFlightMsat.length + ", value = " + byteArrayToHexString(maxHtlcValueInFlightMsat));
        channelReserveSatoshis = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("AcceptChannel channelReserveSatoshis length = " + channelReserveSatoshis.length + ", value = " + byteArrayToHexString(channelReserveSatoshis));
        htlcMinimumMsat = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("AcceptChannel htlcMinimumMsat length = " + htlcMinimumMsat.length + ", value = " + byteArrayToHexString(htlcMinimumMsat));
        minimumDepth = Arrays.copyOfRange(message, l, l+4);
        l = l + 4;
        System.out.println("AcceptChannel minimumDepth length = " + minimumDepth.length + ", value = " + byteArrayToHexString(minimumDepth));
        toSelfDelay = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("AcceptChannel toSelfDelay length = " + toSelfDelay.length + ", value = " + byteArrayToHexString(toSelfDelay));
        maxAcceptedHtlcs = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("AcceptChannel maxAcceptedHtlcs length = " + maxAcceptedHtlcs.length + ", value = " + byteArrayToHexString(maxAcceptedHtlcs));
        fundingPubkey = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("AcceptChannel fundingPubkey length = " + fundingPubkey.length + ", value = " + byteArrayToHexString(fundingPubkey));
        revocationBasepoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("AcceptChannel revocationBasepoint length = " + revocationBasepoint.length + ", value = " + byteArrayToHexString(revocationBasepoint));
        paymentBasepoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("AcceptChannel paymentBasepoint length = " + paymentBasepoint.length + ", value = " + byteArrayToHexString(paymentBasepoint));
        delayedPaymentBasepoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("AcceptChannel delayedPaymentBasepoint length = " + delayedPaymentBasepoint.length + ", value = " + byteArrayToHexString(delayedPaymentBasepoint));
        htlcBasepoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("AcceptChannel htlc_basepoint length = " + htlcBasepoint.length + ", value = " + byteArrayToHexString(htlcBasepoint));
        firstPerCommitmentPoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("AcceptChannel firstPerCommitmentPoint length = " + firstPerCommitmentPoint.length + ", value = " + byteArrayToHexString(firstPerCommitmentPoint));
        shutdownLen = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(shutdownLen).getShort();
        System.out.println("AcceptChannel shutdownLen length = " + shutdownLen.length + ", value = " + byteArrayToHexString(shutdownLen));
        if(lenBytes != 0)
            shutdownScriptpubkey = Arrays.copyOfRange(message, l, l+lenBytes);
        else
            shutdownScriptpubkey = new byte[0];
        l = l + lenBytes;
        System.out.println("AcceptChannel shutdownScriptpubkey length = " + shutdownScriptpubkey.length + ", value = " + byteArrayToHexString(shutdownScriptpubkey));
        System.out.println("AcceptChannel l = "+l);
    }

    public AcceptChannel(OpenChannel openChannel, Keys keys) {
        this.temporaryChannelId = openChannel.getTemporaryChannelId();
        this.dustLimitSatoshis = openChannel.getChannelReserveSatoshis();
        this.channelReserveSatoshis = openChannel.getDustLimitSatoshis();
        this.htlcMinimumMsat = openChannel.getHtlcMinimumMsat();
        this.maxHtlcValueInFlightMsat = openChannel.getMaxHtlcValueInFlightMsat();
        this.maxAcceptedHtlcs = openChannel.getMaxAcceptedHtlcs();
        this.toSelfDelay = openChannel.getToSelfDelay();
        this.shutdownLen = openChannel.getShutdownLen();
        this.shutdownScriptpubkey = openChannel.getShutdownScriptpubkey();
        this.fundingPubkey = keys.getLocalFundingPubKey();
        this.revocationBasepoint = keys.getLocalRevocationBasePoint();
        this.paymentBasepoint = keys.getLocalPaymentBasePoint();
        this.delayedPaymentBasepoint = keys.getLocalDelayedPaymentBasePoint();
        this.htlcBasepoint = keys.getLocalHtlcBasePoint();
        this.firstPerCommitmentPoint = keys.getLocalPerCommitmentPoint();
        this.minimumDepth = Utils.getHexOfIntWithBytes(3, 4);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(temporaryChannelId);
            outputStream.write(dustLimitSatoshis);
            outputStream.write(maxHtlcValueInFlightMsat);
            outputStream.write(channelReserveSatoshis);
            outputStream.write(htlcMinimumMsat);
            outputStream.write(minimumDepth);
            outputStream.write(toSelfDelay);
            outputStream.write(maxAcceptedHtlcs);
            outputStream.write(fundingPubkey);
            outputStream.write(revocationBasepoint);
            outputStream.write(paymentBasepoint);
            outputStream.write(delayedPaymentBasepoint);
            outputStream.write(htlcBasepoint);
            outputStream.write(firstPerCommitmentPoint);
            outputStream.write(shutdownLen);
            outputStream.write(shutdownScriptpubkey);
            System.out.println("AcceptChannel Message : "+Utils.byteArrayToHexString(outputStream.toByteArray( )));
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the temporaryChannelId
     */
    public byte[] getTemporaryChannelId() {
        return temporaryChannelId;
    }

    /**
     * @param temporaryChannelId the temporaryChannelId to set
     */
    public void setTemporaryChannelId(byte[] temporaryChannelId) {
        this.temporaryChannelId = temporaryChannelId;
    }

    /**
     * @return the dustLimitSatoshis
     */
    public byte[] getDustLimitSatoshis() {
        return dustLimitSatoshis;
    }

    /**
     * @param dustLimitSatoshis the dustLimitSatoshis to set
     */
    public void setDustLimitSatoshis(byte[] dustLimitSatoshis) {
        this.dustLimitSatoshis = dustLimitSatoshis;
    }

    /**
     * @return the maxHtlcValueInFlightMsat
     */
    public byte[] getMaxHtlcValueInFlightMsat() {
        return maxHtlcValueInFlightMsat;
    }

    /**
     * @param maxHtlcValueInFlightMsat the maxHtlcValueInFlightMsat to set
     */
    public void setMaxHtlcValueInFlightMsat(byte[] maxHtlcValueInFlightMsat) {
        this.maxHtlcValueInFlightMsat = maxHtlcValueInFlightMsat;
    }

    /**
     * @return the channelReserveSatoshis
     */
    public byte[] getChannelReserveSatoshis() {
        return channelReserveSatoshis;
    }

    /**
     * @param channelReserveSatoshis the channelReserveSatoshis to set
     */
    public void setChannelReserveSatoshis(byte[] channelReserveSatoshis) {
        this.channelReserveSatoshis = channelReserveSatoshis;
    }

    /**
     * @return the htlcMinimumMsat
     */
    public byte[] getHtlcMinimumMsat() {
        return htlcMinimumMsat;
    }

    /**
     * @param htlcMinimumMsat the htlcMinimumMsat to set
     */
    public void setHtlcMinimumMsat(byte[] htlcMinimumMsat) {
        this.htlcMinimumMsat = htlcMinimumMsat;
    }

    /**
     * @return the minimumDepth
     */
    public byte[] getMinimumDepth() {
        return minimumDepth;
    }

    /**
     * @param minimumDepth the minimumDepth to set
     */
    public void setMinimumDepth(byte[] minimumDepth) {
        this.minimumDepth = minimumDepth;
    }

    /**
     * @return the toSelfDelay
     */
    public byte[] getToSelfDelay() {
        return toSelfDelay;
    }

    /**
     * @param toSelfDelay the toSelfDelay to set
     */
    public void setToSelfDelay(byte[] toSelfDelay) {
        this.toSelfDelay = toSelfDelay;
    }

    /**
     * @return the maxAcceptedHtlcs
     */
    public byte[] getMaxAcceptedHtlcs() {
        return maxAcceptedHtlcs;
    }

    /**
     * @param maxAcceptedHtlcs the maxAcceptedHtlcs to set
     */
    public void setMaxAcceptedHtlcs(byte[] maxAcceptedHtlcs) {
        this.maxAcceptedHtlcs = maxAcceptedHtlcs;
    }

    /**
     * @return the fundingPubkey
     */
    public byte[] getFundingPubkey() {
        return fundingPubkey;
    }

    /**
     * @param fundingPubkey the fundingPubkey to set
     */
    public void setFundingPubkey(byte[] fundingPubkey) {
        this.fundingPubkey = fundingPubkey;
    }

    /**
     * @return the revocationBasepoint
     */
    public byte[] getRevocationBasepoint() {
        return revocationBasepoint;
    }

    /**
     * @param revocationBasepoint the revocationBasepoint to set
     */
    public void setRevocationBasepoint(byte[] revocationBasepoint) {
        this.revocationBasepoint = revocationBasepoint;
    }

    /**
     * @return the paymentBasepoint
     */
    public byte[] getPaymentBasepoint() {
        return paymentBasepoint;
    }

    /**
     * @param paymentBasepoint the paymentBasepoint to set
     */
    public void setPaymentBasepoint(byte[] paymentBasepoint) {
        this.paymentBasepoint = paymentBasepoint;
    }

    /**
     * @return the delayedPaymentBasepoint
     */
    public byte[] getDelayedPaymentBasepoint() {
        return delayedPaymentBasepoint;
    }

    /**
     * @param delayedPaymentBasepoint the delayedPaymentBasepoint to set
     */
    public void setDelayedPaymentBasepoint(byte[] delayedPaymentBasepoint) {
        this.delayedPaymentBasepoint = delayedPaymentBasepoint;
    }

    /**
     * @return the htlcBasepoint
     */
    public byte[] getHtlcBasepoint() {
        return htlcBasepoint;
    }

    /**
     * @param htlcBasepoint the htlcBasepoint to set
     */
    public void setHtlcBasepoint(byte[] htlcBasepoint) {
        this.htlcBasepoint = htlcBasepoint;
    }

    /**
     * @return the firstPerCommitmentPoint
     */
    public byte[] getFirstPerCommitmentPoint() {
        return firstPerCommitmentPoint;
    }

    /**
     * @param firstPerCommitmentPoint the firstPerCommitmentPoint to set
     */
    public void setFirstPerCommitmentPoint(byte[] firstPerCommitmentPoint) {
        this.firstPerCommitmentPoint = firstPerCommitmentPoint;
    }

    /**
     * @return the shutdownLen
     */
    public byte[] getShutdownLen() {
        return shutdownLen;
    }

    /**
     * @param shutdownLen the shutdownLen to set
     */
    public void setShutdownLen(byte[] shutdownLen) {
        this.shutdownLen = shutdownLen;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the shutdownScriptpubkey
     */
    public byte[] getShutdownScriptpubkey() {
        return shutdownScriptpubkey;
    }

    /**
     * @param shutdownScriptpubkey the shutdownScriptpubkey to set
     */
    public void setShutdownScriptpubkey(byte[] shutdownScriptpubkey) {
        this.shutdownScriptpubkey = shutdownScriptpubkey;
    }
}
