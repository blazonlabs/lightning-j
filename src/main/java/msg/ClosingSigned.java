/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class ClosingSigned {
    
    private byte[] channelId;
    private byte[] feeSatoshis;
    private byte[] signature;
    private int type = 39;
    
    public ClosingSigned(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("ClosingSigned channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        feeSatoshis = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("ClosingSigned feeSatoshis length = " + feeSatoshis.length + ", value = " + byteArrayToHexString(feeSatoshis));
        signature = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("ClosingSigned shutdownLen length = " + signature.length + ", value = " + byteArrayToHexString(signature));
        System.out.println("FundingSigned l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(feeSatoshis);
            outputStream.write(signature);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the feeSatoshis
     */
    public byte[] getFeeSatoshis() {
        return feeSatoshis;
    }

    /**
     * @param feeSatoshis the feeSatoshis to set
     */
    public void setFeeSatoshis(byte[] feeSatoshis) {
        this.feeSatoshis = feeSatoshis;
    }

    /**
     * @return the signature
     */
    public byte[] getSignature() {
        return signature;
    }

    /**
     * @param signature the signature to set
     */
    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
