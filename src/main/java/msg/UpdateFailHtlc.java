/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class UpdateFailHtlc {
    
    private byte[] channelId;
    private byte[] id;
    private byte[] len;
    private byte[] reason;
    private int type = 131;
    
    public UpdateFailHtlc(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("UpdateFailHtlc channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        id = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("UpdateFailHtlc id length = " + id.length + ", value = " + byteArrayToHexString(id));
        len = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(len).getShort();
        System.out.println("UpdateFailHtlc len length = " + len.length + ", value = " + byteArrayToHexString(len));
        reason = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("UpdateFailHtlc reason length = " + reason.length + ", value = " + byteArrayToHexString(reason));
        System.out.println("UpdateFailHtlc l = "+l);
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(id);
            outputStream.write(len);
            outputStream.write(reason);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the id
     */
    public byte[] getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(byte[] id) {
        this.id = id;
    }

    /**
     * @return the len
     */
    public byte[] getLen() {
        return len;
    }

    /**
     * @param len the len to set
     */
    public void setLen(byte[] len) {
        this.len = len;
    }

    /**
     * @return the reason
     */
    public byte[] getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(byte[] reason) {
        this.reason = reason;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
