/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import bitcoin.BitcoinUtils;
import ec.Hash;
import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class Transaction {
    private byte[] version;
    private byte[] marker;
    private byte[] flag;
    private byte[] noOfTxIn;
    private int noOfTxIns;
    private byte[][] txIn;
    private byte[] noOfTxOut;
    private int noOfTxOuts;
    private byte[][] txOut;
    private byte[] noOfWitness;
    private int noOfWitnesses;
    private byte[][] witness;
    private byte[] locktime;
    private byte[] hash;
    private byte[] txid;
    
    public Transaction(byte[] rawTx){
        int l = 0;
        version = Arrays.copyOfRange(rawTx, l, l+4);
        l = l + 4;
        System.out.println("Transaction version length = " + version.length + ", value = " + byteArrayToHexString(version));
        marker = Arrays.copyOfRange(rawTx, l, l+1);
        l = l + 1;
        System.out.println("Transaction marker length = " + marker.length + ", value = " + byteArrayToHexString(marker));
        flag = Arrays.copyOfRange(rawTx, l, l+1);
        l = l + 1;
        System.out.println("Transaction flag length = " + flag.length + ", value = " + byteArrayToHexString(flag));
        noOfTxIn = Arrays.copyOfRange(rawTx, l, l+1);
        l = l + 1;
        noOfTxIns = ByteBuffer.wrap(noOfTxIn).get();
        System.out.println("Transaction noOfTxIn length = " + noOfTxIn.length + ", value = " + byteArrayToHexString(noOfTxIn));
        System.out.println("Transaction noOfTxIns = " + noOfTxIns);
        txIn = new byte[noOfTxIns][];
        for(int i=0;i<noOfTxIns;i++){
            txIn[i] = Arrays.copyOfRange(rawTx, l, l+41);
            l = l + 41;
            System.out.println("Transaction txIn " + i + " length = " + txIn[i].length + ", value = " + byteArrayToHexString(txIn[i]));
        }
        noOfTxOut = Arrays.copyOfRange(rawTx, l, l+1);
        l = l + 1;
        noOfTxOuts = ByteBuffer.wrap(noOfTxOut).get();
        System.out.println("Transaction noOfTxOut length = " + noOfTxOut.length + ", value = " + byteArrayToHexString(noOfTxOut));
        System.out.println("Transaction noOfTxOuts = " + noOfTxOuts);
        int j = l;
        txOut = new byte[noOfTxOuts][];
        for(int i=0;i<noOfTxOuts;i++){
            byte[] amount = Arrays.copyOfRange(rawTx, j, j+8);
            j = j + 8;
            byte[] len = Arrays.copyOfRange(rawTx, j, j+1);
            int length = ByteBuffer.wrap(len).get();
            txOut[i] = Arrays.copyOfRange(rawTx, l, l+8+1+length);
            l = l + 8 + 1 + length;
            j = l;
            System.out.println("Transaction txOut " + i + " length = " + txOut[i].length + ", value = " + byteArrayToHexString(txOut[i]));
        }
        noOfWitness = Arrays.copyOfRange(rawTx, l, l+1);
        l = l + 1;
        noOfWitnesses = ByteBuffer.wrap(noOfWitness).get();
        System.out.println("Transaction noOfWitness length = " + noOfWitness.length + ", value = " + byteArrayToHexString(noOfWitness));
        System.out.println("Transaction noOfWitnesses = " + noOfWitnesses);
        j = l; 
        witness = new byte[noOfWitnesses][];
        for(int i=0;i<noOfWitnesses;i++){
            byte[] len = Arrays.copyOfRange(rawTx, j, j+1);
            int length = ByteBuffer.wrap(len).get();
            witness[i] = Arrays.copyOfRange(rawTx, l, l+1+length);
            l = l + 1 + length;
            j = l;
            System.out.println("Transaction witness " + i + " length = " + witness[i].length + ", value = " + byteArrayToHexString(witness[i]));
        }
        locktime = Arrays.copyOfRange(rawTx, l, l+4);
        l = l + 4;
        System.out.println("Transaction locktime length = " + locktime.length + ", value = " + byteArrayToHexString(locktime));
        System.out.println("Transaction l = "+l);
        hash = Utils.reverseHexArray(Hash.sha256(Hash.sha256(rawTx)));
        System.out.println("Transaction hash length = " + hash.length + ", value = " + byteArrayToHexString(hash));
        //txid = reverse(sha256(sha256(version + txins + txouts + locktime)))
        txid = Utils.reverseHexArray(Hash.sha256(Hash.sha256(getTxidRaw())));
        System.out.println("Transaction txid length = " + txid.length + ", value = " + byteArrayToHexString(txid));
    }

    public byte[] getTxidRaw(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(this.version);
            outputStream.write(this.noOfTxIn);
            for(int i=0;i<this.noOfTxIns;i++){
                outputStream.write(this.txIn[i]);
            }
            outputStream.write(this.noOfTxOut);
            for(int i=0;i<this.noOfTxOuts;i++){
                outputStream.write(this.txOut[i]);
            }
            outputStream.write(this.locktime);
            System.out.println("Transaction getTxidRaw : "+Utils.byteArrayToHexString(outputStream.toByteArray( )));
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the version
     */
    public byte[] getVersion() {
        return version;
    }

    /**
     * @return the marker
     */
    public byte[] getMarker() {
        return marker;
    }

    /**
     * @return the flag
     */
    public byte[] getFlag() {
        return flag;
    }

    /**
     * @return the noOfTxIn
     */
    public byte[] getNoOfTxIn() {
        return noOfTxIn;
    }

    /**
     * @return the noOfTxIns
     */
    public int getNoOfTxIns() {
        return noOfTxIns;
    }

    /**
     * @return the txIn
     */
    public byte[][] getTxIn() {
        return txIn;
    }

    /**
     * @return the noOfTxOut
     */
    public byte[] getNoOfTxOut() {
        return noOfTxOut;
    }

    /**
     * @return the noOfTxOuts
     */
    public int getNoOfTxOuts() {
        return noOfTxOuts;
    }

    /**
     * @return the txOut
     */
    public byte[][] getTxOut() {
        return txOut;
    }

    /**
     * @return the noOfWitness
     */
    public byte[] getNoOfWitness() {
        return noOfWitness;
    }

    /**
     * @return the noOfWitnesses
     */
    public int getNoOfWitnesses() {
        return noOfWitnesses;
    }

    /**
     * @return the witness
     */
    public byte[][] getWitness() {
        return witness;
    }

    /**
     * @return the locktime
     */
    public byte[] getLocktime() {
        return locktime;
    }
    
    public static void main(String[] args){
        byte[] raw = Utils.hexStringToByteArray("01000000000101390002ce85b6f678b8fc8478a709f1181d04fd6ff056f97b50bf4835d3e1c2360100000000ffffffff0240420f000000000022002064d06e7bcc3e110fc2f6ff5511a1f5dde49482f225e4dc5dd04cd27a1969ff21bc27584817000000160014573a5455ed6d792660a0fc85f532706c264ea4e002483045022100819e80c1298dcb546e88a212744a45d1f0bffe5f2a8865a51c30b5cd2901aa6902204b7a4691259bf058109c30bd12a17cc7e15589debf46cd98b9db5004fdac9e800121034d70decc9418df04bfb9f6436bbfa86cfe756910e4bd9be1b74888b72c40fb2500000000");
        Transaction trs = new Transaction(raw);
    }

    /**
     * @return the hash
     */
    public byte[] getHash() {
        return hash;
    }

    /**
     * @return the txid
     */
    public byte[] getTxid() {
        return txid;
    }
}
