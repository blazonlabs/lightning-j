/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class Init {
    
    private byte[] gflen;
    private byte[] globalfeatures;
    private byte[] lflen;
    private byte[] localfeatures;
    private int type = 16;
    
    public Init(byte[] message){
        int l = 0;
        gflen = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(gflen).getShort();
        System.out.println("Init gflen length = " + gflen.length + ", value = " + byteArrayToHexString(gflen));
        globalfeatures = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("Init globalfeatures length = " + globalfeatures.length + ", value = " + byteArrayToHexString(globalfeatures));
        lflen = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        lenBytes = ByteBuffer.wrap(lflen).getShort();
        System.out.println("Init lflen length = " + lflen.length + ", value = " + byteArrayToHexString(lflen));
        localfeatures = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("Init localfeatures length = " + localfeatures.length + ", value = " + byteArrayToHexString(localfeatures));
        System.out.println("Init l = "+l);
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(gflen);
            outputStream.write(globalfeatures);
            outputStream.write(lflen);
            outputStream.write(localfeatures);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the gflen
     */
    public byte[] getGflen() {
        return gflen;
    }

    /**
     * @param gflen the gflen to set
     */
    public void setGflen(byte[] gflen) {
        this.gflen = gflen;
    }

    /**
     * @return the globalfeatures
     */
    public byte[] getGlobalfeatures() {
        return globalfeatures;
    }

    /**
     * @param globalfeatures the globalfeatures to set
     */
    public void setGlobalfeatures(byte[] globalfeatures) {
        this.globalfeatures = globalfeatures;
    }

    /**
     * @return the lflen
     */
    public byte[] getLflen() {
        return lflen;
    }

    /**
     * @param lflen the lflen to set
     */
    public void setLflen(byte[] lflen) {
        this.lflen = lflen;
    }

    /**
     * @return the localfeatures
     */
    public byte[] getLocalfeatures() {
        return localfeatures;
    }

    /**
     * @param localfeatures the localfeatures to set
     */
    public void setLocalfeatures(byte[] localfeatures) {
        this.localfeatures = localfeatures;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
    
}
