/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class FundingSigned {
    
    private byte[] channelId;
    private byte[] signature;
    private int type = 35;
    
    public FundingSigned(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("FundingSigned channel_id length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        signature = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("FundingSigned shutdownLen length = " + signature.length + ", value = " + byteArrayToHexString(signature));
        System.out.println("FundingSigned l = "+l);
    }
    
    public FundingSigned(FundingCreated fundingCreated, byte[] sig){
        channelId = Utils.getBigXOR(fundingCreated.getFundingTxid(), fundingCreated.getFundingOutputIndex());
        signature = sig;
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(signature);
            System.out.println("FundingSigned Message : "+Utils.byteArrayToHexString(outputStream.toByteArray( )));
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the channel_id
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channel_id to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the signature
     */
    public byte[] getSignature() {
        return signature;
    }

    /**
     * @param signature the signature to set
     */
    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
