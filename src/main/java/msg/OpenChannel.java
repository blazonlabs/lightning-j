/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import bitcoin.BitTrans;
import bitcoin.BitcoinClient;
import ec.ECDH_BC;
import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class OpenChannel {
    
    private byte[] chainHash;
    private byte[] temporaryChannelId;
    private byte[] fundingSatoshis;
    private byte[] pushMsat;
    private byte[] dustLimitSatoshis;
    private byte[] maxHtlcValueInFlightMsat;
    private byte[] channelReserveSatoshis;
    private byte[] htlcMinimumMsat;
    private byte[] feeratePerKw;
    private byte[] toSelfDelay;
    private byte[] maxAcceptedHtlcs;
    private byte[] fundingPubkey;
    private byte[] revocationBasepoint;
    private byte[] paymentBasepoint;
    private byte[] delayedPaymentBasepoint;
    private byte[] htlcBasepoint;
    private byte[] firstPerCommitmentPoint;
    private byte[] channelFlags;
    private byte[] shutdownLen;
    private byte[] shutdownScriptpubkey;
    private int type = 32;
    
    public OpenChannel(byte[] message){
        int l = 0;
        chainHash = Arrays.copyOfRange(message, 0, 32);
        l = l + 32;
        System.out.println("OpenChannel chainHash length = " + chainHash.length + ", value = " + byteArrayToHexString(chainHash));
        temporaryChannelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("OpenChannel temporaryChannelId length = " + temporaryChannelId.length + ", value = " + byteArrayToHexString(temporaryChannelId));
        fundingSatoshis = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("OpenChannel fundingSatoshis length = " + fundingSatoshis.length + ", value = " + byteArrayToHexString(fundingSatoshis));
        pushMsat = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("OpenChannel pushMsat length = " + pushMsat.length + ", value = " + byteArrayToHexString(pushMsat));
        dustLimitSatoshis = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("OpenChannel dustLimitSatoshis length = " + dustLimitSatoshis.length + ", value = " + byteArrayToHexString(dustLimitSatoshis));
        maxHtlcValueInFlightMsat = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("OpenChannel maxHtlcValueInFlightMsat length = " + maxHtlcValueInFlightMsat.length + ", value = " + byteArrayToHexString(maxHtlcValueInFlightMsat));
        channelReserveSatoshis = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("OpenChannel channelReserveSatoshis length = " + channelReserveSatoshis.length + ", value = " + byteArrayToHexString(channelReserveSatoshis));
        htlcMinimumMsat = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("OpenChannel htlcMinimumMsat length = " + htlcMinimumMsat.length + ", value = " + byteArrayToHexString(htlcMinimumMsat));
        feeratePerKw = Arrays.copyOfRange(message, l, l+4);
        l = l + 4;
        System.out.println("OpenChannel feeratePerKw length = " + feeratePerKw.length + ", value = " + byteArrayToHexString(feeratePerKw));
        toSelfDelay = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("OpenChannel toSelfDelay length = " + toSelfDelay.length + ", value = " + byteArrayToHexString(toSelfDelay));
        maxAcceptedHtlcs = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("OpenChannel maxAcceptedHtlcs length = " + maxAcceptedHtlcs.length + ", value = " + byteArrayToHexString(maxAcceptedHtlcs));
        fundingPubkey = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("OpenChannel fundingPubkey length = " + fundingPubkey.length + ", value = " + byteArrayToHexString(fundingPubkey));
        revocationBasepoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("OpenChannel revocationBasepoint length = " + revocationBasepoint.length + ", value = " + byteArrayToHexString(revocationBasepoint));
        paymentBasepoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("OpenChannel paymentBasepoint length = " + paymentBasepoint.length + ", value = " + byteArrayToHexString(paymentBasepoint));
        delayedPaymentBasepoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("OpenChannel delayedPaymentBasepoint length = " + delayedPaymentBasepoint.length + ", value = " + byteArrayToHexString(delayedPaymentBasepoint));
        htlcBasepoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("OpenChannel htlc_basepoint length = " + htlcBasepoint.length + ", value = " + byteArrayToHexString(htlcBasepoint));
        firstPerCommitmentPoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("OpenChannel firstPerCommitmentPoint length = " + firstPerCommitmentPoint.length + ", value = " + byteArrayToHexString(firstPerCommitmentPoint));
        channelFlags = Arrays.copyOfRange(message, l, l+1);
        l = l + 1;
        System.out.println("OpenChannel channelFlags length = " + channelFlags.length + ", value = " + byteArrayToHexString(channelFlags));
        shutdownLen = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(shutdownLen).getShort();
        System.out.println("OpenChannel shutdownLen length = " + shutdownLen.length + ", value = " + byteArrayToHexString(shutdownLen));
        if(lenBytes != 0)
            shutdownScriptpubkey = Arrays.copyOfRange(message, l, l+lenBytes);
        else
            shutdownScriptpubkey = new byte[0];
        l = l + lenBytes;
        System.out.println("OpenChannel shutdownScriptpubkey length = " + shutdownScriptpubkey.length + ", value = " + byteArrayToHexString(shutdownScriptpubkey));
        System.out.println("OpenChannel l = "+l);
    }
    
    public OpenChannel(BitTrans bt, Keys keys, String amount){
        this.chainHash = bt.getBu().reverse(Utils.hexStringToByteArray(bt.getBc().getBitcoin().getBlock(0).hash()));
        this.temporaryChannelId = Utils.hexStringToByteArray("b5396f7a0f167e7bcc696e999581e4b44c96b41a7f7a5cb72dfba9c9b7df6998");
        long amt = Long.parseLong(amount);
        this.fundingSatoshis = Utils.getHexOfLongWithBytes(amt, 8);
        this.pushMsat = Utils.hexStringToByteArray("0000000000000000");
        this.dustLimitSatoshis = Utils.hexStringToByteArray("000000000000023d");
        this.maxHtlcValueInFlightMsat = Utils.hexStringToByteArray("000000003b023380");
        this.channelReserveSatoshis = Utils.hexStringToByteArray("0000000000002710");
        this.htlcMinimumMsat = Utils.hexStringToByteArray("00000000000003e8");
        this.feeratePerKw = Utils.hexStringToByteArray("000030d4");
        this.toSelfDelay = Utils.hexStringToByteArray("0090");
        this.maxAcceptedHtlcs = Utils.hexStringToByteArray("01e3");
        //this.fundingPubkey = keys.getLocalFundingPubKey();
        this.fundingPubkey = Utils.hexStringToByteArray("03e4506178e1fc22bcd7f1b4825f6d1adb169a5976cd9dea1d4e52e7b3696dcc7b");
        this.revocationBasepoint = keys.getLocalRevocationBasePoint();
        this.paymentBasepoint = keys.getLocalPaymentBasePoint();
        this.delayedPaymentBasepoint = keys.getLocalDelayedPaymentBasePoint();
        this.htlcBasepoint = keys.getLocalHtlcBasePoint();
        this.firstPerCommitmentPoint = keys.getLocalPerCommitmentPoint();
        this.channelFlags = Utils.hexStringToByteArray("01");
        this.shutdownLen = Utils.hexStringToByteArray("0000");
        this.shutdownScriptpubkey = new byte[0];
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(chainHash);
            outputStream.write(temporaryChannelId);
            outputStream.write(fundingSatoshis);
            outputStream.write(pushMsat);
            outputStream.write(dustLimitSatoshis);
            outputStream.write(maxHtlcValueInFlightMsat);
            outputStream.write(channelReserveSatoshis);
            outputStream.write(htlcMinimumMsat);
            outputStream.write(feeratePerKw);
            outputStream.write(toSelfDelay);
            outputStream.write(maxAcceptedHtlcs);
            outputStream.write(fundingPubkey);
            outputStream.write(revocationBasepoint);
            outputStream.write(paymentBasepoint);
            outputStream.write(delayedPaymentBasepoint);
            outputStream.write(htlcBasepoint);
            outputStream.write(firstPerCommitmentPoint);
            outputStream.write(channelFlags);
            outputStream.write(shutdownLen);
            outputStream.write(getShutdownScriptpubkey());
            System.out.println("OpenChannel Message : "+Utils.byteArrayToHexString(outputStream.toByteArray( )));
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the chainHash
     */
    public byte[] getChainHash() {
        return chainHash;
    }

    /**
     * @param chainHash the chainHash to set
     */
    public void setChainHash(byte[] chainHash) {
        this.chainHash = chainHash;
    }

    /**
     * @return the temporaryChannelId
     */
    public byte[] getTemporaryChannelId() {
        return temporaryChannelId;
    }

    /**
     * @param temporaryChannelId the temporaryChannelId to set
     */
    public void setTemporaryChannelId(byte[] temporaryChannelId) {
        this.temporaryChannelId = temporaryChannelId;
    }

    /**
     * @return the fundingSatoshis
     */
    public byte[] getFundingSatoshis() {
        return fundingSatoshis;
    }

    /**
     * @param fundingSatoshis the fundingSatoshis to set
     */
    public void setFundingSatoshis(byte[] fundingSatoshis) {
        this.fundingSatoshis = fundingSatoshis;
    }

    /**
     * @return the pushMsat
     */
    public byte[] getPushMsat() {
        return pushMsat;
    }

    /**
     * @param pushMsat the pushMsat to set
     */
    public void setPushMsat(byte[] pushMsat) {
        this.pushMsat = pushMsat;
    }

    /**
     * @return the dustLimitSatoshis
     */
    public byte[] getDustLimitSatoshis() {
        return dustLimitSatoshis;
    }

    /**
     * @param dustLimitSatoshis the dustLimitSatoshis to set
     */
    public void setDustLimitSatoshis(byte[] dustLimitSatoshis) {
        this.dustLimitSatoshis = dustLimitSatoshis;
    }

    /**
     * @return the maxHtlcValueInFlightMsat
     */
    public byte[] getMaxHtlcValueInFlightMsat() {
        return maxHtlcValueInFlightMsat;
    }

    /**
     * @param maxHtlcValueInFlightMsat the maxHtlcValueInFlightMsat to set
     */
    public void setMaxHtlcValueInFlightMsat(byte[] maxHtlcValueInFlightMsat) {
        this.maxHtlcValueInFlightMsat = maxHtlcValueInFlightMsat;
    }

    /**
     * @return the channelReserveSatoshis
     */
    public byte[] getChannelReserveSatoshis() {
        return channelReserveSatoshis;
    }

    /**
     * @param channelReserveSatoshis the channelReserveSatoshis to set
     */
    public void setChannelReserveSatoshis(byte[] channelReserveSatoshis) {
        this.channelReserveSatoshis = channelReserveSatoshis;
    }

    /**
     * @return the htlcMinimumMsat
     */
    public byte[] getHtlcMinimumMsat() {
        return htlcMinimumMsat;
    }

    /**
     * @param htlcMinimumMsat the htlcMinimumMsat to set
     */
    public void setHtlcMinimumMsat(byte[] htlcMinimumMsat) {
        this.htlcMinimumMsat = htlcMinimumMsat;
    }

    /**
     * @return the feeratePerKw
     */
    public byte[] getFeeratePerKw() {
        return feeratePerKw;
    }

    /**
     * @param feeratePerKw the feeratePerKw to set
     */
    public void setFeeratePerKw(byte[] feeratePerKw) {
        this.feeratePerKw = feeratePerKw;
    }

    /**
     * @return the toSelfDelay
     */
    public byte[] getToSelfDelay() {
        return toSelfDelay;
    }

    /**
     * @param toSelfDelay the toSelfDelay to set
     */
    public void setToSelfDelay(byte[] toSelfDelay) {
        this.toSelfDelay = toSelfDelay;
    }

    /**
     * @return the maxAcceptedHtlcs
     */
    public byte[] getMaxAcceptedHtlcs() {
        return maxAcceptedHtlcs;
    }

    /**
     * @param maxAcceptedHtlcs the maxAcceptedHtlcs to set
     */
    public void setMaxAcceptedHtlcs(byte[] maxAcceptedHtlcs) {
        this.maxAcceptedHtlcs = maxAcceptedHtlcs;
    }

    /**
     * @return the fundingPubkey
     */
    public byte[] getFundingPubkey() {
        return fundingPubkey;
    }

    /**
     * @param fundingPubkey the fundingPubkey to set
     */
    public void setFundingPubkey(byte[] fundingPubkey) {
        this.fundingPubkey = fundingPubkey;
    }

    /**
     * @return the revocationBasepoint
     */
    public byte[] getRevocationBasepoint() {
        return revocationBasepoint;
    }

    /**
     * @param revocationBasepoint the revocationBasepoint to set
     */
    public void setRevocationBasepoint(byte[] revocationBasepoint) {
        this.revocationBasepoint = revocationBasepoint;
    }

    /**
     * @return the paymentBasepoint
     */
    public byte[] getPaymentBasepoint() {
        return paymentBasepoint;
    }

    /**
     * @param paymentBasepoint the paymentBasepoint to set
     */
    public void setPaymentBasepoint(byte[] paymentBasepoint) {
        this.paymentBasepoint = paymentBasepoint;
    }

    /**
     * @return the delayedPaymentBasepoint
     */
    public byte[] getDelayedPaymentBasepoint() {
        return delayedPaymentBasepoint;
    }

    /**
     * @param delayedPaymentBasepoint the delayedPaymentBasepoint to set
     */
    public void setDelayedPaymentBasepoint(byte[] delayedPaymentBasepoint) {
        this.delayedPaymentBasepoint = delayedPaymentBasepoint;
    }

    /**
     * @return the htlcBasepoint
     */
    public byte[] getHtlcBasepoint() {
        return htlcBasepoint;
    }

    /**
     * @param htlcBasepoint the htlcBasepoint to set
     */
    public void setHtlcBasepoint(byte[] htlcBasepoint) {
        this.htlcBasepoint = htlcBasepoint;
    }

    /**
     * @return the firstPerCommitmentPoint
     */
    public byte[] getFirstPerCommitmentPoint() {
        return firstPerCommitmentPoint;
    }

    /**
     * @param firstPerCommitmentPoint the firstPerCommitmentPoint to set
     */
    public void setFirstPerCommitmentPoint(byte[] firstPerCommitmentPoint) {
        this.firstPerCommitmentPoint = firstPerCommitmentPoint;
    }

    /**
     * @return the channelFlags
     */
    public byte[] getChannelFlags() {
        return channelFlags;
    }

    /**
     * @param channelFlags the channelFlags to set
     */
    public void setChannelFlags(byte[] channelFlags) {
        this.channelFlags = channelFlags;
    }

    /**
     * @return the shutdownLen
     */
    public byte[] getShutdownLen() {
        return shutdownLen;
    }

    /**
     * @param shutdownLen the shutdownLen to set
     */
    public void setShutdownLen(byte[] shutdownLen) {
        this.shutdownLen = shutdownLen;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the shutdownScriptpubkey
     */
    public byte[] getShutdownScriptpubkey() {
        return shutdownScriptpubkey;
    }

    /**
     * @param shutdownScriptpubkey the shutdownScriptpubkey to set
     */
    public void setShutdownScriptpubkey(byte[] shutdownScriptpubkey) {
        this.shutdownScriptpubkey = shutdownScriptpubkey;
    }
    
}
