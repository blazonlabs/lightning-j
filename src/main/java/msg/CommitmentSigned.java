/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class CommitmentSigned {
    
    private byte[] channelId;
    private byte[] signature;
    private byte[] numHtlcs;
    private byte[] htlcSignature;
    private int type = 132;
    
    public CommitmentSigned(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("CommitmentSigned channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        signature = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("CommitmentSigned signature length = " + signature.length + ", value = " + byteArrayToHexString(signature));
        numHtlcs = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(numHtlcs).getShort();
        System.out.println("CommitmentSigned numHtlcs length = " + numHtlcs.length + ", value = " + byteArrayToHexString(numHtlcs));
        htlcSignature = Arrays.copyOfRange(message, l, l+(lenBytes*64));
        l = l + (lenBytes*64);
        System.out.println("CommitmentSigned scriptpubkey length = " + htlcSignature.length + ", value = " + byteArrayToHexString(htlcSignature));
        System.out.println("CommitmentSigned l = "+l);
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(signature);
            outputStream.write(numHtlcs);
            outputStream.write(htlcSignature);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the signature
     */
    public byte[] getSignature() {
        return signature;
    }

    /**
     * @param signature the signature to set
     */
    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    /**
     * @return the numHtlcs
     */
    public byte[] getNumHtlcs() {
        return numHtlcs;
    }

    /**
     * @param numHtlcs the numHtlcs to set
     */
    public void setNumHtlcs(byte[] numHtlcs) {
        this.numHtlcs = numHtlcs;
    }

    /**
     * @return the htlcSignature
     */
    public byte[] getHtlcSignature() {
        return htlcSignature;
    }

    /**
     * @param htlcSignature the htlcSignature to set
     */
    public void setHtlcSignature(byte[] htlcSignature) {
        this.htlcSignature = htlcSignature;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
