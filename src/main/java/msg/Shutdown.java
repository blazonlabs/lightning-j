/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class Shutdown {
    
    private byte[] channelId;
    private byte[] len;
    private byte[] scriptpubkey;
    private int type = 38;
    
    public Shutdown(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("Shutdown channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        len = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(len).getShort();
        System.out.println("Shutdown len length = " + len.length + ", value = " + byteArrayToHexString(len));
        scriptpubkey = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("Shutdown scriptpubkey length = " + scriptpubkey.length + ", value = " + byteArrayToHexString(scriptpubkey));
        System.out.println("Shutdown l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(len);
            outputStream.write(scriptpubkey);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the len
     */
    public byte[] getLen() {
        return len;
    }

    /**
     * @param len the len to set
     */
    public void setLen(byte[] len) {
        this.len = len;
    }

    /**
     * @return the scriptpubkey
     */
    public byte[] getScriptpubkey() {
        return scriptpubkey;
    }

    /**
     * @param scriptpubkey the scriptpubkey to set
     */
    public void setScriptpubkey(byte[] scriptpubkey) {
        this.scriptpubkey = scriptpubkey;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
