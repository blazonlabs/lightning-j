/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class AnnouncementSignatures {
    
    private byte[] channelId;
    private byte[] shortChannelId;
    private byte[] nodeSignature;
    private byte[] bitcoinSignature;
    private int type = 259;
    
    public AnnouncementSignatures(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("AnnouncementSignatures channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        shortChannelId = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("AnnouncementSignatures shortChannelId length = " + shortChannelId.length + ", value = " + byteArrayToHexString(shortChannelId));
        nodeSignature = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("AnnouncementSignatures nodeSignature length = " + nodeSignature.length + ", value = " + byteArrayToHexString(nodeSignature));
        bitcoinSignature = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("AnnouncementSignatures bitcoinSignature length = " + bitcoinSignature.length + ", value = " + byteArrayToHexString(bitcoinSignature));
        System.out.println("AnnouncementSignatures l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(getChannelId());
            outputStream.write(getShortChannelId());
            outputStream.write(getNodeSignature());
            outputStream.write(getBitcoinSignature());
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the shortChannelId
     */
    public byte[] getShortChannelId() {
        return shortChannelId;
    }

    /**
     * @param shortChannelId the shortChannelId to set
     */
    public void setShortChannelId(byte[] shortChannelId) {
        this.shortChannelId = shortChannelId;
    }

    /**
     * @return the nodeSignature
     */
    public byte[] getNodeSignature() {
        return nodeSignature;
    }

    /**
     * @param nodeSignature the nodeSignature to set
     */
    public void setNodeSignature(byte[] nodeSignature) {
        this.nodeSignature = nodeSignature;
    }

    /**
     * @return the bitcoinSignature
     */
    public byte[] getBitcoinSignature() {
        return bitcoinSignature;
    }

    /**
     * @param bitcoinSignature the bitcoinSignature to set
     */
    public void setBitcoinSignature(byte[] bitcoinSignature) {
        this.bitcoinSignature = bitcoinSignature;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
