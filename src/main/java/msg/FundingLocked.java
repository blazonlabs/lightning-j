/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class FundingLocked {
    
    private byte[] channelId;
    private byte[] nextPerCommitmentPoint;
    private int type = 36;
    
    public FundingLocked(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("FundingLocked channel_id length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        nextPerCommitmentPoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("FundingLocked nextPerCommitmentPoint length = " + nextPerCommitmentPoint.length + ", value = " + byteArrayToHexString(nextPerCommitmentPoint));
        System.out.println("FundingLocked l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(nextPerCommitmentPoint);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the nextPerCommitmentPoint
     */
    public byte[] getNextPerCommitmentPoint() {
        return nextPerCommitmentPoint;
    }

    /**
     * @param nextPerCommitmentPoint the nextPerCommitmentPoint to set
     */
    public void setNextPerCommitmentPoint(byte[] nextPerCommitmentPoint) {
        this.nextPerCommitmentPoint = nextPerCommitmentPoint;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
    
}
