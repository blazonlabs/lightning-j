/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import java.net.InetSocketAddress;

/**
 *
 * @author Abhimanyu
 */
public class CmdToInitiator {
    private String ip;
    private int port;
    private String rspub;
    
    public CmdToInitiator(String ip, int port, String rspub){
        this.ip = ip;
        this.port = port;
        this.rspub = rspub;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * @return the rspub
     */
    public String getRspub() {
        return rspub;
    }
    
    public byte[] getRspubBytes() {
        return Utils.hexStringToByteArray(getRspub());
    }

    /**
     * @param rspub the rspub to set
     */
    public void setRspub(String rspub) {
        this.rspub = rspub;
    }
    
    public InetSocketAddress getAddress(){
        return new InetSocketAddress(ip, port);
    }
}
