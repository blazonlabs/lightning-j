/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class Error {
    
    private byte[] channelId;
    private byte[] len;
    private byte[] data;
    private int type = 17;
    
    public Error(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("Error channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        len = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(len).getShort();
        System.out.println("Error len length = " + len.length + ", value = " + byteArrayToHexString(len));
        data = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("Error data length = " + data.length + ", value = " + byteArrayToHexString(data));
        System.out.println("Error l = "+l);
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(len);
            outputStream.write(data);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the len
     */
    public byte[] getLen() {
        return len;
    }

    /**
     * @param len the len to set
     */
    public void setLen(byte[] len) {
        this.len = len;
    }

    /**
     * @return the data
     */
    public byte[] getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(byte[] data) {
        this.data = data;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
    
}
