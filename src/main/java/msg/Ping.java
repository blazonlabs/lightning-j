/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class Ping {
    
    private byte[] numPongBytes;
    private byte[] byteslen;
    private byte[] ignored;
    private int type = 18;
    
    public Ping(byte[] message){
        int l = 0;
        numPongBytes = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("Ping numPongBytes length = " + numPongBytes.length + ", value = " + byteArrayToHexString(numPongBytes));
        byteslen = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(byteslen).getShort();
        System.out.println("Ping byteslen length = " + byteslen.length + ", value = " + byteArrayToHexString(byteslen));
        ignored = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("Ping ignored length = " + ignored.length + ", value = " + byteArrayToHexString(ignored));
        System.out.println("Ping l = "+l);
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(numPongBytes);
            outputStream.write(byteslen);
            outputStream.write(ignored);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the numPongBytes
     */
    public byte[] getNumPongBytes() {
        return numPongBytes;
    }

    /**
     * @param numPongBytes the numPongBytes to set
     */
    public void setNumPongBytes(byte[] numPongBytes) {
        this.numPongBytes = numPongBytes;
    }

    /**
     * @return the byteslen
     */
    public byte[] getByteslen() {
        return byteslen;
    }

    /**
     * @param byteslen the byteslen to set
     */
    public void setByteslen(byte[] byteslen) {
        this.byteslen = byteslen;
    }

    /**
     * @return the ignored
     */
    public byte[] getIgnored() {
        return ignored;
    }

    /**
     * @param ignored the ignored to set
     */
    public void setIgnored(byte[] ignored) {
        this.ignored = ignored;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
    
}
