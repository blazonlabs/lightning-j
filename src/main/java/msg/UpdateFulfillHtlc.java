/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class UpdateFulfillHtlc {
    
    private byte[] channelId;
    private byte[] id;
    private byte[] paymentPreimage;
    private int type = 130;
    
    public UpdateFulfillHtlc(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("UpdateFulfillHtlc channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        id = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("UpdateFulfillHtlc id length = " + id.length + ", value = " + byteArrayToHexString(id));
        paymentPreimage = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("UpdateFulfillHtlc paymentPreimage length = " + paymentPreimage.length + ", value = " + byteArrayToHexString(paymentPreimage));
        System.out.println("UpdateFulfillHtlc l = "+l);
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(id);
            outputStream.write(paymentPreimage);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the id
     */
    public byte[] getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(byte[] id) {
        this.id = id;
    }

    /**
     * @return the paymentPreimage
     */
    public byte[] getPaymentPreimage() {
        return paymentPreimage;
    }

    /**
     * @param paymentPreimage the paymentPreimage to set
     */
    public void setPaymentPreimage(byte[] paymentPreimage) {
        this.paymentPreimage = paymentPreimage;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
    
}
