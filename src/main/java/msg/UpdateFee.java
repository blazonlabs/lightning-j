/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class UpdateFee {
    
    private byte[] channelId;
    private byte[] feeratePerKw;
    private int type = 134;
    
    public UpdateFee(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("UpdateFee channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        feeratePerKw = Arrays.copyOfRange(message, l, l+4);
        l = l + 4;
        System.out.println("UpdateFee feeratePerKw length = " + feeratePerKw.length + ", value = " + byteArrayToHexString(feeratePerKw));
        System.out.println("UpdateFee l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(feeratePerKw);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the feeratePerKw
     */
    public byte[] getFeeratePerKw() {
        return feeratePerKw;
    }

    /**
     * @param feeratePerKw the feeratePerKw to set
     */
    public void setFeeratePerKw(byte[] feeratePerKw) {
        this.feeratePerKw = feeratePerKw;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
