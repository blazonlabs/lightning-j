/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class UpdateFailMalformedHtlc {
    
    private byte[] channelId;
    private byte[] id;
    private byte[] sha256OfOnion;
    private byte[] failureCode;
    private int type = 135;
    
    public UpdateFailMalformedHtlc(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("UpdateFailMalformedHtlc channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        id = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("UpdateFailMalformedHtlc id length = " + id.length + ", value = " + byteArrayToHexString(id));
        sha256OfOnion = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("UpdateFailMalformedHtlc sha256OfOnion length = " + sha256OfOnion.length + ", value = " + byteArrayToHexString(sha256OfOnion));
        failureCode = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("UpdateFailMalformedHtlc failureCode length = " + failureCode.length + ", value = " + byteArrayToHexString(failureCode));
        System.out.println("UpdateFailMalformedHtlc l = "+l);
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(id);
            outputStream.write(sha256OfOnion);
            outputStream.write(failureCode);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the id
     */
    public byte[] getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(byte[] id) {
        this.id = id;
    }

    /**
     * @return the sha256OfOnion
     */
    public byte[] getSha256OfOnion() {
        return sha256OfOnion;
    }

    /**
     * @param sha256OfOnion the sha256OfOnion to set
     */
    public void setSha256OfOnion(byte[] sha256OfOnion) {
        this.sha256OfOnion = sha256OfOnion;
    }

    /**
     * @return the failureCode
     */
    public byte[] getFailureCode() {
        return failureCode;
    }

    /**
     * @param failureCode the failureCode to set
     */
    public void setFailureCode(byte[] failureCode) {
        this.failureCode = failureCode;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
