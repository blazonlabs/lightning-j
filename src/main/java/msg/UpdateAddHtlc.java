/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class UpdateAddHtlc {
    
    private byte[] channelId;
    private byte[] id;
    private byte[] amountMsat;
    private byte[] paymentHash;
    private byte[] cltvExpiry;
    private byte[] onionRoutingPacket;
    private int type = 128;
    
    public UpdateAddHtlc(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("UpdateAddHtlc channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        id = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("UpdateAddHtlc id length = " + id.length + ", value = " + byteArrayToHexString(id));
        amountMsat = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("UpdateAddHtlc amountMsat length = " + amountMsat.length + ", value = " + byteArrayToHexString(amountMsat));
        paymentHash = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("UpdateAddHtlc paymentHash length = " + paymentHash.length + ", value = " + byteArrayToHexString(paymentHash));
        cltvExpiry = Arrays.copyOfRange(message, l, l+4);
        l = l + 4;
        System.out.println("UpdateAddHtlc cltvExpiry length = " + cltvExpiry.length + ", value = " + byteArrayToHexString(cltvExpiry));
        onionRoutingPacket = Arrays.copyOfRange(message, l, l+1366);
        l = l + 1366;
        System.out.println("UpdateAddHtlc onionRoutingPacket length = " + onionRoutingPacket.length + ", value = " + byteArrayToHexString(onionRoutingPacket));
        System.out.println("UpdateAddHtlc l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(id);
            outputStream.write(amountMsat);
            outputStream.write(paymentHash);
            outputStream.write(cltvExpiry);
            outputStream.write(onionRoutingPacket);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the id
     */
    public byte[] getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(byte[] id) {
        this.id = id;
    }

    /**
     * @return the amountMsat
     */
    public byte[] getAmountMsat() {
        return amountMsat;
    }

    /**
     * @param amountMsat the amountMsat to set
     */
    public void setAmountMsat(byte[] amountMsat) {
        this.amountMsat = amountMsat;
    }

    /**
     * @return the paymentHash
     */
    public byte[] getPaymentHash() {
        return paymentHash;
    }

    /**
     * @param paymentHash the paymentHash to set
     */
    public void setPaymentHash(byte[] paymentHash) {
        this.paymentHash = paymentHash;
    }

    /**
     * @return the cltvExpiry
     */
    public byte[] getCltvExpiry() {
        return cltvExpiry;
    }

    /**
     * @param cltvExpiry the cltvExpiry to set
     */
    public void setCltvExpiry(byte[] cltvExpiry) {
        this.cltvExpiry = cltvExpiry;
    }

    /**
     * @return the onionRoutingPacket
     */
    public byte[] getOnionRoutingPacket() {
        return onionRoutingPacket;
    }

    /**
     * @param onionRoutingPacket the onionRoutingPacket to set
     */
    public void setOnionRoutingPacket(byte[] onionRoutingPacket) {
        this.onionRoutingPacket = onionRoutingPacket;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
