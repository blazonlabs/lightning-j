/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class ChannelReestablish {
    
    private byte[] channelId;
    private byte[] nextLocalCommitmentNumber;
    private byte[] nextRemoteRevocationNumber;
    private byte[] yourLastPerCommitmentSecret;
    private byte[] myCurrentPerCommitmentPoint;
    private int type = 136;
    
    public ChannelReestablish(byte[] message){
        int l = 0;
        channelId = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("ChannelReestablish channelId length = " + channelId.length + ", value = " + byteArrayToHexString(channelId));
        nextLocalCommitmentNumber = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("ChannelReestablish nextLocalCommitmentNumber length = " + nextLocalCommitmentNumber.length + ", value = " + byteArrayToHexString(nextLocalCommitmentNumber));
        nextRemoteRevocationNumber = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("ChannelReestablish nextRemoteRevocationNumber length = " + nextRemoteRevocationNumber.length + ", value = " + byteArrayToHexString(nextRemoteRevocationNumber));
        yourLastPerCommitmentSecret = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("ChannelReestablish yourLastPerCommitmentSecret length = " + yourLastPerCommitmentSecret.length + ", value = " + byteArrayToHexString(yourLastPerCommitmentSecret));
        myCurrentPerCommitmentPoint = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("ChannelReestablish myCurrentPerCommitmentPoint length = " + myCurrentPerCommitmentPoint.length + ", value = " + byteArrayToHexString(myCurrentPerCommitmentPoint));
        System.out.println("ChannelReestablish l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(channelId);
            outputStream.write(nextLocalCommitmentNumber);
            outputStream.write(nextRemoteRevocationNumber);
            outputStream.write(yourLastPerCommitmentSecret);
            outputStream.write(myCurrentPerCommitmentPoint);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    /**
     * @return the channelId
     */
    public byte[] getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(byte[] channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the nextLocalCommitmentNumber
     */
    public byte[] getNextLocalCommitmentNumber() {
        return nextLocalCommitmentNumber;
    }

    /**
     * @param nextLocalCommitmentNumber the nextLocalCommitmentNumber to set
     */
    public void setNextLocalCommitmentNumber(byte[] nextLocalCommitmentNumber) {
        this.nextLocalCommitmentNumber = nextLocalCommitmentNumber;
    }

    /**
     * @return the nextRemoteRevocationNumber
     */
    public byte[] getNextRemoteRevocationNumber() {
        return nextRemoteRevocationNumber;
    }

    /**
     * @param nextRemoteRevocationNumber the nextRemoteRevocationNumber to set
     */
    public void setNextRemoteRevocationNumber(byte[] nextRemoteRevocationNumber) {
        this.nextRemoteRevocationNumber = nextRemoteRevocationNumber;
    }

    /**
     * @return the yourLastPerCommitmentSecret
     */
    public byte[] getYourLastPerCommitmentSecret() {
        return yourLastPerCommitmentSecret;
    }

    /**
     * @param yourLastPerCommitmentSecret the yourLastPerCommitmentSecret to set
     */
    public void setYourLastPerCommitmentSecret(byte[] yourLastPerCommitmentSecret) {
        this.yourLastPerCommitmentSecret = yourLastPerCommitmentSecret;
    }

    /**
     * @return the myCurrentPerCommitmentPoint
     */
    public byte[] getMyCurrentPerCommitmentPoint() {
        return myCurrentPerCommitmentPoint;
    }

    /**
     * @param myCurrentPerCommitmentPoint the myCurrentPerCommitmentPoint to set
     */
    public void setMyCurrentPerCommitmentPoint(byte[] myCurrentPerCommitmentPoint) {
        this.myCurrentPerCommitmentPoint = myCurrentPerCommitmentPoint;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
