/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class Pong {
    
    private byte[] byteslen;
    private byte[] ignored;
    private int type = 19;
    
    public Pong(byte[] message){
        int l = 0;
        byteslen = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(byteslen).getShort();
        System.out.println("Pong byteslen length = " + byteslen.length + ", value = " + byteArrayToHexString(byteslen));
        ignored = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("Pong ignored length = " + ignored.length + ", value = " + byteArrayToHexString(ignored));
        System.out.println("Pong l = "+l);
    }
    
    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(byteslen);
            outputStream.write(ignored);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the byteslen
     */
    public byte[] getByteslen() {
        return byteslen;
    }

    /**
     * @param byteslen the byteslen to set
     */
    public void setByteslen(byte[] byteslen) {
        this.byteslen = byteslen;
    }

    /**
     * @return the ignored
     */
    public byte[] getIgnored() {
        return ignored;
    }

    /**
     * @param ignored the ignored to set
     */
    public void setIgnored(byte[] ignored) {
        this.ignored = ignored;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
    
}
