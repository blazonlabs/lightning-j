/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class NodeAnnouncement {
    
    private byte[] signature;
    private byte[] flen;
    private byte[] features;
    private byte[] timestamp;
    private byte[] nodeId;
    private byte[] rgbColor;
    private byte[] alias;
    private byte[] addrlen;
    private byte[] addresses;
    private int type = 257;
    
    public NodeAnnouncement(byte[] message){
        int l = 0;
        signature = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("NodeAnnouncement signature length = " + signature.length + ", value = " + byteArrayToHexString(signature));
        flen = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        short lenBytes = ByteBuffer.wrap(flen).getShort();
        System.out.println("NodeAnnouncement flen length = " + flen.length + ", value = " + byteArrayToHexString(flen));
        features = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("NodeAnnouncement features length = " + features.length + ", value = " + byteArrayToHexString(features));
        timestamp = Arrays.copyOfRange(message, l, l+4);
        l = l + 4;
        System.out.println("NodeAnnouncement timestamp length = " + timestamp.length + ", value = " + byteArrayToHexString(timestamp));
        nodeId = Arrays.copyOfRange(message, l, l+33);
        l = l + 33;
        System.out.println("NodeAnnouncement nodeId length = " + nodeId.length + ", value = " + byteArrayToHexString(nodeId));
        rgbColor = Arrays.copyOfRange(message, l, l+3);
        l = l + 3;
        System.out.println("NodeAnnouncement rgbColor length = " + rgbColor.length + ", value = " + byteArrayToHexString(rgbColor));
        alias = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("NodeAnnouncement alias length = " + alias.length + ", value = " + byteArrayToHexString(alias));
        addrlen = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        lenBytes = ByteBuffer.wrap(addrlen).getShort();
        System.out.println("NodeAnnouncement addrlen length = " + addrlen.length + ", value = " + byteArrayToHexString(addrlen));
        addresses = Arrays.copyOfRange(message, l, l+lenBytes);
        l = l + lenBytes;
        System.out.println("NodeAnnouncement addresses length = " + addresses.length + ", value = " + byteArrayToHexString(addresses));
        System.out.println("NodeAnnouncement l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(signature);
            outputStream.write(flen);
            outputStream.write(features);
            outputStream.write(timestamp);
            outputStream.write(nodeId);
            outputStream.write(rgbColor);
            outputStream.write(alias);
            outputStream.write(addrlen);
            outputStream.write(addresses);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the signature
     */
    public byte[] getSignature() {
        return signature;
    }

    /**
     * @param signature the signature to set
     */
    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    /**
     * @return the flen
     */
    public byte[] getFlen() {
        return flen;
    }

    /**
     * @param flen the flen to set
     */
    public void setFlen(byte[] flen) {
        this.flen = flen;
    }

    /**
     * @return the features
     */
    public byte[] getFeatures() {
        return features;
    }

    /**
     * @param features the features to set
     */
    public void setFeatures(byte[] features) {
        this.features = features;
    }

    /**
     * @return the timestamp
     */
    public byte[] getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(byte[] timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the nodeId
     */
    public byte[] getNodeId() {
        return nodeId;
    }

    /**
     * @param nodeId the nodeId to set
     */
    public void setNodeId(byte[] nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * @return the rgbColor
     */
    public byte[] getRgbColor() {
        return rgbColor;
    }

    /**
     * @param rgbColor the rgbColor to set
     */
    public void setRgbColor(byte[] rgbColor) {
        this.rgbColor = rgbColor;
    }

    /**
     * @return the alias
     */
    public byte[] getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(byte[] alias) {
        this.alias = alias;
    }

    /**
     * @return the addrlen
     */
    public byte[] getAddrlen() {
        return addrlen;
    }

    /**
     * @param addrlen the addrlen to set
     */
    public void setAddrlen(byte[] addrlen) {
        this.addrlen = addrlen;
    }

    /**
     * @return the addresses
     */
    public byte[] getAddresses() {
        return addresses;
    }

    /**
     * @param addresses the addresses to set
     */
    public void setAddresses(byte[] addresses) {
        this.addresses = addresses;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
