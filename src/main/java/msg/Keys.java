/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.ECDH_BC;
import ec.KeyUtils;

/**
 *
 * @author Abhimanyu
 */
public class Keys {
    private KeyUtils localFundingKeys;
    private KeyUtils localRevocationPoint;
    private KeyUtils localPaymentPoint;
    private KeyUtils localDelayedPaymentPoint;
    private KeyUtils localHtlcPoint;
    private KeyUtils localFirstPerCommitmentPoint;
    private byte[] localPubKey;
    private byte[] remotePubKey;
    private byte[] localHtlcPubKey;
    private byte[] remoteHtlcPubKey;
    private byte[] localDelayedPubKey;
    private byte[] remoteDelayedPubKey;
    private byte[] theirDelayedPubKey;
    private byte[] revocationPubKey;
    private byte[] revocationPrvKey;
    private byte[] theirRemotePubKey;
    
    public Keys(){
        localFundingKeys = new KeyUtils();
        localRevocationPoint = new KeyUtils();
        localPaymentPoint = new KeyUtils();
        localDelayedPaymentPoint = new KeyUtils();
        localHtlcPoint = new KeyUtils();
        localFirstPerCommitmentPoint = new KeyUtils();
    }

    public void generate(OpenChannel openChannel){
        localPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), this.getLocalPaymentBasePoint());
        remotePubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), openChannel.getPaymentBasepoint());
        theirRemotePubKey = ECDH_BC.getPubKeyFromBasepoint(openChannel.getFirstPerCommitmentPoint(), this.getLocalPaymentBasePoint());
        
        localHtlcPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), this.getLocalHtlcBasePoint());
        remoteHtlcPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), openChannel.getHtlcBasepoint());
        
        localDelayedPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), this.getLocalDelayedPaymentBasePoint());
        remoteDelayedPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), openChannel.getDelayedPaymentBasepoint());
        theirDelayedPubKey = ECDH_BC.getPubKeyFromBasepoint(openChannel.getFirstPerCommitmentPoint(), openChannel.getDelayedPaymentBasepoint());
        
        revocationPubKey = ECDH_BC.getRevocationPubKey(openChannel.getFirstPerCommitmentPoint(), this.getLocalRevocationBasePoint());
    }
    
    public void generate(AcceptChannel acceptChannel){
        localPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), this.getLocalPaymentBasePoint());
        remotePubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), acceptChannel.getPaymentBasepoint());
        theirRemotePubKey = ECDH_BC.getPubKeyFromBasepoint(acceptChannel.getFirstPerCommitmentPoint(), this.getLocalPaymentBasePoint());
        
        localHtlcPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), this.getLocalHtlcBasePoint());
        remoteHtlcPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), acceptChannel.getHtlcBasepoint());
        
        localDelayedPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), this.getLocalDelayedPaymentBasePoint());
        remoteDelayedPubKey = ECDH_BC.getPubKeyFromBasepoint(this.getLocalPerCommitmentPoint(), acceptChannel.getDelayedPaymentBasepoint());
        theirDelayedPubKey = ECDH_BC.getPubKeyFromBasepoint(acceptChannel.getFirstPerCommitmentPoint(), acceptChannel.getDelayedPaymentBasepoint());
        
        revocationPubKey = ECDH_BC.getRevocationPubKey(acceptChannel.getFirstPerCommitmentPoint(), this.getLocalRevocationBasePoint());
    }
    
    /**
     * @return the localFundingKeys
     */
    public KeyUtils getLocalFundingKeys() {
        return localFundingKeys;
    }

    /**
     * @return the localFundingPubKey
     */
    public byte[] getLocalFundingPubKey() {
        return localFundingKeys.getPub();
    }
    
    /**
     * @return the localFundingPrvKey
     */
    public byte[] getLocalFundingPrvKey() {
        return localFundingKeys.getPrv();
    }
    
    /**
     * @param localFundingKeys the localFundingKeys to set
     */
    public void setLocalFundingKeys(KeyUtils localFundingKeys) {
        this.localFundingKeys = localFundingKeys;
    }

    /**
     * @return the localRevocationPoint
     */
    public KeyUtils getLocalRevocationPoint() {
        return localRevocationPoint;
    }
    
    /**
     * @return the localRevocationBasePoint
     */
    public byte[] getLocalRevocationBasePoint() {
        return localRevocationPoint.getPub();
    }

    /**
     * @param localRevocationPoint the localRevocationPoint to set
     */
    public void setLocalRevocationPoint(KeyUtils localRevocationPoint) {
        this.localRevocationPoint = localRevocationPoint;
    }

    /**
     * @return the localPaymentPoint
     */
    public KeyUtils getLocalPaymentPoint() {
        return localPaymentPoint;
    }
    
    /**
     * @return the localPaymentBasePoint
     */
    public byte[] getLocalPaymentBasePoint() {
        return localPaymentPoint.getPub();
    }

    /**
     * @param localPaymentPoint the localPaymentPoint to set
     */
    public void setLocalPaymentPoint(KeyUtils localPaymentPoint) {
        this.localPaymentPoint = localPaymentPoint;
    }

    /**
     * @return the localDelayedPaymentPoint
     */
    public KeyUtils getLocalDelayedPaymentPoint() {
        return localDelayedPaymentPoint;
    }
    
    /**
     * @return the localDelayedPaymentBasePoint
     */
    public byte[] getLocalDelayedPaymentBasePoint() {
        return localDelayedPaymentPoint.getPub();
    }

    /**
     * @param localDelayedPaymentPoint the localDelayedPaymentPoint to set
     */
    public void setLocalDelayedPaymentPoint(KeyUtils localDelayedPaymentPoint) {
        this.localDelayedPaymentPoint = localDelayedPaymentPoint;
    }

    /**
     * @return the localHtlcPoint
     */
    public KeyUtils getLocalHtlcPoint() {
        return localHtlcPoint;
    }
    
    /**
     * @return the localHtlcBasePoint
     */
    public byte[] getLocalHtlcBasePoint() {
        return localHtlcPoint.getPub();
    }

    /**
     * @param localHtlcPoint the localHtlcPoint to set
     */
    public void setLocalHtlcPoint(KeyUtils localHtlcPoint) {
        this.localHtlcPoint = localHtlcPoint;
    }

    /**
     * @return the localFirstPerCommitmentPoint
     */
    public KeyUtils getLocalFirstPerCommitmentPoint() {
        return localFirstPerCommitmentPoint;
    }
    
    /**
     * @return the localFirstPerCommitmentPoint
     */
    public byte[] getLocalPerCommitmentPoint() {
        return localFirstPerCommitmentPoint.getPub();
    }

    /**
     * @param localFirstPerCommitmentPoint the localFirstPerCommitmentPoint to set
     */
    public void setLocalFirstPerCommitmentPoint(KeyUtils localFirstPerCommitmentPoint) {
        this.localFirstPerCommitmentPoint = localFirstPerCommitmentPoint;
    }

    /**
     * @return the localPubKey
     */
    public byte[] getLocalPubKey() {
        return localPubKey;
    }

    /**
     * @return the remotePubKey
     */
    public byte[] getRemotePubKey() {
        return remotePubKey;
    }

    /**
     * @return the localHtlcPubKey
     */
    public byte[] getLocalHtlcPubKey() {
        return localHtlcPubKey;
    }

    /**
     * @return the remoteHtlcPubKey
     */
    public byte[] getRemoteHtlcPubKey() {
        return remoteHtlcPubKey;
    }

    /**
     * @return the localDelayedPubKey
     */
    public byte[] getLocalDelayedPubKey() {
        return localDelayedPubKey;
    }

    /**
     * @return the remoteDelayedPubKey
     */
    public byte[] getRemoteDelayedPubKey() {
        return remoteDelayedPubKey;
    }

    /**
     * @return the localRevocationPubKey
     */
    public byte[] getRevocationPubKey() {
        return revocationPubKey;
    }

    /**
     * @return the remoteRevocationPubKey
     */
    public byte[] getRevocationPrvKey() {
        return revocationPrvKey;
    }

    /**
     * @return the theirDelayedPubKey
     */
    public byte[] getTheirDelayedPubKey() {
        return theirDelayedPubKey;
    }

    /**
     * @return the theirRemotePubKey
     */
    public byte[] getTheirRemotePubKey() {
        return theirRemotePubKey;
    }
}
