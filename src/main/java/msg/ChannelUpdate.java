/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import ec.Utils;
import static ec.Utils.byteArrayToHexString;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class ChannelUpdate {
    
    private byte[] signature;
    private byte[] chainHash;
    private byte[] shortChannelId;
    private byte[] timestamp;
    private byte[] flags;
    private byte[] cltvExpiryDelta;
    private byte[] htlcMinimumMsat;
    private byte[] feeBaseMsat;
    private byte[] feeProportionalMillionths;
    private int type = 258;
    
    public ChannelUpdate(byte[] message){
        int l = 0;
        signature = Arrays.copyOfRange(message, l, l+64);
        l = l + 64;
        System.out.println("ChannelUpdate signature length = " + signature.length + ", value = " + byteArrayToHexString(signature));
        chainHash = Arrays.copyOfRange(message, l, l+32);
        l = l + 32;
        System.out.println("ChannelUpdate chainHash length = " + chainHash.length + ", value = " + byteArrayToHexString(chainHash));
        shortChannelId = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("ChannelUpdate shortChannelId length = " + shortChannelId.length + ", value = " + byteArrayToHexString(shortChannelId));
        timestamp = Arrays.copyOfRange(message, l, l+4);
        l = l + 4;
        System.out.println("ChannelUpdate timestamp length = " + timestamp.length + ", value = " + byteArrayToHexString(timestamp));
        flags = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("ChannelUpdate flags length = " + flags.length + ", value = " + byteArrayToHexString(flags));
        cltvExpiryDelta = Arrays.copyOfRange(message, l, l+2);
        l = l + 2;
        System.out.println("ChannelUpdate cltvExpiryDelta length = " + cltvExpiryDelta.length + ", value = " + byteArrayToHexString(cltvExpiryDelta));
        htlcMinimumMsat = Arrays.copyOfRange(message, l, l+8);
        l = l + 8;
        System.out.println("ChannelUpdate htlcMinimumMsat length = " + htlcMinimumMsat.length + ", value = " + byteArrayToHexString(htlcMinimumMsat));
        feeBaseMsat = Arrays.copyOfRange(message, l, l+4);
        l = l + 4;
        System.out.println("ChannelUpdate feeBaseMsat length = " + feeBaseMsat.length + ", value = " + byteArrayToHexString(feeBaseMsat));
        feeProportionalMillionths = Arrays.copyOfRange(message, l, l+4);
        l = l + 4;
        System.out.println("ChannelUpdate feeProportionalMillionths length = " + feeProportionalMillionths.length + ", value = " + byteArrayToHexString(feeProportionalMillionths));
        System.out.println("ChannelUpdate l = "+l);
    }

    public byte[] getMessage(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(Utils.getHexOfShortWithBytes((short) type, 2));
            outputStream.write(signature);
            outputStream.write(chainHash);
            outputStream.write(shortChannelId);
            outputStream.write(timestamp);
            outputStream.write(flags);
            outputStream.write(cltvExpiryDelta);
            outputStream.write(htlcMinimumMsat);
            outputStream.write(feeBaseMsat);
            outputStream.write(feeProportionalMillionths);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the signature
     */
    public byte[] getSignature() {
        return signature;
    }

    /**
     * @param signature the signature to set
     */
    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    /**
     * @return the chainHash
     */
    public byte[] getChainHash() {
        return chainHash;
    }

    /**
     * @param chainHash the chainHash to set
     */
    public void setChainHash(byte[] chainHash) {
        this.chainHash = chainHash;
    }

    /**
     * @return the shortChannelId
     */
    public byte[] getShortChannelId() {
        return shortChannelId;
    }

    /**
     * @param shortChannelId the shortChannelId to set
     */
    public void setShortChannelId(byte[] shortChannelId) {
        this.shortChannelId = shortChannelId;
    }

    /**
     * @return the timestamp
     */
    public byte[] getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(byte[] timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the flags
     */
    public byte[] getFlags() {
        return flags;
    }

    /**
     * @param flags the flags to set
     */
    public void setFlags(byte[] flags) {
        this.flags = flags;
    }

    /**
     * @return the cltvExpiryDelta
     */
    public byte[] getCltvExpiryDelta() {
        return cltvExpiryDelta;
    }

    /**
     * @param cltvExpiryDelta the cltvExpiryDelta to set
     */
    public void setCltvExpiryDelta(byte[] cltvExpiryDelta) {
        this.cltvExpiryDelta = cltvExpiryDelta;
    }

    /**
     * @return the htlcMinimumMsat
     */
    public byte[] getHtlcMinimumMsat() {
        return htlcMinimumMsat;
    }

    /**
     * @param htlcMinimumMsat the htlcMinimumMsat to set
     */
    public void setHtlcMinimumMsat(byte[] htlcMinimumMsat) {
        this.htlcMinimumMsat = htlcMinimumMsat;
    }

    /**
     * @return the feeBaseMsat
     */
    public byte[] getFeeBaseMsat() {
        return feeBaseMsat;
    }

    /**
     * @param feeBaseMsat the feeBaseMsat to set
     */
    public void setFeeBaseMsat(byte[] feeBaseMsat) {
        this.feeBaseMsat = feeBaseMsat;
    }

    /**
     * @return the feeProportionalMillionths
     */
    public byte[] getFeeProportionalMillionths() {
        return feeProportionalMillionths;
    }

    /**
     * @param feeProportionalMillionths the feeProportionalMillionths to set
     */
    public void setFeeProportionalMillionths(byte[] feeProportionalMillionths) {
        this.feeProportionalMillionths = feeProportionalMillionths;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
}
