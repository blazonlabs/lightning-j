/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitcoin;

import ec.Hash;
import ec.Utils;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.DatatypeConverter;
import opcodes.ScriptOpCodes;
import org.apache.commons.lang3.ArrayUtils;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Sha256Hash;
import wf.bitcoin.javabitcoindrpcclient.BitcoinJSONRPCClientNew;
import wf.bitcoin.javabitcoindrpcclient.BitcoindRpcClient;

/**
 *
 * @author Abhimanyu
 */
public class BitcoinUtils {
    
    //private BitcoinJSONRPCClientNew bitcoin;
    public enum NetworkType{MAINNET,TESTNET, REGTEST;}
    
    public enum Weight{
        INITIAL_WEIGHT(724);
        private int numVal;
        Weight(int numVal) {
            this.numVal = numVal;
        }
        public int getNumVal() {
            return numVal;
        }
    }
    
    public BitcoinUtils(){
        
    }
    
    public String amountAfterFee(String amount, String fee){
        System.out.println("amountAfterFee amount : "+amount);
        System.out.println("amountAfterFee fee : "+fee);
        String base = "0000000000000000";
        Long amt = new BigInteger(amount, 16).longValue();
        Long feePerKW = new BigInteger(fee, 16).longValue();
        Long spendingAmt = amt - ((feePerKW * Weight.INITIAL_WEIGHT.getNumVal())/1000);
        System.out.println("amountAfterFee amt : "+amt);
        System.out.println("amountAfterFee feePerKW : "+feePerKW);
        System.out.println("amountAfterFee spendingAmt : "+spendingAmt);
        //Long _spendAmt = spendingAmt.longValue();
        String __ = Long.toHexString(spendingAmt);
        return String.format("%s%s", base.substring(0,16-__.length()) ,__ );

    }
    
    public byte[] btcToSatoshi(String bt){
        Double btc = Double.parseDouble(bt) * 100000000;
        long _satoshis = btc.longValue();
        return Utils.longToLittleEndian(_satoshis);
    }
    
    public String pkhFromAddress(String addr){
        try {
            byte[] __ = Utils.fromBase58(addr);
            return Utils.byteArrayToHexString(Arrays.copyOfRange(__, 1,__.length-4));
        } catch (Exception ex) {
            return "";
        }
    }
    
    public static String pkhFromBC32Address(String _) throws Exception{
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        return ec.Utils.byteArrayToHexString( su.decode("bcrt",_ ).getRight() );
    }
    
    public String hextoDoubleHash(String _){
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Hash.sha256(ec.Utils.hexStringToByteArray(_))));
    }
    
    public byte[] hextoDoubleHash(byte[] hex){
        String doubleHash = Utils.byteArrayToHexString(hex) + "01000000";
        System.out.println(String.format("hex for hash: %s", doubleHash));
        return Hash.sha256(Hash.sha256(Utils.hexStringToByteArray(doubleHash)));
    }
    
    public String signedHex(ECKey e, byte[] pubKey){
        byte[] msg = hextoDoubleHash(pubKey);
        System.out.println(String.format("double hash: %s",msg));
        
        Sha256Hash hash = Sha256Hash.wrap(msg);
        ECKey.ECDSASignature s = e.sign(hash);
        byte[] res = s.encodeToDER();
        String test = DatatypeConverter.printHexBinary(res);
        String sig = test.toLowerCase();
        
        System.out.println(String.format("--->> %s",sig));
        sig = sig + "01";
        String publicKey = Utils.byteArrayToHexString(e.getPubKey());
        return String.format("%s%s%s%s",Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(sig.length()/2))),sig,Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(publicKey.length()/2))),publicKey);
        //this.scriptSig = Utils.hexStringToByteArray(String.format("%s%s%s%s",Utils.byteArrayToHexString(bu.compactSizeUint(BigInteger.valueOf(sig.length()/2))),sig,Utils.byteArrayToHexString(bu.compactSizeUint(BigInteger.valueOf(publicKey.length()/2))),publicKey));
        //this.scriptSigSize = bu.compactSizeUint(BigInteger.valueOf(this.scriptSig.length));
        
        //return ec.Utils.byteArrayToHexString(this.hex());
    }
    
    public String signedHex( ECKey[] e, byte[] redScript, byte[] hex){
        
        //byte[] redeemScript = redScript;
        //setScriptSig(redScript);
        byte[] msg = hextoDoubleHash(hex);
        System.out.println(String.format("double hash: %s",msg));
        String[] sig = new String[2];
        for (int i = 0; i < 2; i++) {
            Sha256Hash hash = Sha256Hash.wrap(msg);
            ECKey.ECDSASignature s = e[i].sign(hash);
            byte[] res = s.encodeToDER();
            String test = DatatypeConverter.printHexBinary(res);
            sig[i] = test.toLowerCase();
            sig[i] = sig[i] + "01";
        }
        
        
        System.out.println(String.format("Sig1\n%s\nSig2\n%s",sig[0],sig[1]));
        return String.format("00%s%s%s%s%s%s",Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(sig[0].length()/2))),sig[0],Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(sig[1].length()/2))),sig[1],Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(redScript.length))),Utils.byteArrayToHexString(redScript));
        //this.scriptSig = Utils.hexStringToByteArray(String.format("00%s%s%s%s%s%s",Utils.byteArrayToHexString(bu.compactSizeUint(BigInteger.valueOf(sig[0].length()/2))),sig[0],Utils.byteArrayToHexString(bu.compactSizeUint(BigInteger.valueOf(sig[1].length()/2))),sig[1],Utils.byteArrayToHexString(bu.compactSizeUint(BigInteger.valueOf(redScript.length))),Utils.byteArrayToHexString(redScript)));
        //this.scriptSigSize = bu.compactSizeUint(BigInteger.valueOf(this.scriptSig.length));
        
        //return ec.Utils.byteArrayToHexString(this.hex());
    }
    
    public P2WPKH signedHex(ECKey e, String pubKey, String amt, P2WPKH p2wpkh){

        System.out.println(String.format("pubKey: %s",pubKey));
        String scriptPub = pubKey.substring(2);
        System.out.println(String.format("pKey: %s",scriptPub));
        String prevOuts = String.format("%s%s",Utils.byteArrayToHexString(p2wpkh.prevTxid), Utils.byteArrayToHexString(p2wpkh.prevVout));
        String outputs = String.format("%s%s%s",Utils.byteArrayToHexString(p2wpkh.satoshis), Utils.byteArrayToHexString(p2wpkh.scriptPubKeySize), Utils.byteArrayToHexString(p2wpkh.scriptPubKey));
        System.out.println(String.format("outputs: %s",outputs));
        String hashPrevouts = hextoDoubleHash(prevOuts);
        String hashSequence = hextoDoubleHash(Utils.byteArrayToHexString(p2wpkh.sequence));
        String scriptCode = String.format("1976a9%s88ac",scriptPub);
        String amount= Utils.byteArrayToHexString(btcToSatoshi(amt));
        String hashOutputs= hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",Utils.byteArrayToHexString(p2wpkh.version),hashPrevouts,hashSequence,prevOuts,scriptCode,amount,Utils.byteArrayToHexString(p2wpkh.sequence),hashOutputs,Utils.byteArrayToHexString(p2wpkh.locktime),nHashType);
        String msg = hextoDoubleHash(hashPreImage);
        
        Sha256Hash hash = Sha256Hash.wrap(msg);
        ECKey.ECDSASignature s = e.sign(hash);
        byte[] res = s.encodeToDER();
        String test = DatatypeConverter.printHexBinary(res);
        String sig = test.toLowerCase();
        
        System.out.println(String.format("--->> %s",sig));
        sig = sig + "01";
        String publicKey = ec.Utils.byteArrayToHexString(e.getPubKey());
        p2wpkh.numberOfWitness=Utils.hexStringToByteArray("02");
        p2wpkh.witness = Utils.hexStringToByteArray(String.format("%s%s%s%s", Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(sig.length()/2))), sig, Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(publicKey.length()/2))), publicKey));

        return p2wpkh;
        //return ec.Utils.byteArrayToHexString(this.hex2());
    }
    
    public P2WPKH signedHex(ECKey[] e, String redeemScript, String amt, P2WPKH p2wpkh){

        System.out.println(String.format("redeem script: %s",redeemScript));
        String redeemScriptHex = scriptToHex(redeemScript);
        System.out.println(String.format("redeem script hex: %s",redeemScriptHex));
        String prevOuts = String.format("%s%s",Utils.byteArrayToHexString(p2wpkh.prevTxid), Utils.byteArrayToHexString(p2wpkh.prevVout));
        String outputs = String.format("%s%s%s",Utils.byteArrayToHexString(p2wpkh.satoshis), Utils.byteArrayToHexString(p2wpkh.scriptPubKeySize), Utils.byteArrayToHexString(p2wpkh.scriptPubKey));
        
        String hashPrevouts = hextoDoubleHash(prevOuts);
        String hashSequence = hextoDoubleHash(Utils.byteArrayToHexString(p2wpkh.sequence));
        String scriptCode = String.format("%s%s",Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2))),redeemScriptHex);
        String amount= Utils.byteArrayToHexString(btcToSatoshi(amt));
        String hashOutputs= hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",Utils.byteArrayToHexString(p2wpkh.version),hashPrevouts,hashSequence,prevOuts,scriptCode,amount,Utils.byteArrayToHexString(p2wpkh.sequence),hashOutputs,Utils.byteArrayToHexString(p2wpkh.locktime),nHashType);
        String msg = hextoDoubleHash(hashPreImage);
        String[] sig = new String[2];
        for (int i = 0; i < 2; i++) {
            
            Sha256Hash hash = Sha256Hash.wrap(msg);
            ECKey.ECDSASignature s = e[i].sign(hash);
            byte[] res = s.encodeToDER();
            String test = DatatypeConverter.printHexBinary(res);
            sig[i] = test.toLowerCase();
        
            System.out.println(String.format("--->> %s",sig[i]));
            sig[i] = sig[i] + "01";
        }
        
        p2wpkh.numberOfWitness = Utils.hexStringToByteArray("04");
        p2wpkh.witness = Utils.hexStringToByteArray(String.format("00%s%s%s%s%s%s",Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(sig[0].length()/2))), sig[0], Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(sig[1].length()/2))), sig[1], Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2))), redeemScriptHex));

        return p2wpkh;
        //return ec.Utils.byteArrayToHexString(this.hex2());
    }
    
    public P2WSH signedHex(ECKey e, String pubKey, String amt, P2WSH p2wsh){

        System.out.println(String.format("pubKey: %s",pubKey));
        String scriptPub = pubKey.substring(2);
        System.out.println(String.format("pKey: %s",scriptPub));
        String prevOuts = String.format("%s%s",Utils.byteArrayToHexString(p2wsh.prevTxid), Utils.byteArrayToHexString(p2wsh.prevVout));
        String outputs = String.format("%s%s%s",Utils.byteArrayToHexString(p2wsh.satoshis), Utils.byteArrayToHexString(p2wsh.scriptPubKeySize), Utils.byteArrayToHexString(p2wsh.scriptPubKey));
        System.out.println(String.format("outputs: %s",outputs));
        String hashPrevouts = hextoDoubleHash(prevOuts);
        String hashSequence = hextoDoubleHash(Utils.byteArrayToHexString(p2wsh.sequence));
        String scriptCode = String.format("1976a9%s88ac",scriptPub);
        String amount= Utils.byteArrayToHexString(btcToSatoshi(amt));
        String hashOutputs= hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",Utils.byteArrayToHexString(p2wsh.version),hashPrevouts,hashSequence,prevOuts,scriptCode,amount,Utils.byteArrayToHexString(p2wsh.sequence),hashOutputs,Utils.byteArrayToHexString(p2wsh.locktime),nHashType);
        String msg = hextoDoubleHash(hashPreImage);
        
        Sha256Hash hash = Sha256Hash.wrap(msg);
        ECKey.ECDSASignature s = e.sign(hash);
        byte[] res = s.encodeToDER();
        String test = DatatypeConverter.printHexBinary(res);
        String sig = test.toLowerCase();
        
        System.out.println(String.format("--->> %s",sig));
        sig = sig + "01";
        String publicKey = ec.Utils.byteArrayToHexString(e.getPubKey());
        p2wsh.numberOfWitness=Utils.hexStringToByteArray("02");
        p2wsh.witness = Utils.hexStringToByteArray(String.format("%s%s%s%s", Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(sig.length()/2))), sig, Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(publicKey.length()/2))), publicKey));

        return p2wsh;
        //return ec.Utils.byteArrayToHexString(this.hex2());
    }
    
    public byte[] compactSizeUint(BigInteger x){
        
        String ret="";
        String base="00";
        if(x.compareTo(new BigInteger("0"))>=0 && x.compareTo(new BigInteger("252")) <= 0){
            String temp = String.format("%x",x.and(new BigInteger("ff",16)));
            ret = String.format("%s%s", base.substring(0,2-temp.length()) ,temp );
        }
        else if(x.compareTo(new BigInteger("253"))>=0 && x.compareTo(new BigInteger("65535")) <= 0)
            ret = String.format("%x",x.and(new BigInteger("ffff",16)));
        else if(x.compareTo(new BigInteger("65536"))>=0 && x.compareTo(new BigInteger("4294967295")) <= 0)
            ret = String.format("%x",x.and(new BigInteger("ffffffff",16)));        
        else if(x.compareTo(new BigInteger("4294967296"))>=0 && x.compareTo(new BigInteger("18446744073709551615")) <= 0)
            ret = String.format("%x",x.and(new BigInteger("ffffffffffffffff",16)));
        return Utils.hexStringToByteArray(ret);
    }
    
    public static byte[] reverse(byte[] _){
        byte[] clone = ArrayUtils.clone(_);
        ArrayUtils.reverse(clone);
        return clone;
    }
    
    public String scriptToHex( String _){
        String opcode[]= _.split(" ");
        String scriptHex = "";
        int x = 0xff;
        for(int i=0;i<opcode.length;i++){
            x = ScriptOpCodes.getOpCode(opcode[i]);
            if(x!=0xff)
                scriptHex = scriptHex + Integer.toHexString(x);
            else if(x==0xff)
                scriptHex = scriptHex + Utils.byteArrayToHexString(compactSizeUint(BigInteger.valueOf(opcode[i].length()/2))) + opcode[i];
        }
        System.out.println(scriptHex);
        return scriptHex;
    }
    
    public String scriptToHash(String _){
        String scriptHex = scriptToHex(_);
        System.out.println("scriptToHash scriptHex : "+scriptHex);
        return Utils.byteArrayToHexString(Hash.ripemd160(Hash.sha256(Utils.hexStringToByteArray(scriptHex))));
    }

    public String scriptToWHash(String _){
        String scriptHex = scriptToHex(_);
        System.out.println("scriptToHash scriptHex : "+scriptHex);
        return Utils.byteArrayToHexString(Hash.sha256(Utils.hexStringToByteArray(scriptHex)));
    }
    
    public String scriptToAddress(NetworkType netType, String script){
        String scriptHash = scriptToHash(script);
        if(netType == NetworkType.MAINNET)
            scriptHash = "05"+scriptHash;
        else
            scriptHash = "c4"+scriptHash;    
        String _tmp = Utils.byteArrayToHexString(Hash.sha256(Hash.sha256(Utils.hexStringToByteArray(scriptHash))));
        return Utils.toBase58( Utils.hexStringToByteArray(scriptHash+_tmp.substring(0, 8)));
        
    }
    
}
