/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitcoin;

import bolt3.Bolt3Util;
import ec.Utils;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;

/**
 *
 * @author Abhimanyu
 */
public class P2SH {
    static BitcoinUtils bu;
    BitcoinUtils.NetworkType netType;
    byte[] version = Utils.intToLittleEndianWithByte(2, 4);
    byte[] inputCount = Utils.hexStringToByteArray("01");
    byte[] prevTxid;
    byte[] prevVout;
    byte[] scriptSigSize = new byte[1];
    byte[] scriptSig = new byte[0];
    byte[] sequence = Utils.hexStringToByteArray("ffffffff");
    byte[] outputCount = Utils.hexStringToByteArray("01");
    String amount;
    byte[] satoshis;
    byte[] scriptPubKeySize;
    byte[] scriptPubKey;
    byte[] locktime = new byte[4];
    
    public P2SH(){
        bu = new BitcoinUtils();
    }
    
    public  P2SH(BitcoinUtils.NetworkType netType, byte[] prevTxid, int prevVout, String redeemScript, String amount){
        bu = new BitcoinUtils();
        this.netType = netType;
        this.prevTxid = bu.reverse(prevTxid);
        this.prevVout = Utils.intToLittleEndianWithByte(prevVout, 4);
        this.satoshis = bu.btcToSatoshi(amount);
        this.scriptPubKey = Utils.hexStringToByteArray(String.format("a914%s87", bu.scriptToHash(redeemScript)));
        this.scriptPubKeySize = bu.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length));
    }
    
    public void setScriptSig(byte[] _){
        this.scriptSig = _;
        this.scriptSigSize = bu.compactSizeUint(BigInteger.valueOf(this.scriptSig.length));
    }
    
    public BitcoinUtils getBitcoinUtilsObj(){
        return bu;
    } 
    
    public byte[] hex(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(version);
            outputStream.write(inputCount);
            outputStream.write(prevTxid);
            outputStream.write(prevVout);
            outputStream.write(scriptSigSize);
            outputStream.write(scriptSig);
            outputStream.write(sequence);
            outputStream.write(outputCount);
            outputStream.write(satoshis);
            outputStream.write(scriptPubKeySize);
            outputStream.write(scriptPubKey);
            outputStream.write(locktime);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public static void main(String args[]){
        BitcoinClient bc = new BitcoinClient("http://blazon:blazon123@192.168.1.101:7332/");
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairU = new ECKey();
        ECKey pairV = new ECKey();
        String addrU = pairU.toAddress(reg).toBase58();
        String addrV = pairV.toAddress(reg).toBase58();
        System.out.println("\n\nTestcase 5 results:\n");
        System.out.println(String.format("U addr \n%s\nV addr \n%s",addrU, addrV));
        String fundingTX = bc.getBitcoin().sendToAddress(addrU, 50 );
        bc.getBitcoin().generate(50);
        bc.getBitcoin().importAddress(addrU, null, true);
        bc.getBitcoin().importAddress(addrV, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to U \n%s",fundingTX));
        bc.printUTXO(addrU);
        bc.printUTXO(addrV);
        System.out.println(String.format("vout for %s %s",addrU,fundingTX));
        System.out.println(bc.voutFromAddressTxid(addrU, fundingTX));
        String redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",ec.Utils.byteArrayToHexString(pairU.getPubKey()),ec.Utils.byteArrayToHexString(pairV.getPubKey()));
        P2SH txnUtoMs = new P2SH(BitcoinUtils.NetworkType.REGTEST, Utils.hexStringToByteArray(fundingTX), bc.voutFromAddressTxid(addrU, fundingTX) , redeemScript , "49.99" );
        System.out.println(String.format("transactionHex is \n%s",ec.Utils.byteArrayToHexString(txnUtoMs.hex())));
        
        txnUtoMs.setScriptSig(bc.scriptPubKeyFromTxid(addrU, fundingTX));
        String signedHex1 = bu.signedHex(pairU,txnUtoMs.hex());
        txnUtoMs.setScriptSig(Utils.hexStringToByteArray(signedHex1));
        signedHex1 = Utils.byteArrayToHexString(txnUtoMs.hex());
        System.out.println(String.format("Signed hex1: %s",signedHex1));

        String txidUtoMs = bc.getBitcoin().sendRawTransaction(signedHex1);
        String multiSigAddr = bu.scriptToAddress( txnUtoMs.netType,redeemScript );
        bc.getBitcoin().importAddress(multiSigAddr, null, true);
        System.out.println("redeem address is \n"+multiSigAddr);
        bc.getBitcoin().generate(5);
        System.out.println(String.format("TXId from U to Multi sig is \n%s",txidUtoMs));
        bc.printUTXO(addrU);
        bc.printUTXO(addrV);
        bc.printUTXO(multiSigAddr);
        System.out.println(String.format("vout for %s %s",multiSigAddr,txidUtoMs));
        System.out.println(bc.voutFromAddressTxid(multiSigAddr, txidUtoMs));
        P2PKH txnMstoV = new P2PKH(Bolt3Util.NetworkType.REGTEST, Utils.hexStringToByteArray(txidUtoMs), bc.voutFromAddressTxid(multiSigAddr, txidUtoMs) , addrV, "49.98");
        
        ECKey[] privateKeys = new ECKey[2];
        privateKeys[0] = pairU;
        privateKeys[1] = pairV;
        
        txnMstoV.setScriptSig(Utils.hexStringToByteArray(bu.scriptToHex(redeemScript)));
        String signedHex2 = bu.signedHex(privateKeys, Utils.hexStringToByteArray(bu.scriptToHex(redeemScript)), txnMstoV.hex());
        txnMstoV.setScriptSig(Utils.hexStringToByteArray(signedHex2));
        signedHex2 = Utils.byteArrayToHexString(txnMstoV.hex());
        System.out.println(String.format("Signed hex2: %s",signedHex2));
        
        String txidMsToV = bc.getBitcoin().sendRawTransaction(signedHex2);
        bc.getBitcoin().generate(50);
        System.out.println(String.format("TXId from Multi sig to V is \n%s",txidMsToV));
        bc.printUTXO(addrU);
        bc.printUTXO(addrV);
        bc.printUTXO(multiSigAddr);
            
    }
}
