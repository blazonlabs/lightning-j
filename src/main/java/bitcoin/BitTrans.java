/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitcoin;

import ec.ECDH_BC;
import ec.Utils;
import java.util.Arrays;

/**
 *
 * @author Abhimanyu
 */
public class BitTrans {
    private BitcoinClient bc;
    private BitcoinUtils bu;
    
    public BitTrans(String address, BitcoinUtils bu){
        bc = new BitcoinClient(address);
        this.bu = bu;
    }
    
    public boolean sigVerify(byte[] fundingTXId, byte[] fundingVout, byte[] fundingAmount, byte[] selfDelay, byte[] revocationPubKey, byte[] localDelayedPubKey , byte[] remotePubKey, byte[] pubKey1, byte[] pubKey2, byte[] sig, byte[] feePerKW, byte[] seq, byte[] locktime){
        try{
            SegwitAddressUtil su = SegwitAddressUtil.getInstance();
            byte witnessVersion = 0;

            String spendingAmount = getBu().amountAfterFee(Utils.byteArrayToHexString(fundingAmount), Utils.byteArrayToHexString(feePerKW));
            String redeemScript = "";
            if(ECDH_BC.compareKeys(pubKey1, pubKey2) > 0){
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey2), Utils.byteArrayToHexString(pubKey1));
            }else{
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey1), Utils.byteArrayToHexString(pubKey2));
            }
            System.out.println("sigVerify Redeem Script : "+redeemScript);
            
            String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(getBu().scriptToHash(redeemScript)));
            getBc().getBitcoin().importAddress(multiSigAddr, null, true);
            System.out.println("sigVerify Redeem Address : "+multiSigAddr);


            String escrowScript = String.format("OP_IF %s OP_ELSE %s OP_CHECKSEQUENCEVERIFY OP_DROP %s OP_ENDIF OP_CHECKSIG", ec.Utils.byteArrayToHexString(revocationPubKey), ec.Utils.byteArrayToHexString(selfDelay), ec.Utils.byteArrayToHexString(localDelayedPubKey));
            System.out.println("sigVerify Escrow Script : "+escrowScript);
            String escrowAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(getBu().scriptToHash(escrowScript)));
            getBc().getBitcoin().importAddress(escrowAddr, null, true);
            System.out.println("sigVerify Escrow Address : "+escrowAddr);


            P2WSH2 txn = new P2WSH2(BitcoinUtils.NetworkType.REGTEST, fundingTXId, fundingVout , remotePubKey, Utils.hexStringToByteArray(spendingAmount), seq, locktime);        

            boolean res = txn.verifySignature(redeemScript, fundingAmount, sig, pubKey1);
            System.out.println("sigVerify Signature Verification : "+res);

            return res;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    public byte[] sigGenerate(byte[] fundingTXId, byte[] fundingVout, byte[] fundingAmount, byte[] selfDelay, byte[] revocationPubKey, byte[] localDelayedPubKey , byte[] remotePubKey, byte[] pubKey1, byte[] pubKey2, byte[] sig, byte[] feePerKW, byte[] seq, byte[] locktime, byte[] localPvtKey){
        try{
            System.out.println("sigGenerate Revocation Key : "+Utils.byteArrayToHexString(revocationPubKey));
            System.out.println("sigGenerate Delayed Key : "+Utils.byteArrayToHexString(localDelayedPubKey));

            SegwitAddressUtil su = SegwitAddressUtil.getInstance();
            byte witnessVersion=0;

            String spendingAmount = getBu().amountAfterFee(Utils.byteArrayToHexString(fundingAmount), Utils.byteArrayToHexString(feePerKW));
            String redeemScript = "";
            if(ECDH_BC.compareKeys(pubKey1, pubKey2) > 0){
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey2), Utils.byteArrayToHexString(pubKey1));
            }else{
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey1), Utils.byteArrayToHexString(pubKey2));
            }
            System.out.println("sigGenerate Redeem Script : "+redeemScript);
            String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(getBu().scriptToHash(redeemScript)));
            getBc().getBitcoin().importAddress(multiSigAddr, null, true);
            System.out.println("sigGenerate Redeem Address : "+multiSigAddr);


            String escrowScript = String.format("OP_IF %s OP_ELSE %s OP_CHECKSEQUENCEVERIFY OP_DROP %s OP_ENDIF OP_CHECKSIG",Utils.byteArrayToHexString(revocationPubKey), Utils.byteArrayToHexString(getBu().reverse(selfDelay)), Utils.byteArrayToHexString(localDelayedPubKey));
            System.out.println("sigGenerate Escrow Script : "+escrowScript);
            String escrowAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(getBu().scriptToHash(escrowScript)));
            getBc().getBitcoin().importAddress(escrowAddr, null, true);
            System.out.println("sigGenerate Escrow Address : "+escrowAddr);


            P2WSH2 txn = new P2WSH2(BitcoinUtils.NetworkType.REGTEST, fundingTXId, fundingVout , remotePubKey, Utils.hexStringToByteArray(spendingAmount), seq, locktime, escrowScript);        
            
            String hash = txn.signingHashWithScript(redeemScript, fundingAmount);
            byte[] localSignature = ECDH_BC.sign(Utils.hexStringToByteArray(hash), localPvtKey);
            System.out.println("sigGenerate Signature Generated : "+Utils.byteArrayToHexString(localSignature));
            return localSignature;
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public boolean sigVerify(byte[] fundingTXId, byte[] fundingVout, byte[] fundingAmount, byte[] selfDelay, byte[] revocationPubKey, byte[] localDelayedPubKey , byte[] pubKey1, byte[] pubKey2, byte[] sig, byte[] feePerKW, byte[] seq, byte[] locktime){
        try{
            System.out.println("sigGenerate Revocation Key : "+Utils.byteArrayToHexString(revocationPubKey));
            System.out.println("sigGenerate Delayed Key : "+Utils.byteArrayToHexString(localDelayedPubKey));

            SegwitAddressUtil su = SegwitAddressUtil.getInstance();
            byte witnessVersion=0;

            String spendingAmount = getBu().amountAfterFee(Utils.byteArrayToHexString(fundingAmount), Utils.byteArrayToHexString(feePerKW));
            String redeemScript = "";
            if(ECDH_BC.compareKeys(pubKey1, pubKey2) > 0){
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey2), Utils.byteArrayToHexString(pubKey1));
            }else{
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey1), Utils.byteArrayToHexString(pubKey2));
            }
            System.out.println("sigGenerate Redeem Script : "+redeemScript);
            String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(getBu().scriptToHash(redeemScript)));
            getBc().getBitcoin().importAddress(multiSigAddr, null, true);
            System.out.println("sigGenerate Redeem Address : "+multiSigAddr);


            String escrowScript = String.format("OP_IF %s OP_ELSE %s OP_CHECKSEQUENCEVERIFY OP_DROP %s OP_ENDIF OP_CHECKSIG",Utils.byteArrayToHexString(revocationPubKey), Utils.byteArrayToHexString(getBu().reverse(selfDelay)), Utils.byteArrayToHexString(localDelayedPubKey));
            System.out.println("sigGenerate Escrow Script : "+escrowScript);
            String escrowAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(getBu().scriptToHash(escrowScript)));
            getBc().getBitcoin().importAddress(escrowAddr, null, true);
            System.out.println("sigGenerate Escrow Address : "+escrowAddr);


            P2WSH2 txn = new P2WSH2(BitcoinUtils.NetworkType.REGTEST, fundingTXId, fundingVout , new byte[0], Utils.hexStringToByteArray(spendingAmount), seq, locktime, escrowScript);        
            
            boolean res = txn.verifySignature(redeemScript, fundingAmount, sig, pubKey2);
            System.out.println("sigVerify Signature Verification : "+res);

            return res;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    public byte[] sigGenerate(byte[] fundingTXId, byte[] fundingVout, byte[] fundingAmount, byte[] localPubKey, byte[] pubKey1, byte[] pubKey2, byte[] feePerKW, byte[] seq, byte[] locktime, byte[] localPvtKey){
        try{
            SegwitAddressUtil su = SegwitAddressUtil.getInstance();
            byte witnessVersion = 0;

            String spendingAmount = getBu().amountAfterFee(Utils.byteArrayToHexString(fundingAmount), Utils.byteArrayToHexString(feePerKW));
            String redeemScript = "";
            if(ECDH_BC.compareKeys(pubKey1, pubKey2) > 0){
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey2), Utils.byteArrayToHexString(pubKey1));
            }else{
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey1), Utils.byteArrayToHexString(pubKey2));
            }
            System.out.println("sigVerify Redeem Script : "+redeemScript);
            
            String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(getBu().scriptToHash(redeemScript)));
            getBc().getBitcoin().importAddress(multiSigAddr, null, true);
            System.out.println("sigVerify Redeem Address : "+multiSigAddr);


            P2WSH2 txn = new P2WSH2(BitcoinUtils.NetworkType.REGTEST, fundingTXId, fundingVout , localPubKey, Utils.hexStringToByteArray(spendingAmount), seq, locktime);        

            String hash = txn.signingHashWithScript(redeemScript, fundingAmount);
            
            byte[] localSignature = ECDH_BC.sign(Utils.hexStringToByteArray(hash), localPvtKey);
            System.out.println("sigGenerate Signature Generated : "+Utils.byteArrayToHexString(localSignature));
            return localSignature;
            
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public byte[] txidGenerate(byte[] fundingTXId, byte[] fundingAmount, byte[] pubKey1, byte[] pubKey2, byte[] seq, byte[] locktime, byte[] fundingPvtKey){
        try{
            SegwitAddressUtil su = SegwitAddressUtil.getInstance();
            byte witnessVersion=0;

            String redeemScript = "";
            if(ECDH_BC.compareKeys(pubKey1, pubKey2) > 0){
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey2), Utils.byteArrayToHexString(pubKey1));
            }else{
                redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",Utils.byteArrayToHexString(pubKey1), Utils.byteArrayToHexString(pubKey2));
            }
            System.out.println("txidGenerate Redeem Script : "+redeemScript);
            String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(getBu().scriptToHash(redeemScript)));
            getBc().getBitcoin().importAddress(multiSigAddr, null, true);
            System.out.println("txidGenerate Redeem Address : "+multiSigAddr);

            P2WSH2 txn = new P2WSH2(BitcoinUtils.NetworkType.REGTEST, fundingTXId, new byte[1] , new byte[0], fundingAmount, seq, locktime, redeemScript);        
            
            String hash = txn.signingHashWithKey(pubKey1, Utils.hexStringToByteArray("000000000010c8e0"));
            byte[] localSignature = ECDH_BC.sign(Utils.hexStringToByteArray(hash), fundingPvtKey);
            System.out.println("txidGenerate Signature Generated : "+Utils.byteArrayToHexString(localSignature));
            txn.updateWitness(localSignature, pubKey1);
            
            byte[] ftxid = bu.reverse(Utils.hexStringToByteArray(bu.hextoDoubleHash(Utils.byteArrayToHexString(txn.hex2()))));
            return ftxid;
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * @return the bc
     */
    public BitcoinClient getBc() {
        return bc;
    }

    /**
     * @return the bu
     */
    public BitcoinUtils getBu() {
        return bu;
    }
}
