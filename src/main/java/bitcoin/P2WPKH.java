/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitcoin;

import ec.Utils;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;

/**
 *
 * @author Abhimanyu
 */
public class P2WPKH {
    static BitcoinUtils bu;
    byte[] version = Utils.intToLittleEndianWithByte(2, 4);
    byte[] marker = new byte[1];
    byte[] flag = Utils.hexStringToByteArray("01");
    byte[] inputCount = Utils.hexStringToByteArray("01");
    byte[] prevTxid;
    byte[] prevVout;
    byte[] scriptSigSize = new byte[1];
    byte[] sequence = Utils.hexStringToByteArray("ffffffff");
    byte[] outputCount = Utils.hexStringToByteArray("01");
    String amount;
    byte[] satoshis;
    byte[] scriptPubKeySize;
    byte[] scriptPubKey;
    byte[] numberOfWitness;
    byte[] witness;
    byte[] locktime = new byte[4];
    
    public P2WPKH(){
        bu = new BitcoinUtils();
    }
    
    public  P2WPKH(BitcoinUtils.NetworkType netType, byte[] prevTxid, int prevVout, String rxAddress, String amount) throws Exception{
        bu = new BitcoinUtils();
        this.prevTxid = bu.reverse(prevTxid);
        this.prevVout = Utils.intToLittleEndianWithByte(prevVout, 4);
        this.amount = amount;
        this.satoshis = bu.btcToSatoshi(amount);
        this.scriptPubKey = Utils.hexStringToByteArray(String.format("00%s%s",Utils.byteArrayToHexString(bu.compactSizeUint(BigInteger.valueOf(bu.pkhFromBC32Address(rxAddress).length()/2))), bu.pkhFromBC32Address(rxAddress)));
        this.scriptPubKeySize = bu.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length));
        
    }
    
    public byte[] hex(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(version);
            outputStream.write(inputCount);
            outputStream.write(prevTxid);
            outputStream.write(prevVout);
            outputStream.write(scriptSigSize);
            outputStream.write(sequence);
            outputStream.write(outputCount);
            outputStream.write(satoshis);
            outputStream.write(scriptPubKeySize);
            outputStream.write(scriptPubKey);
            outputStream.write(locktime);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public byte[] hex2(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(version);
            outputStream.write(marker);
            outputStream.write(flag);
            outputStream.write(inputCount);
            outputStream.write(prevTxid);
            outputStream.write(prevVout);
            outputStream.write(scriptSigSize);
            outputStream.write(sequence);
            outputStream.write(outputCount);
            outputStream.write(satoshis);
            outputStream.write(scriptPubKeySize);
            outputStream.write(scriptPubKey);
            outputStream.write(numberOfWitness);
            outputStream.write(witness);
            outputStream.write(locktime);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public static void main(String args[]) throws Exception{
        BitcoinClient bc = new BitcoinClient("http://blazon:blazon123@192.168.1.101:7332/");
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairA = new ECKey();
        ECKey pairB = new ECKey();
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        byte witnessVersion=0;
        String addrA = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairA.getPubKeyHash());
        String addrB = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairB.getPubKeyHash());
        System.out.println("\n\nTestcase 4 results:\n");
        System.out.println(String.format("A add \n%s\nB addr \n%s",addrA, addrB));
        String txid0 = bc.getBitcoin().sendToAddress(addrA, 1 );
        bc.getBitcoin().generate(5);
        bc.getBitcoin().importAddress(addrA, null, true);
        bc.getBitcoin().importAddress(addrB, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to A \n%s",txid0));
        bc.printUTXO(addrA);
        bc.printUTXO(addrB);
        System.out.println(String.format("vout for %s %s",addrA,txid0));
        System.out.println(bc.voutFromAddressTxid(addrA, txid0));
        P2WPKH txn1 = new P2WPKH(BitcoinUtils.NetworkType.REGTEST , Utils.hexStringToByteArray(txid0), bc.voutFromAddressTxid(addrA, txid0) , addrB , "0.99" );
        System.out.println(String.format("transactionHex is \n%s",ec.Utils.byteArrayToHexString(txn1.hex())));
        
        txn1 = bu.signedHex(pairA, Utils.byteArrayToHexString(bc.scriptPubKeyFromTxid(addrA, txid0)), bc.amountFromTxid(addrA, txid0), txn1);
        String signedHex1 = Utils.byteArrayToHexString(txn1.hex2());
        //String signedHex1 = txn1.signedHex(pairA, bc.scriptPubKeyFromTxid(addrA, txid0), bc.amountFromTxid(addrA, txid0));
        System.out.println(String.format("Signed hex1: %s",signedHex1));
        
        String txid1 = bc.getBitcoin().sendRawTransaction(signedHex1);
        System.out.println(String.format("Funding TXId from A to B \n%s",txid1));
        bc.getBitcoin().generate(5);
        bc.printUTXO(addrA);
        bc.printUTXO(addrB);
        
    }
}
