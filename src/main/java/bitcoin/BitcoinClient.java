/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitcoin;

import ec.Utils;
import java.net.MalformedURLException;
import java.util.List;
import wf.bitcoin.javabitcoindrpcclient.BitcoinJSONRPCClientNew;
import wf.bitcoin.javabitcoindrpcclient.BitcoindRpcClient;

/**
 *
 * @author Abhimanyu
 */
public class BitcoinClient {
    
    private BitcoinJSONRPCClientNew bitcoin;
    private String address;
    
    public BitcoinClient(String addr){
        this.address = addr;
        try {
            this.bitcoin = new BitcoinJSONRPCClientNew(addr);
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
    }

    public void printUTXO(String addr){
        List<BitcoindRpcClient.Unspent> lU =  getBitcoin().listUnspent(1, 9999999, addr);
        lU.forEach((BitcoindRpcClient.Unspent u) -> {System.out.println(String.format("%s %s %s %s", u.address(), u.amount(),u.txid(), u.vout()));});
    }
    
    public int voutFromAddressTxid(String addr , String txid){
        final int[]  vout = new int[1];
        List<BitcoindRpcClient.Unspent> lU =  getBitcoin().listUnspent(1, 9999999, addr);
        lU.forEach((BitcoindRpcClient.Unspent u) -> {
            if(u.txid().equalsIgnoreCase(txid))
                vout[0] = u.vout();
        });
        return vout[0];
    }
    
    public byte[] scriptPubKeyFromTxid(String addr , String txid)
    {
        System.out.println(String.format("conaddr: %s",addr));
        final String[]  pubKey = new String[1];
        pubKey[0]="";
        List<BitcoindRpcClient.Unspent> lU =  getBitcoin().listUnspent(1, 9999999, addr);
        lU.forEach((BitcoindRpcClient.Unspent u) -> {
            if(u.txid().equalsIgnoreCase(txid))
                pubKey[0] = String.format("%s",u.scriptPubKey());
        });
        
        return Utils.hexStringToByteArray(pubKey[0]);

    }
    
    public String amountFromTxid(String addr , String txid)
    {
        System.out.println(String.format("conaddr: %s",addr));
        final String[]  amt = new String[1];
        amt[0]="";
        List<BitcoindRpcClient.Unspent> lU =  bitcoin.listUnspent(1, 9999999, addr);
        lU.forEach((BitcoindRpcClient.Unspent u) -> {
            if(u.txid().equalsIgnoreCase(txid))
                amt[0] = String.format("%s",u.amount());
        });
        System.out.println(String.format("amt[0]: %s",amt[0]));
        return amt[0];

    }
    
    /**
     * @return the bitcoin
     */
    public BitcoinJSONRPCClientNew getBitcoin() {
        return bitcoin;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
        try {
            this.bitcoin = new BitcoinJSONRPCClientNew(address);
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void main(String[] args){
        BitcoinClient bc = new BitcoinClient("http://blazon:blazon123@192.168.1.101:7332/");
        BitcoinUtils bu = new BitcoinUtils();
        System.out.println(Utils.byteArrayToHexString(bu.reverse(Utils.hexStringToByteArray(bc.getBitcoin().getBlock(0).hash()))));
    }
}
