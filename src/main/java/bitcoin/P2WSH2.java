/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitcoin;

import ec.ECDH_BC;
import ec.Utils;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;

/**
 *
 * @author Abhimanyu
 */
public class P2WSH2 {
    static BitcoinUtils bu;
    BitcoinUtils.NetworkType netType;
    byte[] version = Utils.intToLittleEndianWithByte(2, 4);
    byte[] marker = new byte[1];
    byte[] flag = Utils.hexStringToByteArray("01");
    byte[] inputCount = Utils.hexStringToByteArray("01");
    byte[] prevTxid;
    byte[] prevVout;
    byte[] scriptSigSize = new byte[1];
    byte[] sequence = Utils.hexStringToByteArray("ffffffff");
    byte[] outputCount = Utils.hexStringToByteArray("01");
    String amount;
    byte[] satoshis;
    byte[] scriptPubKeySize;
    byte[] scriptPubKey;
    byte[] numberOfWitness;
    byte[] witness;
    byte[] locktime = new byte[4];
    
    public P2WSH2(){
        bu = new BitcoinUtils();
    }
    
    public  P2WSH2(BitcoinUtils.NetworkType netType, byte[] prevTxid, byte[] prevVout, byte[] pubKey , byte[] amount1, byte[] seq, byte[] locktime){
        bu = new BitcoinUtils();
        this.netType = netType;
        this.prevTxid = prevTxid;
        this.prevVout = Utils.byteArrayToLittleEndianWithByte(prevVout, 4);
        this.satoshis = bu.reverse(amount1);
        this.sequence = bu.reverse(Utils.hexStringToByteArray(String.format("80%s", ec.Utils.byteArrayToHexString(seq))));//bu.reverse("80"+seq);
        this.locktime = bu.reverse(Utils.hexStringToByteArray(String.format("20%s", ec.Utils.byteArrayToHexString(locktime))));//bu.reverse("20"+locktime);
        this.scriptPubKey = Utils.hexStringToByteArray(String.format("0014%s", ec.Utils.byteArrayToHexString(ec.Hash.ripemd160(ec.Hash.sha256(pubKey)))));
        this.scriptPubKeySize = bu.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length));
        System.out.println("P2WSH2 Raw Transaction : "+Utils.byteArrayToHexString(hex()));
    }
    
    public  P2WSH2(BitcoinUtils.NetworkType netType, byte[] prevTxid, byte[] prevVout, byte[] pubKey , byte[] amount1, byte[] seq, byte[] locktime, String escrowScript){
        bu = new BitcoinUtils();
        this.netType = netType;
        this.prevTxid = prevTxid;
        this.prevVout = Utils.byteArrayToLittleEndianWithByte(prevVout, 4);
        this.satoshis = bu.reverse(amount1);
        this.sequence = bu.reverse(Utils.hexStringToByteArray(String.format("80%s", Utils.byteArrayToHexString(seq))));//bu.reverse("80"+seq);
        this.locktime = bu.reverse(Utils.hexStringToByteArray(String.format("20%s", Utils.byteArrayToHexString(locktime))));//bu.reverse("20"+locktime);
        this.scriptPubKey = Utils.hexStringToByteArray(String.format("0020%s", scriptToHash(escrowScript)));
        this.scriptPubKeySize = bu.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length));
        System.out.println("P2WSH2 Raw Transaction With escrowScript : "+Utils.byteArrayToHexString(hex()));
    }
    
    public String signingHashWithScript(String redeemScript, byte[] amt){
        System.out.println("P2WSH2 signingHash : "+redeemScript);
        String redeemScriptHex = bu.scriptToHex(redeemScript);
        System.out.println("P2WSH2 redeemScriptHex : "+redeemScriptHex);
        String prevOuts = String.format("%s%s",Utils.byteArrayToHexString(this.prevTxid), Utils.byteArrayToHexString(this.prevVout));
        String outputs = String.format("%s%s%s",Utils.byteArrayToHexString(this.satoshis), Utils.byteArrayToHexString(this.scriptPubKeySize), Utils.byteArrayToHexString(this.scriptPubKey));
        
        String hashPrevouts = bu.hextoDoubleHash(prevOuts);
        String hashSequence = bu.hextoDoubleHash(Utils.byteArrayToHexString(this.sequence));
        String scriptCode = String.format("%s%s",Utils.byteArrayToHexString(bu.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2))),redeemScriptHex);
        String amount= Utils.byteArrayToHexString(bu.reverse(amt));
        String hashOutputs= bu.hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",Utils.byteArrayToHexString(this.version),hashPrevouts,hashSequence,prevOuts,scriptCode,amount,Utils.byteArrayToHexString(this.sequence),hashOutputs,Utils.byteArrayToHexString(this.locktime),nHashType);
        System.out.println("P2WSH2 hashPreImage : "+hashPreImage);
        String msg = bu.hextoDoubleHash(hashPreImage);
        System.out.println("P2WSH2 msg : "+msg);
        return msg;
    }
    
    public String signingHashWithKey(byte[] pubKey, byte[] amt){
        //System.out.println("P2WSH2 signingHash : "+redeemScript);
        //String redeemScriptHex = bu.scriptToHex(redeemScript);
        //System.out.println("P2WSH2 redeemScriptHex : "+redeemScriptHex);
        String scriptPub = Utils.byteArrayToHexString(ec.Hash.ripemd160(ec.Hash.sha256(pubKey)));
        String prevOuts = String.format("%s%s",Utils.byteArrayToHexString(this.prevTxid), Utils.byteArrayToHexString(this.prevVout));
        String outputs = String.format("%s%s%s",Utils.byteArrayToHexString(this.satoshis), Utils.byteArrayToHexString(this.scriptPubKeySize), Utils.byteArrayToHexString(this.scriptPubKey));
        
        String hashPrevouts = bu.hextoDoubleHash(prevOuts);
        String hashSequence = bu.hextoDoubleHash(Utils.byteArrayToHexString(this.sequence));
        String scriptCode = String.format("1976a9%s88ac",scriptPub);
        String amount= Utils.byteArrayToHexString(bu.reverse(amt));
        String hashOutputs= bu.hextoDoubleHash(outputs);
        String nHashType="01000000";
        
        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",Utils.byteArrayToHexString(this.version),hashPrevouts,hashSequence,prevOuts,scriptCode,amount,Utils.byteArrayToHexString(this.sequence),hashOutputs,Utils.byteArrayToHexString(this.locktime),nHashType);
        System.out.println("P2WSH2 hashPreImage : "+hashPreImage);
        String msg = bu.hextoDoubleHash(hashPreImage);
        System.out.println("P2WSH2 msg : "+msg);
        return msg;
    }
    
    public boolean verifySignature(String redeemScript, byte[] amt, byte[] signature, byte[] pubKey) throws Exception{
        String msg = signingHashWithScript(redeemScript, amt);
        boolean result = ECDH_BC.verify(ec.Utils.hexStringToByteArray(msg), signature, pubKey);
        return result;
    }
    
    public String scriptToHash(String _){
        String scriptHex = bu.scriptToHex(_);
        return ec.Utils.byteArrayToHexString(ec.Hash.sha256(ec.Utils.hexStringToByteArray(scriptHex)));
    }
    
    public void updateWitness(byte[] sig, byte[] pubKey){
        this.numberOfWitness=Utils.hexStringToByteArray("02");
        this.witness= Utils.hexStringToByteArray(String.format("%s%s%s%s", Utils.byteArrayToHexString(bu.compactSizeUint(BigInteger.valueOf(sig.length))), Utils.byteArrayToHexString(sig), Utils.byteArrayToHexString(bu.compactSizeUint(BigInteger.valueOf(pubKey.length))), Utils.byteArrayToHexString(pubKey)));
    }
    
    public  byte[] hex(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(version);
            outputStream.write(inputCount);
            outputStream.write(prevTxid);
            outputStream.write(prevVout);
            outputStream.write(scriptSigSize);
            outputStream.write(sequence);
            outputStream.write(outputCount);
            outputStream.write(satoshis);
            outputStream.write(scriptPubKeySize);
            outputStream.write(scriptPubKey);
            outputStream.write(locktime);
            return outputStream.toByteArray();
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public  byte[] hex2(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(version);
            outputStream.write(marker);
            outputStream.write(flag);
            outputStream.write(inputCount);
            outputStream.write(prevTxid);
            outputStream.write(prevVout);
            outputStream.write(scriptSigSize);
            outputStream.write(sequence);
            outputStream.write(outputCount);
            outputStream.write(satoshis);
            outputStream.write(scriptPubKeySize);
            outputStream.write(scriptPubKey);
            outputStream.write(numberOfWitness);
            outputStream.write(witness);
            outputStream.write(locktime);
            return outputStream.toByteArray();
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public static void main(String args[]) throws Exception{
        
    }
}
