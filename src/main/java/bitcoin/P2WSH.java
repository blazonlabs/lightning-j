/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitcoin;

import ec.Utils;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;

/**
 *
 * @author Abhimanyu
 */
public class P2WSH {
    static BitcoinUtils bu;
    BitcoinUtils.NetworkType netType;
    byte[] version = Utils.intToLittleEndianWithByte(2, 4);
    byte[] marker = new byte[1];
    byte[] flag = Utils.hexStringToByteArray("01");
    byte[] inputCount = Utils.hexStringToByteArray("01");
    byte[] prevTxid;
    byte[] prevVout;
    byte[] scriptSigSize = new byte[1];
    byte[] sequence = Utils.hexStringToByteArray("ffffffff");
    byte[] outputCount = Utils.hexStringToByteArray("01");
    String amount;
    byte[] satoshis;
    byte[] scriptPubKeySize;
    byte[] scriptPubKey;
    byte[] numberOfWitness;
    byte[] witness;
    byte[] locktime = new byte[4];
    
    public P2WSH(){
        bu = new BitcoinUtils();
    }
    
    public P2WSH(BitcoinUtils.NetworkType netType, byte[] prevTxid, int prevVout, String redeemScript, String amount){
        bu = new BitcoinUtils();
        this.netType = netType;
        this.prevTxid = bu.reverse(prevTxid);
        this.prevVout = Utils.intToLittleEndianWithByte(prevVout, 4);
        this.satoshis = bu.btcToSatoshi(amount);
        
        this.scriptPubKey = Utils.hexStringToByteArray(String.format("0020%s", bu.scriptToWHash(redeemScript)));
        this.scriptPubKeySize = bu.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length));
    }
    
//    public String signedHex(ECKey[] e, String redeemScript, String amt){
//
//        String signatures = signature(e, redeemScript, amt);
//        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
//        int witnessLength = e.length + 2;
//        this.numberOfWitness=String.format("%02X",witnessLength);
//        this.witness = String.format("00%s%s%s",signatures,P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);
//
//        
//        return ec.Utils.byteArrayToHexString(this.hex2());
//    }
    
//    public String signature(ECKey[] e, String redeemScript, String amt){
//
//        System.out.println(String.format("redeem script: %s",redeemScript));
//        String redeemScriptHex = P2PKH.scriptToHex(redeemScript);
//        System.out.println(String.format("redeem script hex: %s",redeemScriptHex));
//        String prevOuts = String.format("%s%s%s",this.prevTxid,this.prevVout,this.voutConst);
//        String outputs = String.format("%s%s%s",this.satoshis,this.scriptPubKeySize,this.scriptPubKey);
//        
//        String hashPrevouts = hextoDoubleHash(prevOuts);
//        String hashSequence = hextoDoubleHash(this.sequence);
//        String scriptCode = String.format("%s%s",P2PKH.compactSizeUint(BigInteger.valueOf(redeemScriptHex.length()/2)),redeemScriptHex);
//        String amount= P2PKH.reverse(P2PKH.btcToSatoshi(amt));;
//        String hashOutputs= hextoDoubleHash(outputs);
//        String nHashType="01000000";
//        
//        String hashPreImage = String.format("%s%s%s%s%s%s%s%s%s%s",this.version,hashPrevouts,hashSequence,prevOuts,scriptCode,amount,this.sequence,hashOutputs,this.locktime,nHashType);
//        String msg = hextoDoubleHash(hashPreImage);
//        int arrLength = e.length;
//        String[] sig = new String[arrLength];
//        String signatures="";
//        for (int i = 0; i < e.length; i++) {
//            
//            Sha256Hash hash = Sha256Hash.wrap(msg);
//            ECKey.ECDSASignature s = e[i].sign(hash);
//            byte[] res = s.encodeToDER();
//            String test = DatatypeConverter.printHexBinary(res);
//            sig[i] = test.toLowerCase();
//        
//            System.out.println(String.format("--->> %s",sig[i]));
//            sig[i] = sig[i] + "01";
//            signatures = signatures + P2PKH.compactSizeUint(BigInteger.valueOf(sig[i].length()/2)) + sig[i];
//        }
//        return signatures;
//    }
    
    public byte[] hex(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(version);
            outputStream.write(inputCount);
            outputStream.write(prevTxid);
            outputStream.write(prevVout);
            outputStream.write(scriptSigSize);
            outputStream.write(sequence);
            outputStream.write(outputCount);
            outputStream.write(satoshis);
            outputStream.write(scriptPubKeySize);
            outputStream.write(scriptPubKey);
            outputStream.write(locktime);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public byte[] hex2(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(version);
            outputStream.write(marker);
            outputStream.write(flag);
            outputStream.write(inputCount);
            outputStream.write(prevTxid);
            outputStream.write(prevVout);
            outputStream.write(scriptSigSize);
            outputStream.write(sequence);
            outputStream.write(outputCount);
            outputStream.write(satoshis);
            outputStream.write(scriptPubKeySize);
            outputStream.write(scriptPubKey);
            outputStream.write(numberOfWitness);
            outputStream.write(witness);
            outputStream.write(locktime);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public static void main(String args[]) throws Exception{
        BitcoinClient bc = new BitcoinClient("http://blazon:blazon123@192.168.1.101:7332/");
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairA = new ECKey();
        ECKey pairB = new ECKey();
        SegwitAddressUtil su = SegwitAddressUtil.getInstance();
        byte witnessVersion=0;
        String addrA = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairA.getPubKeyHash());
        String addrB = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, pairB.getPubKeyHash());
        System.out.println("\n\nTestcase 9 results:\n");
        System.out.println(String.format("A add \n%s\nB addr \n%s",addrA, addrB));
        String txid0 = bc.getBitcoin().sendToAddress(addrA, 1 );
        bc.getBitcoin().generate(5);
        bc.getBitcoin().importAddress(addrA, null, true);
        bc.getBitcoin().importAddress(addrB, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to A \n%s",txid0));
        bc.printUTXO(addrA);
        bc.printUTXO(addrB);
        System.out.println(String.format("vout for %s %s",addrA,txid0));
        System.out.println(bc.voutFromAddressTxid(addrA, txid0));     
        String redeemScript = String.format("OP_2 %s %s OP_2 OP_CHECKMULTISIG",pairA.getPublicKeyAsHex(),pairB.getPublicKeyAsHex());
        P2WSH txnAtoMs = new P2WSH(BitcoinUtils.NetworkType.REGTEST, Utils.hexStringToByteArray(txid0), bc.voutFromAddressTxid(addrA, txid0) , redeemScript , "0.99" );
        txnAtoMs = bu.signedHex(pairA, Utils.byteArrayToHexString(bc.scriptPubKeyFromTxid(addrA, txid0)), bc.amountFromTxid(addrA, txid0), txnAtoMs);
        String signedHex1 = Utils.byteArrayToHexString(txnAtoMs.hex2());
        //String signedHex1 = txnAtoMs.signedHex(pairA, Utils.byteArrayToHexString(bc.scriptPubKeyFromTxid(addrA, txid0)), bc.amountFromTxid(addrA, txid0));
        System.out.println(String.format("signed hex 1\n%s",signedHex1));
        String txidAtoMs = bc.getBitcoin().sendRawTransaction(signedHex1);
        String multiSigAddr = su.encode(ec.Utils.hexStringToByteArray("62637274"), witnessVersion, ec.Utils.hexStringToByteArray(bu.scriptToWHash(redeemScript)));
        bc.getBitcoin().importAddress(multiSigAddr, null, true);
        System.out.println("redeem address is \n"+multiSigAddr);
        bc.getBitcoin().generate(5);
        System.out.println(String.format("TXId from A to Multi sig is \n%s",txidAtoMs));
        bc.printUTXO(addrA);
        bc.printUTXO(addrB);
        bc.printUTXO(multiSigAddr);
        System.out.println(String.format("vout for %s %s",multiSigAddr,txidAtoMs));
        System.out.println(bc.voutFromAddressTxid(multiSigAddr, txidAtoMs));
           
        P2WPKH txnMstoB = new P2WPKH(BitcoinUtils.NetworkType.REGTEST, ec.Utils.hexStringToByteArray(txidAtoMs),bc.voutFromAddressTxid(multiSigAddr, txidAtoMs) , addrB, "0.98");

        ECKey[] privateKeys = new ECKey[2];
        privateKeys[0] = pairA;
        privateKeys[1] = pairB;
        txnMstoB = bu.signedHex(privateKeys, redeemScript, bc.amountFromTxid(multiSigAddr, txidAtoMs), txnMstoB);
        String signedHex2 = Utils.byteArrayToHexString(txnMstoB.hex2());
        //String signedHex2 = txnMstoB.signedHex(privateKeys, redeemScript, bc.amountFromTxid(multiSigAddr, txidAtoMs));
        System.out.println(String.format("signed hex 2\n%s",signedHex2));
        String txidMsToB = bc.getBitcoin().sendRawTransaction(signedHex2);
        bc.getBitcoin().generate(5);
        System.out.println(String.format("TXId from Multi sig to B is \n%s",txidMsToB));
        bc.printUTXO(addrA);
        bc.printUTXO(addrB);
        bc.printUTXO(multiSigAddr);
    }
}
