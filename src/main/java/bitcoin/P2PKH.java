/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitcoin;

import bolt3.Bolt3Util;
import ec.Utils;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;

/**
 *
 * @author Abhimanyu
 */
public class P2PKH{
    static BitcoinUtils bu;
    byte[] version = Utils.intToLittleEndianWithByte(2, 4);
    byte[] inputCount = Utils.hexStringToByteArray("01");
    byte[] prevTxid;
    byte[] prevVout;
    byte[] scriptSigSize = new byte[1];
    byte[] scriptSig = new byte[0];
    byte[] sequence = Utils.hexStringToByteArray("ffffffff");
    byte[] outputCount = Utils.hexStringToByteArray("01");
    String amount;
    byte[] satoshis;
    byte[] scriptPubKeySize;
    byte[] scriptPubKey;
    byte[] locktime = new byte[4];
    
    public P2PKH(){
        bu = new BitcoinUtils();
    }
    
    public  P2PKH(Bolt3Util.NetworkType netType, byte[] prevTxid, int prevVout, String rxAddress, String amount){
        bu = new BitcoinUtils();
        this.prevTxid = bu.reverse(prevTxid);
        this.prevVout = Utils.intToLittleEndianWithByte(prevVout, 4);
        this.amount = amount;
        this.satoshis = bu.btcToSatoshi(amount);
        this.scriptPubKey = Utils.hexStringToByteArray(String.format("76a914%s88ac", bu.pkhFromAddress(rxAddress)));
        this.scriptPubKeySize = bu.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length));
    }
     public P2PKH(Bolt3Util.NetworkType netType, byte[] prevTxid, int prevVout, String rxAddress, String amount, byte[] scriptSig){
        bu = new BitcoinUtils();
        this.prevTxid = bu.reverse(prevTxid);
        this.prevVout = Utils.intToLittleEndianWithByte(prevVout, 4);
        this.satoshis = bu.btcToSatoshi(amount);
        this.scriptPubKey = Utils.hexStringToByteArray(String.format("76a914%s88ac", bu.pkhFromAddress(rxAddress)));
        this.scriptPubKeySize = bu.compactSizeUint(BigInteger.valueOf(this.scriptPubKey.length));
        this.scriptSig = scriptSig;
        this.scriptSigSize = bu.compactSizeUint(BigInteger.valueOf(scriptSig.length));
    }  
    
    public void setScriptSig(byte[] _){
        this.scriptSig = _;
        this.scriptSigSize = bu.compactSizeUint(BigInteger.valueOf(this.scriptSig.length));
    } 
     
    public BitcoinUtils getBitcoinUtilsObj(){
        return bu;
    }

    public byte[] hex(){
        try{
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(version);
            outputStream.write(inputCount);
            outputStream.write(prevTxid);
            outputStream.write(prevVout);
            outputStream.write(scriptSigSize);
            outputStream.write(scriptSig);
            outputStream.write(sequence);
            outputStream.write(outputCount);
            outputStream.write(satoshis);
            outputStream.write(scriptPubKeySize);
            outputStream.write(scriptPubKey);
            outputStream.write(locktime);
            return outputStream.toByteArray( );
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public static void main(String args[]){
        BitcoinClient bc = new BitcoinClient("http://blazon:blazon123@192.168.1.101:7332/");
        NetworkParameters reg = NetworkParameters.regTests();
        ECKey pairA = new ECKey();
        ECKey pairB = new ECKey();
        String addrA = pairA.toAddress(reg).toBase58();
        String addrB = pairB.toAddress(reg).toBase58();
        System.out.println("\n\nTestcase 4 results:\n");
        System.out.println(String.format("A add \n%s\nB addr \n%s",addrA, addrB));
        String txid0 = bc.getBitcoin().sendToAddress(addrA, 100 );
        bc.getBitcoin().generate(5);
        bc.getBitcoin().importAddress(addrA, null, true);
        bc.getBitcoin().importAddress(addrB, null, true);
        System.out.println(String.format("Funding TXId from bitcoin to A \n%s",txid0));
        bc.printUTXO(addrA);
        bc.printUTXO(addrB);
        System.out.println(String.format("vout for %s %s",addrA,txid0));
        System.out.println(bc.voutFromAddressTxid(addrA, txid0));
        P2PKH txn1 = new P2PKH(Bolt3Util.NetworkType.REGTEST , Utils.hexStringToByteArray(txid0), bc.voutFromAddressTxid(addrA, txid0) , addrB , "99.99" );
        System.out.println(String.format("transactionHex is \n%s",ec.Utils.byteArrayToHexString(txn1.hex())));
        
        txn1.setScriptSig(bc.scriptPubKeyFromTxid(addrA , txid0));
        String sigHex = bu.signedHex(pairA,txn1.hex());
        txn1.setScriptSig(Utils.hexStringToByteArray(sigHex));
        sigHex = Utils.byteArrayToHexString(txn1.hex());        
        
        System.out.println(String.format("signed hex \n%s",sigHex));
        String txid1 = bc.getBitcoin().sendRawTransaction(sigHex);
        System.out.println(String.format("TXId from A to B\n%s",txid1));
        bc.getBitcoin().generate(5);
        bc.printUTXO(addrA);
        bc.printUTXO(addrB);
    }
}
