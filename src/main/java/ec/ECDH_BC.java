/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec;

import bolt3.RPC;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import org.bitcoin.NativeSecp256k1;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1OutputStream;
import org.bouncycastle.asn1.DERSequenceGenerator;
import org.bouncycastle.asn1.DLSequence;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.crypto.signers.HMacDSAKCalculator;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.BigIntegers;


public class ECDH_BC
{
    static X9ECParameters params = SECNamedCurves.getByName("secp256k1");
    static ECDomainParameters curve = new ECDomainParameters(params.getCurve(), params.getG(), params.getN(), params.getH());
    static BigInteger halfCurveOrder = params.getN().shiftRight(1);
    
    public static byte[] doECDH (byte[] dataPrv, byte[] dataPub) throws Exception
    {
            ECPoint point = curve.getCurve().decodePoint(dataPub);
            BigInteger scalar = new BigInteger(1, dataPrv);
            ECPoint point1 = point.multiply(scalar).normalize();

            return Hash.sha256(point1.getEncoded(true));
    }
        
    public static byte[] getECPoint (byte[] dataPrv, byte[] dataPub) throws Exception
    {
            ECPoint point = curve.getCurve().decodePoint(dataPub);
            BigInteger scalar = new BigInteger(1, dataPrv);
            ECPoint point1 = point.multiply(scalar).normalize();

            return point1.getEncoded(true);
    }
    
    public static byte[] addECPoint (byte[] dataPrv, byte[] dataPub) throws Exception
    {
            ECPoint point1 = curve.getCurve().decodePoint(dataPrv);
            ECPoint point2 = curve.getCurve().decodePoint(dataPub);
            ECPoint pointOut = point1.add(point2).normalize();

            return pointOut.getEncoded(true);
    }
    
    public static byte[] getECPointN (List<byte[]> dataPrv, byte[] dataPub) throws Exception
    {
            ECPoint point = curve.getCurve().decodePoint(dataPub);
            for(int i=0;i<dataPrv.size();i++){
                BigInteger scalar = new BigInteger(1, dataPrv.get(i));
                point = point.multiply(scalar).normalize();
            }
            return point.getEncoded(true);
    }
    
    public static ECPoint getCurveG() throws Exception
    {
        return curve.getG();
    }
    
    public static byte[] xor(byte[] a, byte[] b){
        byte[] output = new byte[a.length];
        int i = 0;
        for (byte by : a){
            output[i] = (byte)((by ^  b[i++]) & 0xff);
        }
        return output;
    }
    
    public static byte[] getPubKeyFromBasepoint(byte[] perCommitmentPoint, byte[] basepoint){
        try{
            byte[] base = Utils.concatAll(perCommitmentPoint, basepoint);
            byte[] baseg = getECPoint(Hash.sha256(base), params.getG().getEncoded(true));
            return addECPoint(basepoint, baseg);
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public static byte[] getRevocationPubKey(byte[] perCommitmentPoint, byte[] basepoint){
        try{
            byte[] base1 = Utils.concatAll(basepoint, perCommitmentPoint);
            byte[] baseg1 = getECPoint(Hash.sha256(base1), basepoint);
            
            byte[] base2 = Utils.concatAll(perCommitmentPoint, basepoint);
            byte[] baseg2 = getECPoint(Hash.sha256(base2), perCommitmentPoint);
            
            return addECPoint(baseg1, baseg2);
        }catch(Exception e){
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public static int compareKeys(byte[] key1, byte[] key2){
        BigInteger k1 = new BigInteger(1, key1);
        BigInteger k2 = new BigInteger(1, key2);
        return k1.compareTo(k2);
    }
    
    public static boolean verify(byte[] hash, byte[] signature, byte[] publicKey) {
        ASN1InputStream asn1 = new ASN1InputStream(signature);
        try {
            ECDSASigner signer = new ECDSASigner();
            signer.init(false, new ECPublicKeyParameters(curve.getCurve().decodePoint(publicKey), curve));

            DLSequence seq = (DLSequence) asn1.readObject();
            BigInteger r = ((ASN1Integer) seq.getObjectAt(0)).getPositiveValue();
            BigInteger s = ((ASN1Integer) seq.getObjectAt(1)).getPositiveValue();
            return signer.verifySignature(hash, r, s);
        } catch (Exception e) {
            return false;
        } finally {
            try {
                asn1.close();
            } catch (IOException ignored) {
            }
        }
    }
    
    public static byte[] sign(byte[] input, byte[] prv){
        try {
            ECDSASigner signer = new ECDSASigner(new HMacDSAKCalculator(new SHA256Digest()));
            ECPrivateKeyParameters privateKeyParms = new ECPrivateKeyParameters(new BigInteger(1, prv), curve);
            //ParametersWithRandom params = new ParametersWithRandom(privateKeyParms);

            signer.init(true, privateKeyParms);
            
            BigInteger[] sigs = signer.generateSignature(input);
            if (sigs[1].compareTo(halfCurveOrder) > 0)
                sigs[1] = curve.getN().subtract(sigs[1]); 

            /*ByteArrayOutputStream outStream  = new ByteArrayOutputStream();
            DERSequenceGenerator seq = new DERSequenceGenerator(outStream);
            seq.addObject(new ASN1Integer(sigs[0]));
            seq.addObject(new ASN1Integer(sigs[1]));
            seq.close();
            return outStream.toByteArray();*/
            return Utils.concatAll(sigs[0].toByteArray(), sigs[1].toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
            return new byte[0];
        }
    }
    
    public static void main (String [] args) throws Exception
    {
        //byte[] ss = doECDH(Utils.hexStringToByteArray("1212121212121212121212121212121212121212121212121212121212121212"), Utils.hexStringToByteArray("028d7500dd4c12685d1f568b4c2b5048e8534b873319f3a8daa612b469132ec7f7"));
        //System.out.println(Utils.byteArrayToHexString(ss));
        
//        byte[] per_commitment_secret = Utils.hexStringToByteArray("1f1e1d1c1b1a191817161514131211100f0e0d0c0b0a09080706050403020100");
//        byte[] per_commitment_point = getECPoint(per_commitment_secret, params.getG().getEncoded(true));
//        //System.out.println(Utils.byteArrayToHexString(per_commitment_point));
//        
//        byte[] base_secret = Utils.hexStringToByteArray("000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f");
//        byte[] base_point = Utils.hexStringToByteArray("036d6caac248af96f6afa7f904f550253a0f3ef3f5aa2fe6838a95b216691468e2");
//        
//        byte[] base = Utils.concatAll(per_commitment_point, base_point);
//        System.out.println(Utils.byteArrayToHexString(base));
//        System.out.println(Utils.byteArrayToHexString(Hash.sha256(base)));
//        
//        byte[] baseg = getECPoint(Hash.sha256(base), params.getG().getEncoded(true));
//        
//        byte[] localpubkey = addECPoint(base_point, baseg);
//        System.out.println(Utils.byteArrayToHexString(localpubkey));
        
//        byte[] x = Utils.hexStringToByteArray("ae26ee7560b62d007165cfaec0fdf191909936a129dd3f2d734b24609414c10f");
//        byte[] pub = Utils.hexStringToByteArray("02fe817e93a5588a5a777410deed88ab56d51b6500e2e31df79db7df94c24bd6da");
//        byte[] sig = Utils.hexStringToByteArray("3044022009f078fbce5a66f1a7cc47c5ac8a01f05eb0cf3eb60c363033092a68eaa19f8902205894f725876ee774e5b0936b9c764602407ef469f5a94752f9c03b8f0654c0b801");
//        
//        System.out.println(Utils.byteArrayToHexString(getECPoint(sig, pub)));
//        System.out.println(verify(x, sig, pub));
        
//          byte[] hash = Utils.hexStringToByteArray("e49595d342d54379489dcf0a7409f7cacd72b3f821737abdfe3a7f7ae1b52794");
//          byte[] prv = Utils.hexStringToByteArray("0f0763c79d738b57b1e768c1bbc3207d94fa8ff335e156c60fc05c99c2b3b931");
//          byte[] pub = Utils.hexStringToByteArray("03e14bbcf9fac149c6205f9e5c6db172bbf85885ac51f988336e63599acf207618");
//          byte[] sig = Utils.hexStringToByteArray("30440220296bb5c88ddbfa7c1f614371aa7179200bc11404f6e5ab783434bfa12f9cf45c0220475f06bacaf7c7ebc5bf35e9ca0e6ed12126c53039701efe65cf01e294ca3124");
//          System.out.println(Utils.byteArrayToHexString(sig));
//          System.out.println(Utils.byteArrayToHexString(sign(hash, prv)));
        
//        byte[] hash = Utils.hexStringToByteArray("71924fb7ecd4df7df906dbca8abb6b2e9d6aaa8df87d5cc423813e0b93951eac");
//        byte[] prv = Utils.hexStringToByteArray("a7bdd38447718d96dca53b348394bec63f4e52f45d516ce94dde062825e9a0ee");
//        byte[] pub = Utils.hexStringToByteArray("023cd3be542983868d1a3a24a7cca868f0b023944d157dd2a99167e60018d7d1e3");
//        byte[] sig = Utils.hexStringToByteArray("3045022100bc9bf6af0561cdf7cce6a5479c987777281368863c623c2c65f38060c11f3228022014d5c5c6afe70a42cfd524d926e166c182ffa671e72904b5e6249ca21c447a94");
//        System.out.println(Utils.byteArrayToHexString(sig));
//        System.out.println(Utils.byteArrayToHexString(sign(hash, prv)));
//        
//        System.out.println(RPC.signFromHexAndPKey("71924fb7ecd4df7df906dbca8abb6b2e9d6aaa8df87d5cc423813e0b93951eac", "cTCmZtX4mPs5DB3b7MEigBwSipvVKtTaDNdyKNmA9WNG3QsPKCzk"));
        
        System.out.println(Utils.byteArrayToHexString(getRevocationPubKey(Utils.hexStringToByteArray("025f7117a78150fe2ef97db7cfc83bd57b2e2c0d0dd25eaf467a4a1c2a45ce1486"), Utils.hexStringToByteArray("036d6caac248af96f6afa7f904f550253a0f3ef3f5aa2fe6838a95b216691468e2"))));
    }
}
