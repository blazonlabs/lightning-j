/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/*
 * A standards-compliant implementation of RFC 5869
 * for HMAC-based Key Derivation Function.
 * HMAC uses HMAC SHA256 standard.
 */
public class HKDF {

    /**
     * Used for conversion in cases in which you *know* the encoding exists.
     * @param in
     * @return 
     */
    public static final byte[] bytes(String in) {
      try {
          return in.getBytes("UTF-8");
      } catch (java.io.UnsupportedEncodingException e) {
          return null;
      }
    }

    public static final int BLOCKSIZE     = 256 / 8;
    public static final byte[] HMAC_INPUT = bytes("Sync-AES_256_CBC-HMAC256");

    /*
     * Step 1 of RFC 5869
     * Get sha256HMAC Bytes
     * Input: salt (message), IKM (input keyring material)
     * Output: PRK (pseudorandom key)
     */
    public static byte[] hkdfExtract(byte[] salt, byte[] IKM) {
        return digestBytes(IKM, makeHMACHasher(salt));
    }

    /*
     * Step 2 of RFC 5869.
     * Input: PRK from step 1, info, length.
     * Output: OKM (output keyring material).
     */
    public static byte[] hkdfExpand(byte[] prk, byte[] info, int len) {

        Mac hmacHasher = makeHMACHasher(prk);

        byte[] T  = {};
        byte[] Tn = {};

        int iterations = (int) Math.ceil(((double)len) / ((double)BLOCKSIZE));
        for (int i = 0; i < iterations; i++) {
            Tn = digestBytes(ec.Utils.concatAll(Tn, info, ec.Utils.hex2Byte(Integer.toHexString(i + 1))), hmacHasher);
            T = ec.Utils.concatAll(T, Tn);
        }

        return Arrays.copyOfRange(T, 0, len);
    }

    /*
     * Make HMAC key
     * Input: key (salt)
     * Output: Key HMAC-Key
     */
    public static Key makeHMACKey(byte[] key) {
        if (key.length == 0) {
            key = new byte[BLOCKSIZE];
        }
        return new SecretKeySpec(key, "HmacSHA256");
    }

    /*
     * Make an HMAC hasher
     * Input: Key hmacKey
     * Ouput: An HMAC Hasher
     */
    public static Mac makeHMACHasher(byte[] key) {
        Mac hmacHasher = null;
        try {
            hmacHasher = Mac.getInstance("hmacSHA256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            hmacHasher.init(makeHMACKey(key));
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return hmacHasher;
    }

    /*
     * Hash bytes with given hasher
     * Input: message to hash, HMAC hasher
     * Output: hashed byte[].
     */
    public static byte[] digestBytes(byte[] message, Mac hasher) {
        hasher.update(message);
        byte[] ret = hasher.doFinal();
        hasher.reset();
        return ret;
    }
    
    public static byte[] doHKDF(byte[] ck, byte[] ss){
        byte[] prk = hkdfExtract(ck, ss);
        byte[] okm = hkdfExpand(prk, new byte[0], 64);
        return okm;
    }
    
    public static void main(String[] args){
        byte[] oldh = Utils.hexStringToByteArray("9e0e7de8bb75554f21db034633de04be41a2b8a18da7a319a03c803bf02b396c0df6086551151f58b8afe6c195782c6a");
        byte[] h = Hash.sha256(oldh);
        System.err.println(Utils.byteArrayToHexString(h));
        
        byte[] salt = Utils.hexStringToByteArray("e89d31033a1b6bf68c07d22e08ea4d7884646c4b60a9528598ccb4ee2c8f56ba");
        byte[] ikm = Utils.hexStringToByteArray("b36b6d195982c5be874d6d542dc268234379e1ae4ff1709402135b7de5cf0766");
        
        byte[] prk = hkdfExtract(salt, ikm);
        System.err.println(Utils.byteArrayToHexString(prk));
        
        byte[] okm = hkdfExpand(prk, new byte[0], 64);
        System.err.println(Utils.byteArrayToHexString(okm));
        //919219dbb2920afa8db80f9a51787a840bcf111ed8d588caf9ab4be716e42b01981a46c820fb7a241bc8184ba4bb1f01bcdfafb00dde80098cb8c38db9141520
        //919219dbb2920afa8db80f9a51787a840bcf111ed8d588caf9ab4be716e42b01
        //981a46c820fb7a241bc8184ba4bb1f01bcdfafb00dde80098cb8c38db9141520
    }
}
