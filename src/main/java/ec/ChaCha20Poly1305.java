/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.crypto.engines.ChaCha7539Engine;
import org.bouncycastle.crypto.macs.Poly1305;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.bouncycastle.crypto.engines.ChaChaEngine;
/**
 *
 * @author Abhimanyu
 */
public class ChaCha20Poly1305 {
    
    public byte[] poly1305Mac(byte[] key, byte[] data){
        byte[] out = empty(new byte[16]);
        Poly1305 poly = new Poly1305();
        poly.init(new KeyParameter(key));
        poly.update(data, 0, data.length);
        poly.doFinal(out, 0);
        return out;
    }
    
    public byte[] chaCha20Encrypt(byte[] plaintext, byte[] key, byte[] nonce, int counter){
        ChaCha7539Engine engine = new ChaCha7539Engine();
        engine.init(true, new ParametersWithIV(new KeyParameter(key), nonce));
        byte[] ciphertext = new byte[plaintext.length];
        
        switch(counter){
            case 0 :
                break;
            case 1 :
                byte[] dummy = new byte[64];
                engine.processBytes(new byte[64], 0, 64, dummy, 0);
                break;
            default :
                throw new RuntimeException("chacha20 counter must be 0 or 1");
        }
        
        int len = engine.processBytes(plaintext, 0, plaintext.length, ciphertext, 0);
        if(len != plaintext.length){
            System.err.println("ChaCha20 encryption failed");
            return new byte[0];
        }
        return ciphertext;
    }
    
    public byte[] chaCha20LegacyEncrypt(byte[] plaintext, byte[] key, byte[] nonce, int counter){
        ChaChaEngine engine = new ChaChaEngine(20);
        engine.init(true, new ParametersWithIV(new KeyParameter(key), nonce));
        byte[] ciphertext = new byte[plaintext.length];
        
        switch(counter){
            case 0 :
                break;
            case 1 :
                byte[] dummy = new byte[64];
                engine.processBytes(new byte[64], 0, 64, dummy, 0);
                break;
            default :
                throw new RuntimeException("chacha20 counter must be 0 or 1");
        }
        
        int len = engine.processBytes(plaintext, 0, plaintext.length, ciphertext, 0);
        if(len != plaintext.length){
            System.err.println("ChaCha20 encryption failed");
            return new byte[0];
        }
        return ciphertext;
    }
    
    public byte[] chaCha20Decrypt(byte[] ciphertext, byte[] key, byte[] nonce, int counter){
        ChaCha7539Engine engine = new ChaCha7539Engine();
        engine.init(true, new ParametersWithIV(new KeyParameter(key), nonce));
        byte[] plaintext = new byte[ciphertext.length];
        
        switch(counter){
            case 0 :
                break;
            case 1 :
                byte[] dummy = new byte[64];
                engine.processBytes(new byte[64], 0, 64, dummy, 0);
                break;
            default :
                throw new RuntimeException("chacha20 counter must be 0 or 1");
        }
        
        int len = engine.processBytes(ciphertext, 0, ciphertext.length, plaintext, 0);
        if(len != ciphertext.length){
            System.err.println("ChaCha20 decryption failed");
            return new byte[0];
        }
        return plaintext;
    }
    
    public byte[] chaCha20LegacyDecrypt(byte[] ciphertext, byte[] key, byte[] nonce, int counter){
        ChaChaEngine engine = new ChaChaEngine(20);
        engine.init(true, new ParametersWithIV(new KeyParameter(key), nonce));
        byte[] plaintext = new byte[ciphertext.length];
        
        switch(counter){
            case 0 :
                break;
            case 1 :
                byte[] dummy = new byte[64];
                engine.processBytes(new byte[64], 0, 64, dummy, 0);
                break;
            default :
                throw new RuntimeException("chacha20 counter must be 0 or 1");
        }
        
        int len = engine.processBytes(ciphertext, 0, ciphertext.length, plaintext, 0);
        if(len != ciphertext.length){
            System.err.println("ChaCha20 decryption failed");
            return new byte[0];
        }
        return plaintext;
    }
    
    public byte[] chaCha20Poly1305Pad16(byte[] data){
        if(data.length %16 == 0)
            return new byte[0];
        else{
            byte[] ar = new byte[(16 - (data.length % 16))];
            for(int i=0;i<ar.length;i++)
                ar[i] = 0;
            return ar;
        }
    }
    
    public byte[] writeUInt64(long input, ByteOrder bo){
        byte[] ba = new byte[8];
        ByteBuffer bor = ByteBuffer.wrap(ba).order(bo);
        bor.putLong(input);
        return ba;
    }
    
    public byte[] empty(byte[] a){
        for(int i=0;i<a.length;i++)
            a[i] = 0;
        return a;
    }
    
    public byte[][] chaCha20Poly1305Encrypt(byte[] key, byte[] nonce, byte[] plaintext, byte[] aad){
        byte[] polykey = chaCha20Encrypt(empty(new byte[32]), key, nonce, 0);
        byte[] ciphertext  = chaCha20Encrypt(plaintext, key, nonce, 1);
        ByteArrayOutputStream op = new ByteArrayOutputStream();
        try {
            op.write(aad);
            op.write(chaCha20Poly1305Pad16(aad));
            op.write(ciphertext);
            op.write(chaCha20Poly1305Pad16(ciphertext));
            op.write(writeUInt64(aad.length, ByteOrder.LITTLE_ENDIAN));
            op.write(writeUInt64(ciphertext.length, ByteOrder.LITTLE_ENDIAN));
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(ChaCha20Poly1305.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        byte[] tag = poly1305Mac(polykey, op.toByteArray());
        byte[][] out = new byte[2][1];
        out[0] = ciphertext;
        out[1] = tag;
        return out;
    }
    
    public byte[] chaCha20Poly1305Decrypt(byte[] key, byte[] nonce, byte[] ciphertext, byte[] aad, byte [] mac){
        byte[] polykey = chaCha20Encrypt(empty(new byte[32]), key, nonce, 0);
        ByteArrayOutputStream op = new ByteArrayOutputStream();
        try {
            op.write(aad);
            op.write(chaCha20Poly1305Pad16(aad));
            op.write(ciphertext);
            op.write(chaCha20Poly1305Pad16(ciphertext));
            op.write(writeUInt64(aad.length, ByteOrder.LITTLE_ENDIAN));
            op.write(writeUInt64(ciphertext.length, ByteOrder.LITTLE_ENDIAN));
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(ChaCha20Poly1305.class.getName()).log(Level.SEVERE, null, ex);
        }
        byte[] tag = poly1305Mac(polykey, op.toByteArray());
        if(! Arrays.equals(tag, mac)){
            System.err.print("invalid mac");
            return new String("invalid mac").getBytes();
        }else{
            byte[] plaintext  = chaCha20Decrypt(ciphertext, key, nonce, 1);
            return plaintext;
        }
    }
}
