/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec;

import io.tcp.Initiator;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 * @author Abhimanyu
 */
public final class KeyUtils {
    KeyPairGenerator kpgen;
    KeyPair ePair;
    byte[][] e  = new byte[2][1];
    
    public KeyUtils(){
        try {
            Security.addProvider(new BouncyCastleProvider());
            kpgen = KeyPairGenerator.getInstance("ECDH", "BC");
            generateNewKeyPairBytes();
        } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {
            Logger.getLogger(KeyUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static byte [] savePublicKey (PublicKey key) throws Exception
    {
        ECPublicKey eckey = (ECPublicKey)key;
        return eckey.getQ().getEncoded(true);
    }

    public static byte [] savePrivateKey (PrivateKey key) throws Exception
    {
        ECPrivateKey eckey = (ECPrivateKey)key;
        return eckey.getD().toByteArray();
    }
    
    public KeyPair getNewKeyPair(){
        try {
            kpgen.initialize(new ECGenParameterSpec("secp256k1"), new SecureRandom());
            KeyPair ls = kpgen.generateKeyPair();
            return ls;
        } catch (InvalidAlgorithmParameterException ex) {
            Logger.getLogger(Initiator.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public void generateNewKeyPairBytes(){
        try {
            ePair = getNewKeyPair();
            e = new byte[2][1];
            e[0] = KeyUtils.savePrivateKey(ePair.getPrivate());
            e[1] = KeyUtils.savePublicKey(ePair.getPublic());
        } catch (Exception ex) {
            Logger.getLogger(Initiator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public KeyPair keyPair(){
        try {
            return ePair;
        } catch (Exception ex) {
            Logger.getLogger(Initiator.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public byte[] encrypt(String message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.ENCRYPT_MODE, ePair.getPrivate());  

        return cipher.doFinal(message.getBytes());  
    }
    
    public byte[] decrypt(byte [] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.DECRYPT_MODE, ePair.getPublic());
        
        return cipher.doFinal(encrypted);
    }
    
    public byte[] encrypt(PrivateKey privateKey, String message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);  

        return cipher.doFinal(message.getBytes());  
    }
    
    public byte[] decrypt(PublicKey publicKey, byte [] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        
        return cipher.doFinal(encrypted);
    }
    
    
    
    public byte[] getPrv(){
        try {
            return e[0];
        } catch (Exception ex) {
            Logger.getLogger(Initiator.class.getName()).log(Level.SEVERE, null, ex);
            return new byte[0];
        }
    }
    
    public byte[] getPub(){
        try {
            return e[1];
        } catch (Exception ex) {
            Logger.getLogger(Initiator.class.getName()).log(Level.SEVERE, null, ex);
            return new byte[0];
        }
    }
    
    public byte[] prv(){
        try {
            return Utils.hexStringToByteArray("1111111111111111111111111111111111111111111111111111111111111111");
        } catch (Exception ex) {
            Logger.getLogger(Initiator.class.getName()).log(Level.SEVERE, null, ex);
            return new byte[0];
        }
    }
    
    public byte[] pub(){
        try {
            return Utils.hexStringToByteArray("034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa");
        } catch (Exception ex) {
            Logger.getLogger(Initiator.class.getName()).log(Level.SEVERE, null, ex);
            return new byte[0];
        }
    }
    
    
    
    public static void main (String [] args) throws Exception
    {
        Security.addProvider(new BouncyCastleProvider());

        /*KeyPairGenerator kpgen = KeyPairGenerator.getInstance("ECDH", "BC");
        kpgen.initialize(new ECGenParameterSpec("secp256k1"), new SecureRandom());
        KeyPair pairA = kpgen.generateKeyPair();
        System.out.println("Alice: " + pairA.getPrivate());
        System.out.println("Alice: " + pairA.getPublic());
        byte [] dataPrvA = savePrivateKey(pairA.getPrivate());
        byte [] dataPubA = savePublicKey(pairA.getPublic());
        System.out.println("Alice Prv: " + bytesToHex(dataPrvA));
        System.out.println("Alice Pub: " + bytesToHex(dataPubA));

        KeyPair pairB = kpgen.generateKeyPair();
        System.out.println("Bob:   " + pairB.getPrivate());
        System.out.println("Bob:   " + pairB.getPublic());
        byte [] dataPrvB = savePrivateKey(pairB.getPrivate());
        byte [] dataPubB = savePublicKey(pairB.getPublic());
        System.out.println("Bob Prv:   " + bytesToHex(dataPrvB));
        System.out.println("Bob Pub:   " + bytesToHex(dataPubB));

        doECDH("Alice's secret: ", dataPrvA, dataPubB);
        doECDH("Bob's secret:   ", dataPrvB, dataPubA);





        byte[] key = hexStringToByteArray("e68f69b7f096d7917245f5e5cf8ae1595febe4d4644333c99f9c4a1282031c9f");
        byte[] nonce = hexStringToByteArray("000000000000000000000000");
        byte[] aad = hexStringToByteArray("9e0e7de8bb75554f21db034633de04be41a2b8a18da7a319a03c803bf02b396c");
        byte[] plaintext = new byte[0];

        byte[][] cipher = new ChaCha20Poly1305().chaCha20Poly1305Encrypt(key, nonce, plaintext, aad);
        System.out.println("\noutput cipher length : "+cipher[0].length+"\n"+byteArrayToHexString(cipher[0]));
        System.out.println("\noutput tag length : "+cipher[1].length+"\n"+byteArrayToHexString(cipher[1]));

        byte[] mac = hexStringToByteArray("0df6086551151f58b8afe6c195782c6a");
        byte[] plain = new ChaCha20Poly1305().chaCha20Poly1305Decrypt(key, nonce, cipher[0], aad, cipher[1]);
        System.out.println("\n output plain length : "+plain.length+"\n"+byteArrayToHexString(plain));
        */
        
        
        
    }
}
