/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ArrayUtils;

public class Utils {

    private static final int[] r58 = new int[256];
    private static final char[] b58 = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz".toCharArray ();
    /*
     * Helper to convert Hex String to Byte Array
     * Input: Hex string
     * Output: byte[] version of hex string
     */
    public static byte[] hex2Byte(String str)
    {
        if (str.length() % 2 == 1) {
            str = "0" + str;
        }

        byte[] bytes = new byte[str.length() / 2];
        for (int i = 0; i < bytes.length; i++)
        {
            bytes[i] = (byte) Integer
                .parseInt(str.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    /*
     * Helper to convert Byte Array to a Hex String
     * Input: byte[] array
     * Output: Hex string
     */
    public static String byte2hex(byte[] b) {

        // String Buffer can be used instead
        String hs = "";
        String stmp = "";

        for (int n = 0; n < b.length; n++) {
            stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));

            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            } else {
                hs = hs + stmp;
            }

            if (n < b.length - 1) {
                hs = hs + "";
            }
        }

        return hs;
    }

    /*
     * Helper for array concatenation.
     * Input: At least two byte[]
     * Output: A concatenated version of them
     */
    public static byte[] concatAll(byte[] first, byte[]... rest) {
        int totalLength = first.length;
        for (byte[] array : rest) {
            totalLength += array.length;
        }

        byte[] result = Arrays.copyOf(first, totalLength);
        int offset = first.length;

        for (byte[] array : rest) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }

    public static String toBase58(byte[] b) {
        if (b.length == 0) {
            return "";
        }

        int lz = 0;
        while (lz < b.length && b[lz] == 0) {
            ++lz;
        }

        StringBuffer s = new StringBuffer();
        BigInteger n = new BigInteger(1, b);
        while (n.compareTo(BigInteger.ZERO) > 0) {
            BigInteger[] r = n.divideAndRemainder(BigInteger.valueOf(58));
            n = r[0];
            char digit = b58[r[1].intValue()];
            s.append(digit);
        }
        while (lz > 0) {
            --lz;
            s.append("1");
        }
        return s.reverse().toString();
    }

    public static byte[] fromBase58(String s) throws Exception {
        try {
            boolean leading = true;
            int lz = 0;
            BigInteger b = BigInteger.ZERO;
            for (char c : s.toCharArray()) {
                if (leading && c == '1') {
                    ++lz;
                } else {
                    leading = false;
                    b = b.multiply(BigInteger.valueOf(58));
                    b = b.add(BigInteger.valueOf(r58[c]));
                }
            }
            byte[] encoded = b.toByteArray();
            if (encoded[0] == 0) {
                if (lz > 0) {
                    --lz;
                } else {
                    byte[] e = new byte[encoded.length - 1];
                    System.arraycopy(encoded, 1, e, 0, e.length);
                    encoded = e;
                }
            }
            byte[] result = new byte[encoded.length + lz];
            System.arraycopy(encoded, 0, result, lz, encoded.length);

            return result;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new Exception("Invalid character in address");
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
    
    /*
     * Decode a friendly base32 string.
     */
    public static byte[] decodeFriendlyBase32(String base32) {
        Base32 converter = new Base32();
        return converter.decode(base32.replace('8', 'l').replace('9', 'o')
                .toUpperCase());
    }

    /**
     * Utility for Base64 decoding. Should ensure that the correct
     * Apache Commons version is used.
     *
     * @param base64
     *        An input string. Will be decoded as UTF-8.
     * @return
     *        A byte array of decoded values.
     * @throws UnsupportedEncodingException
     *         Should not occur.
     */
    public static byte[] decodeBase64(String base64) throws UnsupportedEncodingException {
        return Base64.decodeBase64(base64.getBytes("UTF-8"));
    }
    
    public static byte[] hexStringToByteArray(String s) {
            int len = s.length();
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                     + Character.digit(s.charAt(i+1), 16));
            }

            return data;
    }

    public static String reverseHexString(String hexString){
            byte[] ret = hexStringToByteArray(hexString);
            ArrayUtils.reverse(ret);
            return byteArrayToHexString(ret);
    }
    
    public static byte[] reverseHexArray(byte[] _){
        byte[] clone = ArrayUtils.clone(_);
        ArrayUtils.reverse(clone);
        return clone;
    }
        
    public static String byteArrayToHexString(byte[] b) {
            int len = b.length;
            String data = new String();

            for (int i = 0; i < len; i++){
                data += Integer.toHexString((b[i] >>> 4) & 0xf);
                data += Integer.toHexString(b[i] & 0xf);
            }
            return data;
    }
        
    public static String convertHexToString(String hex){
                System.out.println("convertHexToString Input Hex : " + hex);
                StringBuilder sb = new StringBuilder();
                StringBuilder temp = new StringBuilder();

                //49204c6f7665204a617661 split into two characters 49, 20, 4c...
                for( int i=0; i<hex.length()-1; i+=2 ){

                    //grab the hex in pairs
                    String output = hex.substring(i, (i + 2));
                    //convert hex to decimal
                    int decimal = Integer.parseInt(output, 16);
                    //convert the decimal to character
                    sb.append((char)decimal);

                    temp.append(decimal);
                }
                System.out.println("convertHexToString Output Decimal : " + temp.toString());

                return sb.toString();
    }
        
    public static byte[] intToLittleEndian(int numero) {
            ByteBuffer bb = ByteBuffer.allocate(8);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            bb.putInt((int) numero);
            return bb.array();
    }
    
    public static byte[] longToLittleEndian(long numero) {
            ByteBuffer bb = ByteBuffer.allocate(8);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            bb.putLong((long) numero);
            return bb.array();
    }
    
    public static byte[] intToLittleEndianWithByte(int numero, int b) {
            ByteBuffer bb = ByteBuffer.allocate(b);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            bb.putInt((int) numero);
            return bb.array();
    }
    
    public static byte[] byteArrayToLittleEndianWithByte(byte[] by, int b) {
            ByteBuffer bb = ByteBuffer.allocate(b);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            bb.put(by);
            return bb.array();
    }
        
    public static byte[] getNonceFromInt(int num){
            return hexStringToByteArray("00000000"+byteArrayToHexString(intToLittleEndian(num)));
    }
    
    public static byte[] getBigXOR(byte[] x, byte[] y){
        if(x.length > y.length){
            y = concatAll(new byte[x.length-y.length], y);
        }
        if(x.length < y.length){
            y = concatAll(new byte[y.length-x.length], x);
        }
        ByteBuffer bb = ByteBuffer.allocate(x.length);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.put(ECDH_BC.xor(x, y));
        return bb.array();
    }
    
    public static byte[] getBig(byte[] x){
        ByteBuffer bb = ByteBuffer.allocate(x.length);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.put(x);
        return bb.array();
    }
    
    /*public static byte[] getByteInFormatString(byte[] arr, int nob){
            ByteBuffer bb = ByteBuffer.allocate(nob);
            bb.put(arr);
            return bb.array();
    }
    public static byte[] getHexByteFromIntInFormat(int num, int nob){
            //return hexStringToByteArray("00000000"+byteArrayToHexString(intToLittleEndian(num)));
            ByteBuffer bb = ByteBuffer.allocate(nob);
            bb.putInt((int) num);
            return bb.array();
    }*/
    public static byte[] getHexOfShortWithBytes(short num, int nob){
            //return hexStringToByteArray("00000000"+byteArrayToHexString(intToLittleEndian(num)));
            ByteBuffer bb = ByteBuffer.allocate(nob);
            bb.putShort((short) num);
            return bb.array();
    }
    
    public static byte[] getHexOfIntWithBytes(int num, int nob){
            //return hexStringToByteArray("00000000"+byteArrayToHexString(intToLittleEndian(num)));
            if(nob > 3){
                ByteBuffer bb = ByteBuffer.allocate(nob);
                bb.putInt((int) num);
                return bb.array();
            }else{
                return new byte[0];
            }
    }
    
    public static byte[] getHexOfLongWithBytes(long num, int nob){
            //return hexStringToByteArray("00000000"+byteArrayToHexString(intToLittleEndian(num)));
            if(nob > 7){
                ByteBuffer bb = ByteBuffer.allocate(nob);
                bb.putLong((long) num);
                return bb.array();
            }else{
                return new byte[0];
            }
    }
    
    
    public static void main (String [] args) throws Exception
    {	
        //System.out.println(byteArrayToHexString(getNonceFromInt(117)));
//        System.out.println(byteArrayToHexString(getHexOfShortWithBytes((short)32, 2)));
//        System.out.println(byteArrayToHexString(getHexOfIntWithBytes(19, 4)));
//        
//        byte[] b = new byte[2];
//        System.out.println(byteArrayToHexString(b));
//        
//        System.out.println(Utils.byteArrayToHexString(getHexOfShortWithBytes((short)1, 2)));
        /*byte[] ftxid = hexStringToByteArray("b478e08eb8101a6a6922759d80dc054f0317a52d82b0d1ee415916cc7aa9e334");
        BigInteger ft = new BigInteger(1, ftxid);
        byte[] foi = concatAll(new byte[30], hexStringToByteArray("0000"));
        BigInteger fo = new BigInteger(1, foi);
        System.out.println(ft.toString(2));
        System.out.println(fo.toString(2));
        byte[] output = getBigXOR(ftxid, foi);
        BigInteger out = new BigInteger(1, output);
        System.out.println(out.toString(2));*/
        
        String amount = "1000000";
        long amt = Long.parseLong(amount);
        System.out.println(Utils.byteArrayToHexString(getHexOfLongWithBytes(amt, 8)));
    }
    
}
